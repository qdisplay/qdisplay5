/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2019 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#ifndef IMAGEOPERATIONSUI_H
#define IMAGEOPERATIONSUI_H

#include <QObject>

#include "errorcodes.h"

class ImageOperationsUI : public QObject
{
    Q_OBJECT

public:
    ImageOperationsUI(QWidget *parent);
    ~ImageOperationsUI() override;

    ErrorCode scale(const QString &inputfilename, QString &outputfilename);

private:
    class Private;
    Private *const d;
};

#endif // IMAGEOPERATIONSUI_H
