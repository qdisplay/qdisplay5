/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2020 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include "exif.h"

#include <QHash>
#include <QProcess>
#include <QFile>
#include <QFileInfo>
#include <QRegularExpression>
#include <QDebug>

#include "fileoperations.h"
#include "imageoperations.h"

Exif::Exif(QObject *parent) :
    QObject(parent)
{
    /// nothing
}

ErrorCode Exif::copyExif(const QString &originFilename, const QString &destinationFilename) {
    if (!QFile::exists(originFilename)) {
        qWarning() << "Exif::copyExif: origin file does not exist:" << originFilename;
        return FileDoesNotExist;
    } else if (!QFile::exists(destinationFilename)) {
        qWarning() << "Exif::copyExif: destination file does not exist:" << destinationFilename;
        return FileDoesNotExist;
    } else if (!ImageOperations::isJPEG(originFilename)) {
        qWarning() << "Exif::copyExif: can only apply exif operations on JPEG files:" << originFilename;
        return UnsupportedFileType;
    } else if (!ImageOperations::isJPEG(destinationFilename)) {
        qWarning() << "Exif::copyExif: can only apply exif operations on JPEG files:" << destinationFilename;
        return UnsupportedFileType;
    }

    const QString tempDir = FileOperations::createTemporaryDirectory(QStringLiteral("copy-exif"));

    QProcess exiv2(this);
    const QStringList argsExtract {QStringLiteral("-l"), tempDir, QStringLiteral("ex"), originFilename};
    exiv2.start(QStringLiteral("exiv2"), argsExtract);

    const QFileInfo originFilenameInfo(originFilename), destinationFilenameInfo(destinationFilename);
    const QString originBase = originFilenameInfo.fileName().remove(QRegularExpression(QStringLiteral("[.]jpe?g"), QRegularExpression::CaseInsensitiveOption));
    const QString destinationBase = destinationFilenameInfo.fileName().remove(QRegularExpression(QStringLiteral("[.]jpe?g"), QRegularExpression::CaseInsensitiveOption));
    const QString originExv = tempDir + QLatin1Char('/') + originBase + QStringLiteral(".exv");
    const QString destinationExv = tempDir + QLatin1Char('/') + destinationBase + QStringLiteral(".exv");

    if (!exiv2.waitForStarted()) {
        qWarning() << "Exif::copyExif: failed to start command" << FileOperations::commandLine(&exiv2);
        QFile::remove(destinationExv);
        FileOperations::removeDirectory(tempDir);
        return ExternalCommandFailed;
    }
    if (!exiv2.waitForFinished(120000)) {
        qWarning() << "Exif::copyExif: failed to finish command" << FileOperations::commandLine(&exiv2);
        QFile::remove(destinationExv);
        FileOperations::removeDirectory(tempDir);
        return ExternalCommandFailed;
    }
    if (exiv2.exitStatus() != QProcess::NormalExit || exiv2.exitCode() != 0) {
        qWarning() << "Exif::copyExif: process existed with error" << FileOperations::commandLine(&exiv2) << "(exit code" << exiv2.exitCode() << ")";
        const QString errorOutput = QString::fromUtf8(exiv2.readAllStandardError());
        qDebug() << errorOutput;
        QFile::remove(destinationExv);
        FileOperations::removeDirectory(tempDir);
        return ExternalCommandFailed;
    }

    if (!QFile::rename(originExv, destinationExv)) {
        qWarning() << "Could not rename file " << originExv << "to" << destinationExv;
        QFile::remove(destinationExv);
        FileOperations::removeDirectory(tempDir);
        return InvalidArgument;
    }

    const QStringList argsRestore {QStringLiteral("-l"), tempDir, QStringLiteral("in"), destinationFilename};
    exiv2.start(QStringLiteral("exiv2"), argsRestore);

    if (!exiv2.waitForStarted()) {
        qWarning() << "Exif::copyExif: failed to start command" << FileOperations::commandLine(&exiv2);
        QFile::remove(destinationExv);
        FileOperations::removeDirectory(tempDir);
        return ExternalCommandFailed;
    }
    if (!exiv2.waitForFinished(120000)) {
        qWarning() << "Exif::copyExif: failed to finish command" << FileOperations::commandLine(&exiv2);
        QFile::remove(destinationExv);
        FileOperations::removeDirectory(tempDir);
        return ExternalCommandFailed;
    }

    if (exiv2.exitStatus() != QProcess::NormalExit || exiv2.exitCode() != 0) {
        qWarning() << "Exif::copyExif: process existed with error" << FileOperations::commandLine(&exiv2) << "(exit code" << exiv2.exitCode() << ")";
        const QString errorOutput = QString::fromUtf8(exiv2.readAllStandardError());
        qDebug() << errorOutput;
        QFile::remove(destinationExv);
        FileOperations::removeDirectory(tempDir);
        return ExternalCommandFailed;
    }

    QFile::remove(destinationExv);
    FileOperations::removeDirectory(tempDir);
    return NoError;
}

ErrorCode Exif::setOrientation(const QString &jpegFilename, ExifOrientation orientation) {
    if (!QFile::exists(jpegFilename)) {
        qWarning() << "Exif::setOrientation: file does not exist:" << jpegFilename;
        return FileDoesNotExist;
    } else if (!ImageOperations::isJPEG(jpegFilename)) {
        qWarning() << "Exif::setOrientation: file is not a JPEG file:" << jpegFilename;
        return UnsupportedOperation;
    }

    QProcess exiv2;
    const QString cmd = orientation == Clear ? QStringLiteral("del Exif.Image.Orientation") : QString(QStringLiteral("set Exif.Image.Orientation %1")).arg(static_cast<int>(orientation));
    const QStringList args {QStringLiteral("-M"), cmd, jpegFilename};
    exiv2.start(QStringLiteral("exiv2"), args);

    if (!exiv2.waitForStarted()) {
        qWarning() << "Exif::setOrientation: failed to start command" << FileOperations::commandLine(&exiv2);
        return ExternalCommandFailed;
    }
    if (!exiv2.waitForFinished(120000)) {
        qWarning() << "Exif::setOrientation: failed to finish command" << FileOperations::commandLine(&exiv2);
        return ExternalCommandFailed;
    }

    const ErrorCode result = exiv2.exitStatus() == QProcess::NormalExit && exiv2.exitCode() == 0 ? NoError : ExternalCommandFailed;
    if (result != NoError) {
        qWarning() << "Exif::setOrientation: process existed with error" << FileOperations::commandLine(&exiv2) << "(exit code" << exiv2.exitCode() << ")";
        const QString errorOutput = QString::fromUtf8(exiv2.readAllStandardError());
        qDebug() << errorOutput;
        return result;
    }

    return result;
}
