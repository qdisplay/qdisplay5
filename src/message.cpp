/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2015 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include "message.h"

#include <QDebug>
#include <QDateTime>
#include <QTimer>
#include <QPainter>

#include "imagewidget.h"

#define convertTime(x) static_cast<int>(((x)-1500000000000)/100)

class Message::Private {
public:
    struct TimedMessage {
        QString msg;
        int expirationTime;
        QColor fontColor;

        TimedMessage():
            expirationTime(0)
        {
            /// nothing
        }

        TimedMessage(QString m, int et, QColor fc) :
            msg(m), expirationTime(et), fontColor(fc)
        {
            /// nothing
        }
    };
    QVector<TimedMessage> timedMessages;

    ImageWidget *imageWidget;
    QTimer *timer;

    Private(ImageWidget *iw, Message *message) :
        imageWidget(iw), timer(new QTimer(iw))
    {
        timer->stop();
        QObject::connect(timer, &QTimer::timeout, message, &Message::updateTimedMessages);
    }

    void paint(QPainter &painter) {
        const int height = imageWidget->fontMetrics().height() + 8;
        const int width = imageWidget->width() - 8;
        int y = imageWidget->height() - 4 - height;

        QColor baseBgColor = QColor(0, 0, 0, 160);

        painter.setRenderHint(QPainter::Antialiasing, true);
        painter.setRenderHint(QPainter::SmoothPixmapTransform, true);

        const int currentTime = convertTime(QDateTime::currentDateTime().toMSecsSinceEpoch());
        for (QVector<Private::TimedMessage>::Iterator it = timedMessages.begin(); it != timedMessages.end(); ++it) {
            QColor fgColor = (*it).fontColor;
            fgColor.setAlpha(fgColor.alpha()*qMin(32, qMax(0, (*it).expirationTime - currentTime)) / 32);
            QColor bgColor = baseBgColor;
            bgColor.setAlpha(bgColor.alpha()*qMin(32, qMax(0, (*it).expirationTime - currentTime)) / 32);
            painter.setBrush(QBrush(bgColor));
            painter.setPen(QPen(fgColor, 1, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));

            painter.fillRect(imageWidget->width() - width - 4, y, width, height, bgColor);
            painter.drawText(imageWidget->width() - width - 8, y, width - 4, height, Qt::AlignRight | Qt::AlignVCenter, (*it).msg);
            y -= height + 4;
        }
    }
};

Message::Message(ImageWidget *imageWidget) :
    QObject(imageWidget), d(new Private(imageWidget, this))
{
    /// nothing
}

Message::~Message() {
    delete d;
}

void Message::showMessage(const QString &messageText, int duration, QColor fontColor) {
    Private::TimedMessage message(messageText, convertTime(QDateTime::currentDateTime().toMSecsSinceEpoch() + duration * 1000), fontColor);
    d->timedMessages.append(message);
    if (!d->timer->isActive())
        d->timer->start(100);

    d->imageWidget->update();
}

void Message::paintEvent(QPainter &painter) {
    d->paint(painter);
}

void Message::updateTimedMessages() {
    const qint64 currentTime = convertTime(QDateTime::currentDateTime().toMSecsSinceEpoch());
    for (QVector<Private::TimedMessage>::Iterator it = d->timedMessages.begin(); it != d->timedMessages.end();) {
        if ((*it).expirationTime <= currentTime)
            it = d->timedMessages.erase(it);
        else
            ++it;
    }
    if (d->timedMessages.isEmpty())
        d->timer->stop();

    d->imageWidget->update();
}
