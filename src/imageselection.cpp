/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2019 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include "imageselection.h"

#include <QMouseEvent>
#include <QRect>
#include <QDebug>

#define round(x) static_cast<int>((x)+0.5)

class ImageSelection::Private {
public:
    QRect currentSelection;
    int maxWidth;
    int maxHeight;
    Qt::CursorShape previousCursor;

    Private() :
        maxWidth(0), maxHeight(0), previousCursor(Qt::CrossCursor)
    {
        /// nothing
    }

    bool near(int x1, int y1, int x2, int y2) const {
        const int dx = x1 - x2;
        const int dy = y1 - y2;
        const int d = dx * dx + dy * dy;
        return d < 32;
    }
};

ImageSelection::ImageSelection(ImageWidget *parent) :
    QObject(parent), d(new Private())
{
    /// nothing
}

ImageSelection::~ImageSelection() {
    delete d;
}

void ImageSelection::clear() {
    d->currentSelection = QRect();
    emit selectionChanged(d->currentSelection);
}

void ImageSelection::resizeEvent(int numerator, int denominator) {
    if (d->currentSelection.isValid()) {

        if (denominator > 0) {
            d->currentSelection = QRect(d->currentSelection.left() * numerator / denominator,
                                        d->currentSelection.top() * numerator / denominator,
                                        d->currentSelection.width() * numerator / denominator,
                                        d->currentSelection.height() * numerator / denominator).normalized();
        } else {
            qWarning() << "denominator is 0 in ImageSelection::resizeEvent(" << numerator << "," << denominator << ")";
            d->currentSelection = d->currentSelection.normalized();
        }
        emit selectionChanged(d->currentSelection);
    }
}

void ImageSelection::mouseMoveEvent(QMouseEvent *event) {
    if (event->buttons() == Qt::LeftButton) {
        const int x = qMin(qMax(0, round(event->position().x())), d->maxWidth);
        const int y = qMin(qMax(0, round(event->position().y())), d->maxHeight);
        d->currentSelection.setRight(x);
        d->currentSelection.setBottom(y);
        emit selectionChanged(d->currentSelection.normalized());
    }

    const int x = qMin(qMax(0, round(event->position().x())), d->maxWidth);
    const int y = qMin(qMax(0, round(event->position().y())), d->maxHeight);
    const QRect n = d->currentSelection.normalized();

    if (d->near(x, y, n.left(), n.top())) {
        if (d->previousCursor != Qt::SizeFDiagCursor)
            emit mouseCursorChange(d->previousCursor = Qt::SizeFDiagCursor);
    } else if (d->near(x, y, n.left(), n.bottom())) {
        if (d->previousCursor != Qt::SizeBDiagCursor)
            emit mouseCursorChange(d->previousCursor = Qt::SizeBDiagCursor);
    } else if (d->near(x, y, n.right(), n.top())) {
        if (d->previousCursor != Qt::SizeBDiagCursor)
            emit mouseCursorChange(d->previousCursor = Qt::SizeBDiagCursor);
    } else if (d->near(x, y, n.right(), n.bottom())) {
        if (d->previousCursor != Qt::SizeFDiagCursor)
            emit mouseCursorChange(d->previousCursor = Qt::SizeFDiagCursor);
    } else if (d->previousCursor != Qt::CrossCursor)
        emit mouseCursorChange(d->previousCursor = Qt::CrossCursor);
}

void ImageSelection::mousePressEvent(QMouseEvent *event) {
    if (event->buttons() == Qt::LeftButton) {
        const int x = qMin(qMax(0, round(event->position().x())), d->maxWidth);
        const int y = qMin(qMax(0, round(event->position().y())), d->maxHeight);
        const QRect n = d->currentSelection.normalized();
        if (d->near(x, y, n.left(), n.top()))
            d->currentSelection = QRect(QPoint(n.right(), n.bottom()), QPoint(x, y));
        else if (d->near(x, y, n.left(), n.bottom()))
            d->currentSelection = QRect(QPoint(n.right(), n.top()), QPoint(x, y));
        else if (d->near(x, y, n.right(), n.top()))
            d->currentSelection = QRect(QPoint(n.left(), n.bottom()), QPoint(x, y));
        else if (d->near(x, y, n.right(), n.bottom()))
            d->currentSelection = QRect(QPoint(n.left(), n.top()), QPoint(x, y));
        else
            d->currentSelection = QRect(QPoint(x, y), QPoint(x, y));

        emit selectionChanged(d->currentSelection.normalized());
    }
}

void ImageSelection::setMax(int maxWidth, int maxHeight) {
    d->maxWidth = maxWidth;
    d->maxHeight = maxHeight;
}

QRect ImageSelection::selection() const {
    return d->currentSelection.normalized();
}
