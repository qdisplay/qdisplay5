/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2019 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#ifndef EXIF_H
#define EXIF_H

#include <QObject>

#include "errorcodes.h"

class Exif : public QObject
{
    Q_OBJECT

public:
    explicit Exif(QObject *parent);

    enum ExifOrientation {Clear = 0, Normal = 1, HorizontalMirror = 2, DoubleMirror = 3, VerticalMirror = 4, HorizontalMirrorRotLeft = 5, RotLeft = 6, HorizontalMirrorRotRight = 7, RotRight = 8};

    ErrorCode backupExif(const QString &jpegFilename);
    ErrorCode restoreExif(const QString &jpegFilename);
    ErrorCode copyExif(const QString &originFilename, const QString &destinationFilename);

    ErrorCode setOrientation(const QString &jpegFilename, ExifOrientation orientation);
};

#endif // EXIF_H
