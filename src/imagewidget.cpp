/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2017 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include "imagewidget.h"

#include <QKeyEvent>
#include <QPaintEvent>
#include <QPainter>
#include <QPixmap>
#include <QMovie>
#include <QSize>
#include <QAction>
#include <QApplication>
#include <QPalette>
#include <QDebug>

#include "imageselection.h"
#include "message.h"

class ImageWidget::Private {
private:
    ImageWidget *p;

public:
    QRect manualOverrideSelectionImageSize;

    Private(ImageWidget *parent) :
        p(parent), isFullscreen(false), fitMode(ImageWidget::fitPreferOriginalSize)
    {
        imageSelection = new ImageSelection(parent);
        message = new Message(parent);
        scaleNumerator = scaleDenominator = 1;

        createActions(parent);
    }

    void createActions(ImageWidget *parent) {
        actionGrayscale = new QAction(QIcon::fromTheme(QStringLiteral("folder-grey")), QStringLiteral("&Grayscale"), parent);
        actionGrayscale->setShortcut(Qt::Key_G);
        parent->addAction(actionGrayscale);
        QObject::connect(actionGrayscale, &QAction::triggered, parent, &ImageWidget::actionTriggered);

        actionScale = new QAction(QIcon::fromTheme(QStringLiteral("transform-scale")), QStringLiteral("&Scale and Quality"), parent);
        actionScale->setShortcut(Qt::Key_S);
        parent->addAction(actionScale);
        QObject::connect(actionScale, &QAction::triggered, parent, &ImageWidget::actionTriggered);

        actionCrop = new QAction(QIcon::fromTheme(QStringLiteral("transform-crop-and-resize")), QStringLiteral("&Crop"), parent);
        actionCrop->setShortcut(Qt::Key_C);
        parent->addAction(actionCrop);
        QObject::connect(actionCrop, &QAction::triggered, parent, &ImageWidget::actionTriggered);
        actionCrop->setEnabled(false);

        actionARAWCRS = new QAction(QStringLiteral("Aspect ratio-aware crop of right side"), parent);
        actionARAWCRS->setShortcut(static_cast<Qt::Key>(Qt::SHIFT) + Qt::Key_C);
        parent->addAction(actionARAWCRS);
        QObject::connect(actionARAWCRS, &QAction::triggered, parent, &ImageWidget::actionTriggered);

        actionBlackBar = new QAction(QIcon::fromTheme(QStringLiteral("align-horizontal-top-out")), QStringLiteral("&Black bar"), parent);
        actionBlackBar->setShortcut(Qt::Key_B);
        parent->addAction(actionBlackBar);
        QObject::connect(actionBlackBar, &QAction::triggered, parent, &ImageWidget::actionTriggered);
        actionBlackBar->setEnabled(false);

        actionBlur = new QAction(QIcon::fromTheme(QStringLiteral("blur")), QStringLiteral("Bl&ur"), parent);
        actionBlur->setShortcut(static_cast<Qt::Key>(Qt::SHIFT) + Qt::Key_B);
        parent->addAction(actionBlur);
        QObject::connect(actionBlur, &QAction::triggered, parent, &ImageWidget::actionTriggered);
        actionBlur->setEnabled(false);

        actionRotateLeft90 = new QAction(QIcon::fromTheme(QStringLiteral("object-rotate-left")), QString(QStringLiteral("Rotate Left 90%1")).arg(QChar(0x00b0)), parent);
        actionRotateLeft90->setShortcut(static_cast<Qt::Key>(Qt::ALT) + Qt::Key_Left);
        parent->addAction(actionRotateLeft90);
        QObject::connect(actionRotateLeft90, &QAction::triggered, parent, &ImageWidget::actionTriggered);

        actionRotateRight90 = new QAction(QIcon::fromTheme(QStringLiteral("object-rotate-right")), QString(QStringLiteral("Rotate Right 90%1")).arg(QChar(0x00b0)), parent);
        actionRotateRight90->setShortcut(static_cast<Qt::Key>(Qt::ALT) + Qt::Key_Right);
        parent->addAction(actionRotateRight90);
        QObject::connect(actionRotateRight90, &QAction::triggered, parent, &ImageWidget::actionTriggered);

        QAction *separator = new QAction(parent);
        separator->setSeparator(true);
        parent->addAction(separator);

        actionTouch = new QAction(QIcon::fromTheme(QStringLiteral("clock")), QStringLiteral("&Touch"), parent);
        actionTouch->setShortcut(Qt::Key_T);
        parent->addAction(actionTouch);
        QObject::connect(actionTouch, &QAction::triggered, parent, &ImageWidget::actionTriggered);

        actionReload = new QAction(QIcon::fromTheme(QStringLiteral("edit-redo")), QStringLiteral("&Reload"), parent);
        actionReload->setShortcut(Qt::Key_R);
        parent->addAction(actionReload);
        QObject::connect(actionReload, &QAction::triggered, parent, &ImageWidget::actionTriggered);

        actionUndo = new QAction(QIcon::fromTheme(QStringLiteral("edit-undo")), QStringLiteral("&Undo"), parent);
        actionUndo->setShortcut(Qt::Key_U);
        parent->addAction(actionUndo);
        QObject::connect(actionUndo, &QAction::triggered, parent, &ImageWidget::actionTriggered);
        actionUndo->setEnabled(false);

        actionRandomizeFileOrder = new QAction(QIcon::fromTheme(QStringLiteral("document-swap")), QStringLiteral("&Randomize order"), parent);
        actionRandomizeFileOrder->setShortcut(static_cast<Qt::Key>(Qt::SHIFT) + Qt::Key_R);
        parent->addAction(actionRandomizeFileOrder);
        QObject::connect(actionRandomizeFileOrder, &QAction::triggered, parent, &ImageWidget::actionTriggered);

        actionFitMode = new QAction(QIcon::fromTheme(QStringLiteral("zoom-fit-best")), QStringLiteral("Toggle &fit image"), parent);
        actionFitMode->setShortcut(Qt::Key_F);
        actionFitMode->setCheckable(true);
        parent->addAction(actionFitMode);
        QObject::connect(actionFitMode, &QAction::triggered, parent, &ImageWidget::toggleFitMode);

        actionFullscreen = new QAction(QIcon::fromTheme(QStringLiteral("view-fullscreen")), QStringLiteral("Toggle fu&llscreen"), parent);
        actionFullscreen->setShortcut(static_cast<Qt::Key>(Qt::SHIFT) + Qt::Key_F);
        actionFullscreen->setCheckable(true);
        parent->addAction(actionFullscreen);
        QObject::connect(actionFullscreen, &QAction::triggered, parent, &ImageWidget::actionTriggered);

        separator = new QAction(parent);
        separator->setSeparator(true);
        parent->addAction(separator);

        actionFileCopyTo = new QAction(QIcon::fromTheme(QStringLiteral("document-save-as")), QStringLiteral("C&opy to ..."), parent);
        actionFileCopyTo->setShortcut(Qt::Key_V);
        parent->addAction(actionFileCopyTo);
        QObject::connect(actionFileCopyTo, &QAction::triggered, parent, &ImageWidget::actionTriggered);

        actionFileMoveToAs = new QAction(QIcon::fromTheme(QStringLiteral("document-save-all")), QStringLiteral("&Move to ..."), parent);
        actionFileMoveToAs->setShortcut(Qt::Key_M);
        parent->addAction(actionFileMoveToAs);
        QObject::connect(actionFileMoveToAs, &QAction::triggered, parent, &ImageWidget::actionTriggered);

        actionConvertImageType = new QAction(QIcon::fromTheme(QStringLiteral("TODO")), QStringLiteral("Convert &image type ..."), parent);
        actionConvertImageType->setShortcut(Qt::Key_I);
        parent->addAction(actionConvertImageType);
        QObject::connect(actionConvertImageType, &QAction::triggered, parent, &ImageWidget::actionTriggered);

        actionDelete = new QAction(QIcon::fromTheme(QStringLiteral("edit-delete")), QStringLiteral("&Delete"), parent);
        actionDelete->setShortcut(Qt::Key_Delete);
        parent->addAction(actionDelete);
        QObject::connect(actionDelete, &QAction::triggered, parent, &ImageWidget::actionTriggered);

        separator = new QAction(parent);
        separator->setSeparator(true);
        parent->addAction(separator);

        actionQuit = new QAction(QIcon::fromTheme(QStringLiteral("application-exit")), QStringLiteral("&Quit"), parent);
        actionQuit->setShortcut(Qt::Key_Q);
        parent->addAction(actionQuit);
        QObject::connect(actionQuit, &QAction::triggered, parent, &ImageWidget::actionTriggered);
    }

    void updateMovieScaledSize() {
        if (!movie.isNull()) {
            const int scaleFactor = fitMode == fitMaximumScale ? qMax(1, qMin(p->width() / movieSize.width(), p->height() / movieSize.height())) : 1;
            movie->setScaledSize(QSize(movieSize.width()*scaleFactor, movieSize.height()*scaleFactor));
        }
    }

    bool isFullscreen;
    FitMode fitMode;
    QPixmap originalPixmap;
    QSharedPointer<QMovie> movie;
    QSize movieSize;
    QPixmap moviePixmap;
    int scaleNumerator, scaleDenominator;
    ImageSelection *imageSelection;
    QRect oldSelection;

    Message *message;

    QAction *actionGrayscale, *actionScale, *actionCrop, *actionARAWCRS, *actionBlackBar, *actionBlur, *actionRotateLeft90, *actionRotateRight90,
            *actionTouch, *actionReload, *actionUndo, *actionRandomizeFileOrder, *actionFitMode,
            *actionDelete, *actionFileCopyTo, *actionFileMoveToAs, *actionQuit, *actionFullscreen,
            *actionConvertImageType;
};

ImageWidget::ImageWidget(QWidget *parent) :
    QWidget(parent), d(new Private(this))
{
    setCursor(Qt::CrossCursor);
    setFocusPolicy(Qt::WheelFocus);
    setMouseTracking(true);
    setContextMenuPolicy(Qt::ActionsContextMenu);

    connect(d->imageSelection, &ImageSelection::selectionChanged, this, &ImageWidget::visualSelectionChanged);
    connect(d->imageSelection, &ImageSelection::mouseCursorChange, this, &ImageWidget::mouseCursorChanged);
}

ImageWidget::~ImageWidget() {
    delete d;
}

void ImageWidget::setImage(const QPixmap &pixmap) {
    d->actionDelete->setEnabled(true);
    d->actionGrayscale->setEnabled(true);
    d->actionRandomizeFileOrder->setEnabled(true);
    d->actionReload->setEnabled(true);
    d->actionRotateLeft90->setEnabled(true);
    d->actionRotateRight90->setEnabled(true);
    d->actionScale->setEnabled(true);
    d->actionTouch->setEnabled(true);

    d->originalPixmap = pixmap;
    d->movie.clear();
    d->imageSelection->clear();
    d->manualOverrideSelectionImageSize = QRect();
    repaint();
}

void ImageWidget::setMovie(QSharedPointer<QMovie> &movie) {
    d->actionDelete->setEnabled(true);
    d->actionGrayscale->setEnabled(false);
    d->actionRandomizeFileOrder->setEnabled(true);
    d->actionReload->setEnabled(true);
    d->actionRotateLeft90->setEnabled(false);
    d->actionRotateRight90->setEnabled(false);
    d->actionScale->setEnabled(false);
    d->actionTouch->setEnabled(true);

    d->originalPixmap = QPixmap();
    d->movie = movie;
    d->imageSelection->clear();
    d->manualOverrideSelectionImageSize = QRect();

    connect(d->movie.data(), &QMovie::frameChanged, this, &ImageWidget::movieFrameChanged);
    connect(d->movie.data(), &QMovie::error, this, &ImageWidget::movieErrors);
    d->movie->setCacheMode(QMovie::CacheAll);
    d->movie->start();
    d->movieSize = d->movie->currentPixmap().size();
    d->updateMovieScaledSize();
}

void ImageWidget::unsetImage(bool nonImageFile) {
    d->actionDelete->setEnabled(nonImageFile);
    d->actionGrayscale->setEnabled(false);
    d->actionRandomizeFileOrder->setEnabled(false);
    d->actionReload->setEnabled(false);
    d->actionRotateLeft90->setEnabled(false);
    d->actionRotateRight90->setEnabled(false);
    d->actionScale->setEnabled(false);
    d->actionTouch->setEnabled(false);

    d->originalPixmap = QPixmap();
    d->movie.clear();
    d->imageSelection->clear();
    d->manualOverrideSelectionImageSize = QRect();
    repaint();
}

bool ImageWidget::toggleFullscreenMode() {
    d->isFullscreen = !d->isFullscreen;
    d->actionFullscreen->setChecked(d->isFullscreen);
    return d->isFullscreen;
}

void ImageWidget::showMessage(const QString &messageText, int duration, QColor fontColor) {
    d->message->showMessage(messageText, duration, fontColor);
}

QSize ImageWidget::imageSize() const {
    if (!d->movie.isNull())
        return QSize(); // TODO
    else if (!d->originalPixmap.isNull())
        return d->originalPixmap.size();
    else
        return QSize();
}

QSize ImageWidget::visualImageSize() const
{
    const QSize result{d->originalPixmap.width() * d->scaleDenominator / d->scaleNumerator, d->originalPixmap.height() * d->scaleDenominator / d->scaleNumerator};
    return result;
}

QRect ImageWidget::selection() const {
    if (d->manualOverrideSelectionImageSize.width() > 1 && d->manualOverrideSelectionImageSize.height() > 1)
        return d->manualOverrideSelectionImageSize;

    if (!d->movie.isNull() || d->originalPixmap.isNull())
        return QRect(); ///< no selection unless pixmap in use
    if (d->isFullscreen)
        return QRect(); ///< no selection in fullscreen mode

    const QRect visualSelection = d->imageSelection->selection();
    if (d->scaleDenominator < 1) {
        d->scaleDenominator = d->scaleNumerator = 1;
        qWarning() << "denominator is 0 in ImageWidget::selection: d->scaleDenominator=" << d->scaleDenominator << "  d->scaleNumerator=" << d->scaleNumerator << ")";
    }
    QRect result = QRect(qMax(0, visualSelection.left() * d->scaleNumerator / d->scaleDenominator),
                         qMax(0, visualSelection.top() * d->scaleNumerator / d->scaleDenominator),
                         (visualSelection.width() + 1) * d->scaleNumerator / d->scaleDenominator,
                         (visualSelection.height() + 1) * d->scaleNumerator / d->scaleDenominator);
    result.setRight(qMin(d->originalPixmap.width(), result.right()));
    result.setBottom(qMin(d->originalPixmap.height(), result.bottom()));
    return result;
}

void ImageWidget::setARAWCRSselection()
{
// Substract 0.5% from 'a', but as all is done in int,
// add 500 before dividing by 1000. Then, do some shifting
// right and left by 2 to get values divisible by 4.
// Requires to have an edge length of more than 100
// to have an effect
#define magicFormula(a) (((((a) * 995 + 500) / 1000) >> 2) << 2)
    const QPoint p{magicFormula(d->originalPixmap.width()),magicFormula(d->originalPixmap.height())};
    d->manualOverrideSelectionImageSize = QRect(QPoint(0, 0), p);
}

void ImageWidget::paintEvent(QPaintEvent *event) {
    const QRect selection = d->imageSelection->selection().adjusted(0, 0, -1, -1);

    QPainter painter(this);

    painter.setRenderHint(QPainter::Antialiasing, false);
    painter.setRenderHint(QPainter::SmoothPixmapTransform, false);
    painter.setPen(Qt::NoPen);

    /// Draw background
    if (d->isFullscreen) {
        painter.setBrush(QBrush(Qt::black, Qt::SolidPattern));
        painter.drawRect(event->rect());
    } else {
        painter.setBrush(QBrush(Qt::lightGray, Qt::SolidPattern));
        painter.drawRect(event->rect());
        painter.setBrush(QBrush(Qt::white, Qt::BDiagPattern));
        painter.drawRect(event->rect());
    }

    if (!d->originalPixmap.isNull()) {
        painter.setRenderHint(QPainter::Antialiasing, true);
        painter.setRenderHint(QPainter::SmoothPixmapTransform, true);

        if (d->originalPixmap.width() < 1 || d->originalPixmap.height() < 1)
            qWarning() << "pixmap size is invalid: " << d->originalPixmap.width() << "x" << d->originalPixmap.height();

        int w = qMax(d->originalPixmap.width(), 1);
        int h = qMax(d->originalPixmap.height(), 1);
        if (w > width() || h > height() || d->fitMode == fitMaximumScale) {
            if (static_cast<double>(w) / width() > static_cast<double>(h) / height()) {
                d->scaleNumerator = w;
                d->scaleDenominator = width();
                h = h * width() / w;
                w = width();
            } else {
                d->scaleNumerator = h;
                d->scaleDenominator = height();
                w = w * height() / h;
                h = height();
            }
            const int x = d->isFullscreen ? (width() - w) / 2 : 0;
            const int y = d->isFullscreen ? (height() - h) / 2 : 0;
            painter.drawPixmap(x, y, w, h, d->originalPixmap);
            d->imageSelection->setMax(w - 1, h - 1);

            emit zoomPercentChanged(d->scaleDenominator * 100 / d->scaleNumerator);
        } else {
            const int x = d->isFullscreen ? (width() - w) / 2 : 0;
            const int y = d->isFullscreen ? (height() - h) / 2 : 0;
            painter.drawPixmap(x, y, d->originalPixmap);
            d->scaleNumerator = d->scaleDenominator = 1;
            d->imageSelection->setMax(d->originalPixmap.width() - 1, d->originalPixmap.height() - 1);

            emit zoomPercentChanged(100);
        }

        if (selection.isValid() && !d->isFullscreen) {
            painter.setBrush(Qt::NoBrush);

            const int cx = w / 2, cy = h / 2;
            const QVector<QPoint> selectionCorners{
                QPoint{selection.left(), selection.top()}, QPoint{selection.right(), selection.top()}, QPoint{selection.left(), selection.bottom()}, QPoint{selection.right(), selection.bottom()}
            };
            for (const auto &coordpair : {
            QPair<QPoint, QPoint> {{0, 0}, {cx, cy}}, QPair<QPoint, QPoint> {{cx, cy}, {w, h}}, QPair<QPoint, QPoint> {{0, h}, {cx, cy}}, QPair<QPoint, QPoint> {{w, 0}, {cx, cy}}
                }) {
                const QPoint coord1 = coordpair.first, coord2 = coordpair.second;
                const bool inverse = coordpair.first.x() > coordpair.second.x() || coordpair.first.y() > coordpair.second.y();
                const int minx = qMin(coord1.x(), coord2.x()), miny = qMin(coord1.y(), coord2.y());
                const int maxx = qMax(coord1.x(), coord2.x()), maxy = qMax(coord1.y(), coord2.y());
                const int dx = ((inverse ? w - coordpair.first.x() : coordpair.first.x()) + (inverse ? w - coordpair.second.x() : coordpair.second.x())) / 2;
                const int dy = (coordpair.first.y() + coordpair.second.y()) / 2;
                painter.setPen(QPen(QColor(255, 255, 255, 127), 3, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
                for (const auto &selectioncorner : selectionCorners) {
                    const int diff = abs(dx * selectioncorner.y() - dy * (inverse ? w - selectioncorner.x() : selectioncorner.x()));
                    if (selectioncorner.x() >= minx && selectioncorner.y() >= miny && selectioncorner.x() <= maxx && selectioncorner.y() <= maxy && diff < 1024) {
                        painter.setPen(QPen(QColor(31, 255, 63, 191), 3, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
                        break;
                    }
                }
                painter.drawLine(coord1.x(), coord1.y(), coord2.x(), coord2.y());
            }

            painter.setRenderHint(QPainter::Antialiasing, false);
            const int ratio = abs(selection.width() * h - selection.height() * w);
            static const QPen greenPen{QColor(31, 255, 31, 191), 3, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin};
            static const QPen yellowPen{QColor(223, 223, 0, 159), 3, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin};
            if (ratio < 8192) {
                if (ratio < 768)
                    painter.setPen(greenPen);
                else
                    painter.setPen(yellowPen);
                painter.drawRect(selection);
            } else {
                const int ratioFlipped = abs(selection.width() * w - selection.height() * h);
                if (ratioFlipped < 8192) {
                    // Color-hinting if cutting a portrait image from a landscape image or vice versa
                    if (ratioFlipped < 768)
                        painter.setPen(greenPen);
                    else
                        painter.setPen(yellowPen);
                    painter.drawRect(selection);
                }
            }

            painter.setPen(QPen(QColor(255, 255, 255, 255), 1, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
            painter.drawRect(selection);
            painter.setPen(QPen(QColor(0, 0, 0, 127), 1, Qt::DotLine, Qt::SquareCap, Qt::MiterJoin));
            painter.drawRect(selection);
        }
    } else if (!d->movie.isNull() && !d->moviePixmap.isNull()) {
        painter.setRenderHint(QPainter::Antialiasing, false);
        painter.setRenderHint(QPainter::SmoothPixmapTransform, false);

        const int x = d->isFullscreen ? (width() - d->moviePixmap.width()) / 2 : 0;
        const int y = d->isFullscreen ? (height() - d->moviePixmap.height()) / 2 : 0;
        painter.drawPixmap(x, y, d->moviePixmap);
        d->scaleNumerator = d->scaleDenominator = 1;

        QColor progressColor = QApplication::palette(this).color(QPalette::Highlight);
        progressColor.setAlpha(127);
        painter.setPen(Qt::NoPen);
        painter.setBrush(QBrush(progressColor));
        const int h = 4;
        const int w = d->movie->currentFrameNumber() * width() / qMax(1, d->movie->frameCount() - 1);
        painter.drawRect(0, height() - h, w, h);

        emit zoomPercentChanged(100);
    }

    d->message->paintEvent(painter);
}

void ImageWidget::resizeEvent(QResizeEvent *event) {
    Q_UNUSED(event)
    // TODO compute both arguments from size, oldSize, and pixmap size
    //d->imageSelection->resizeEvent(1,1);
    d->imageSelection->clear();
    d->manualOverrideSelectionImageSize = QRect();

    d->updateMovieScaledSize();
}

void ImageWidget::keyPressEvent(QKeyEvent *event) {
    if (event->modifiers() == Qt::NoModifier) {
        switch (event->key()) {
        case Qt::Key_Space:
        case Qt::Key_PageDown:
            emit commandTriggered(commandNextImage);
            break;
        case Qt::Key_Backspace:
        case Qt::Key_PageUp:
            emit commandTriggered(commandPrevImage);
            break;
        case Qt::Key_Home:
            emit commandTriggered(commandGotoFirst);
            break;
        case Qt::Key_End:
            emit commandTriggered(commandGotoLast);
            break;
        }
    } else if (event->modifiers() == Qt::ShiftModifier) {
        switch (event->key()) {
        case Qt::Key_Space:
        case Qt::Key_PageDown:
            emit commandTriggered(commandNext10Image);
            break;
        case Qt::Key_Backspace:
        case Qt::Key_PageUp:
            emit commandTriggered(commandPrev10Image);
            break;
        }
    } else if (event->modifiers() == Qt::ControlModifier) {
        switch (event->key()) {
        case Qt::Key_Space:
        case Qt::Key_PageDown:
            emit commandTriggered(commandNext100Image);
            break;
        case Qt::Key_Backspace:
        case Qt::Key_PageUp:
            emit commandTriggered(commandPrev100Image);
            break;
        }
    }
}

void ImageWidget::wheelEvent(QWheelEvent *event) {
    if (event->angleDelta().y() < 0) {
        if (event->modifiers() == Qt::NoModifier)
            emit commandTriggered(commandNextImage);
        else if (event->modifiers() == Qt::ShiftModifier)
            emit commandTriggered(commandNext10Image);
        else if (event->modifiers() == Qt::ControlModifier)
            emit commandTriggered(commandNext100Image);
    } else if (event->angleDelta().y() > 0) {
        if (event->modifiers() == Qt::NoModifier)
            emit commandTriggered(commandPrevImage);
        else if (event->modifiers() == Qt::ShiftModifier)
            emit commandTriggered(commandPrev10Image);
        else if (event->modifiers() == Qt::ControlModifier)
            emit commandTriggered(commandPrev100Image);
    }
}

void ImageWidget::mouseMoveEvent(QMouseEvent *event) {
    if (!d->originalPixmap.isNull() && !d->isFullscreen) {
        d->imageSelection->mouseMoveEvent(event);
        d->manualOverrideSelectionImageSize = QRect();
    }
}

void ImageWidget::mousePressEvent(QMouseEvent *event) {
    if (!d->originalPixmap.isNull() && !d->isFullscreen) {
        d->imageSelection->mousePressEvent(event);
        d->manualOverrideSelectionImageSize = QRect();
    }
}

void ImageWidget::visualSelectionChanged(QRect selection) {
    update();

    const bool hasBlockSelection = selection.isValid() && selection.height() > 3 && selection.width() > 3;
    d->actionCrop->setEnabled(hasBlockSelection);
    d->actionBlackBar->setEnabled(hasBlockSelection);
    d->actionBlur->setEnabled(hasBlockSelection);
    d->oldSelection = selection;
}

void ImageWidget::mouseCursorChanged(Qt::CursorShape cursor) {
    setCursor(cursor);
}

void ImageWidget::movieFrameChanged(int) {
    if (!d->movie.isNull()) {
        d->moviePixmap = d->movie->currentPixmap();
        repaint();
    }
}

void ImageWidget::movieErrors(QImageReader::ImageReaderError error) {
    const QString fileName = d->movie.isNull() ? QStringLiteral("not known") : QString(QStringLiteral("\"%1\"")).arg(d->movie->fileName());
    switch (error) {
    case QImageReader::FileNotFoundError:
        showMessage(QString(QStringLiteral("Movie: file not found (file %1)")).arg(fileName), 5, Qt::red);
        break;
    case QImageReader::DeviceError:
        showMessage(QString(QStringLiteral("Movie: device error (file %1)")).arg(fileName), 5, Qt::red);
        break;
    case QImageReader::UnsupportedFormatError:
        showMessage(QString(QStringLiteral("Movie: unsupported format (file %1)")).arg(fileName), 5, Qt::red);
        break;
    case QImageReader::InvalidDataError:
        showMessage(QString(QStringLiteral("Movie: invalid data (file %1)")).arg(fileName), 5, Qt::red);
        break;
    case QImageReader::UnknownError:
        showMessage(QString(QStringLiteral("Movie: unknown error (file %1)")).arg(fileName), 5, Qt::red);
        break;
    }

    if (!d->movie.isNull())
        d->movie->stop();
}

void ImageWidget::actionTriggered() {
    QAction *sendingAction = qobject_cast<QAction *>(sender());
    if (sendingAction == d->actionScale && !d->originalPixmap.isNull())
        emit commandTriggered(commandScaleAndQuality);
    else if (sendingAction == d->actionCrop && !d->originalPixmap.isNull())
        emit commandTriggered(commandCrop);
    else if (sendingAction == d->actionARAWCRS && !d->originalPixmap.isNull())
        emit commandTriggered(commandARAWCRS);
    else if (sendingAction == d->actionBlackBar && !d->originalPixmap.isNull())
        emit commandTriggered(commandBlackBar);
    else if (sendingAction == d->actionBlur && !d->originalPixmap.isNull())
        emit commandTriggered(commandBlur);
    else if (sendingAction == d->actionGrayscale && !d->originalPixmap.isNull())
        emit commandTriggered(commandGrayScale);
    else if (sendingAction == d->actionRotateLeft90 && !d->originalPixmap.isNull())
        emit commandTriggered(commandRotateLeft90);
    else if (sendingAction == d->actionRotateRight90 && !d->originalPixmap.isNull())
        emit commandTriggered(commandRotateRight90);
    else if (sendingAction == d->actionReload)
        emit commandTriggered(commandReload);
    else if (sendingAction == d->actionDelete)
        emit commandTriggered(commandDelete);
    else if (sendingAction == d->actionTouch)
        emit commandTriggered(commandTouch);
    else if (sendingAction == d->actionUndo)
        emit commandTriggered(commandUndo);
    else if (sendingAction == d->actionRandomizeFileOrder)
        emit commandTriggered(commandRandomizeFileOrder);
    else if (sendingAction == d->actionFileCopyTo)
        emit commandTriggered(commandFileCopyTo);
    else if (sendingAction == d->actionFileMoveToAs)
        emit commandTriggered(commandFileMoveTo);
    else if (sendingAction == d->actionConvertImageType)
        emit commandTriggered(commandConvertImageType);
    else if (sendingAction == d->actionQuit)
        emit commandTriggered(commandQuit);
    else if (sendingAction == d->actionFullscreen)
        emit commandTriggered(commandFullscreen);
    else
        qWarning() << "ImageWidget::actionTriggered: unknown sender";
}

void ImageWidget::toggleFitMode() {
    d->fitMode = static_cast<FitMode>(d->fitMode + 1);
    if (d->fitMode >= fitInvalid)
        d->fitMode = static_cast<FitMode>(0);

    QString fitModeMessage;
    switch (d->fitMode) {
    case fitMaximumScale:
        fitModeMessage = QStringLiteral("Scaling to maximum size");
        break;
    case fitPreferOriginalSize:
        fitModeMessage = QStringLiteral("Keeping original size, scaling only when necessary");
        break;
    default:
        /// Ignore any other case such as fitInvalid (should never happen)
        break;
    }

    showMessage(fitModeMessage, 4);

    d->updateMovieScaledSize();
    repaint();
}

void ImageWidget::availableUndoStepsChanged(int count) {
    d->actionUndo->setEnabled(count > 0);
}
