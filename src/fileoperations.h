/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2020 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#ifndef FILEOPERATIONS_H
#define FILEOPERATIONS_H

#include <QObject>

#include "errorcodes.h"

class QProcess;

class FileOperations : public QObject
{
    Q_OBJECT
public:
    static ErrorCode touchFile(const QString &filename);

    static ErrorCode copyFile(const QString &inputFilename, const QString &outputFilename, bool force = false);
    static ErrorCode moveFile(const QString &inputFilename, const QString &outputFilename, bool force = false);

    static qint64 freeDiskSpace(const QString &fileSystem);

    static QString temporaryFilename(const QString &stem = QString(), const QString &suffix = QString());
    static QString createTemporaryDirectory(const QString &stem = QString());
    static ErrorCode removeDirectory(const QString &directory);

    static QString dialogFilterForFilename(const QString &filename);

    static bool cloneFilePermissions(const QString &inputFilename, const QString &outputFilename, bool removeExecutable = true);

    static QString commandLine(QProcess *process);

protected:
    explicit FileOperations(QObject *parent = nullptr);

private:
    static QString escapeCommandLine(const QString &input);
};

#endif // FILEOPERATIONS_H
