/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2019 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#ifndef IMAGESELECTION_H
#define IMAGESELECTION_H

#include <QObject>

#include "imagewidget.h"

class ImageSelection : public QObject
{
    Q_OBJECT
public:
    explicit ImageSelection(ImageWidget *parent = nullptr);
    ~ImageSelection() override;

    void clear();

    void resizeEvent(int numerator, int denominator);
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);

    void setMax(int maxWidth, int maxHeight);
    QRect selection() const;

signals:
    void selectionChanged(QRect);
    void mouseCursorChange(Qt::CursorShape);

private:
    class Private;
    Private *const d;
};

#endif // IMAGESELECTION_H
