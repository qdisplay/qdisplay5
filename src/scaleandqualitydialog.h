/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2022 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. s*
 ***************************************************************************/

#ifndef SCALEANDQUALITYDIALOG_H
#define SCALEANDQUALITYDIALOG_H

#include <QDialog>

#include "imageoperations.h"

class ScaleAndQualityDialog : public QDialog
{
    Q_OBJECT
public:
    enum IndexColors {ColorsAutomatic = 0, Colors2, Colors2Dithered, Colors4, Colors4Dithered, Colors8, Colors8Dithered, Colors16, Colors16Dithered, Colors32, Colors32Dithered, Colors64, Colors64Dithered, Colors128, Colors128Dithered, Colors256, Colors256Dithered};

    struct ScaleQualityParameters {
        bool keepAspectRatio;
        int width, height;
        ImageOperations::FileType originalFileType;
        ImageOperations::Colors originalColors;
        int percentQuality;
        IndexColors indexColors;
        int scale;
        double gamma;
        bool normalize;
        ImageOperations::FileType convertTo;
    };

    explicit ScaleAndQualityDialog(const ScaleQualityParameters &sqp, QWidget *parent = nullptr);
    ~ScaleAndQualityDialog() override;

    ScaleQualityParameters parameters() const;

protected:
    bool eventFilter(QObject *obj, QEvent *event) override;

private slots:
    void toggleKeepAspectRatio();
    void gammaSliderChanged(int newGamma);
    void widthChanged(int newWidth);
    void heightChanged(int newHeight);
    void scaleChanged(int newScale);
    void combobBoxScaleChanged(int newIndex);
    void targetFileTypeChanged();

private:
    class Private;
    Private *const d;
};

#endif // SCALEANDQUALITYDIALOG_H
