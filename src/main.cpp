/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2022 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include "mainwindow.h"

#include <iostream>

#include <QApplication>
#include <QTime>
#include <QStringList>
#include <QDir>
#include <QFileInfo>
#include <QDebug>
#include <QString>
#include <QRegularExpression>
#include <QImageReader>

#include "imageoperations.h"

void addFilesFromDirectory(const QString &directory, QStringList &files, const QRegularExpression &fileExtensionsRegExp) {
    QDir dir(directory);

    const QFileInfoList dirListing = dir.entryInfoList(QStringList(), QDir::Files | QDir::Readable, QDir::Name);
    for (const QFileInfo &dirEntry : dirListing) {
        if (dirEntry.absoluteFilePath().indexOf(fileExtensionsRegExp) >= 0)
            files.append(dirEntry.absoluteFilePath());
    }
}

int main(int argc, char *argv[])
{
    bool showVersionInfo = argc <= 1;
    bool showHelp = false;
    QApplication a(argc, argv);

    QSet<QString> imageFileExtensions{QStringLiteral("jpeg"), QStringLiteral("jpg"), QStringLiteral("avif"), QStringLiteral("svg"), QStringLiteral("png"), QStringLiteral("tiff"), QStringLiteral("tif"), QStringLiteral("gif"), QStringLiteral("ppm"), QStringLiteral("pgm"), QStringLiteral("pbm"), QStringLiteral("webp")};
    if (ImageOperations::canJpegXL())
        imageFileExtensions.insert(QStringLiteral("jxl"));
    for (QSet<QString>::Iterator it = imageFileExtensions.begin(); it != imageFileExtensions.end();) {
        if (!QImageReader::supportedImageFormats().contains(it->toUtf8().constData())) {
            qWarning() << "Disabling support for image filename extension" << *it;
            it = imageFileExtensions.erase(it);
        }
        else
            ++it;
    }
    static const QSet<QString> otherFileExtensions{QStringLiteral("otf"), QStringLiteral("ttf"), QStringLiteral("pfb"), QStringLiteral("pdf"), QStringLiteral("eps")};
    QSet<QString> fileExtensions{imageFileExtensions + otherFileExtensions};

    const QRegularExpression fileExtensionsRegExp = QRegularExpression(QString(QStringLiteral("[.](%1)$")).arg(QStringList(fileExtensions.constBegin(), fileExtensions.constEnd()).join(QChar('|'))), QRegularExpression::CaseInsensitiveOption);

    QStringList files;
    for (int i = 1; !showVersionInfo && !showHelp && i < a.arguments().count(); ++i) {
        const QString arg = a.arguments().at(i);
        if (arg == QStringLiteral("-version") || arg == QStringLiteral("--version"))
            showVersionInfo = true;
        else if (arg == QStringLiteral("-help") || arg == QStringLiteral("--help"))
            showHelp = true;
        else if (arg.startsWith(QLatin1Char('@'))) {
            QFile fileListFile(arg.mid(1));
            if (fileListFile.open(QFile::ReadOnly)) {
                QTextStream ts(&fileListFile);
                while (!ts.atEnd()) {
                    const QString line = ts.readLine();
                    if (line.isEmpty()) continue; ///< skip empty lines
                    if (line.startsWith(QLatin1Char('#'))) continue; ///< skip lines starting with a hash (may be comment)

                    const QFileInfo lineFileInfo(line);
                    if (!lineFileInfo.exists()) {
                        qDebug() << "Don't know what to do with this line:" << line;
                        continue;
                    }
                    if (lineFileInfo.isDir())
                        addFilesFromDirectory(lineFileInfo.absoluteFilePath(), files, fileExtensionsRegExp);
                    else if (lineFileInfo.isFile() && lineFileInfo.absoluteFilePath().indexOf(fileExtensionsRegExp) >= 0)
                        files.append(lineFileInfo.absoluteFilePath());
                    else
                        qDebug() << "Don't know what to do with this line:" << line;
                }
                fileListFile.close();
            } else
                qWarning() << "Cannot open list of filenames:" << fileListFile.fileName();
        } else {
            QFileInfo fi(arg);
            if (fi.isDir()) {
                addFilesFromDirectory(fi.absoluteFilePath(), files, fileExtensionsRegExp);
            } else if (fi.isFile() && fi.absoluteFilePath().indexOf(fileExtensionsRegExp) >= 0) {
                files.append(fi.absoluteFilePath());
            } else
                qWarning() << "Don't know what to do with this command line argument:" << arg;
        }
    }

    showVersionInfo |= files.isEmpty();

    if (showHelp) {
        std::cout << "Usage:  qdisplay file1 file2 file3 ..." << std::endl << std::endl;
        std::cout << "A \"file\" may be as well:" << std::endl;
        std::cout << " - a directory which is scanned non-recursively;" << std::endl;
        std::cout << "   found image files will be shown" << std::endl;
        std::cout << " - if starting with \"@\", the argument's remainder" << std::endl;
        std::cout << "   is interpreted as a text file, where each non-empty line" << std::endl;
        std::cout << "   contains a file name of an image to show" << std::endl;
        return 0;
    } else if (showVersionInfo) {
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        std::cout << "QDisplay5 -- An image viewer for the command line, based on Qt5" << std::endl;
#else // #if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
        std::cout << "QDisplay5 -- An image viewer for the command line, based on Qt6" << std::endl;
#endif // QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
        std::cout << "Copyright (C) 2014-2024 by Thomas Fischer <fischer@unix-ag.uni-kl.de>" << std::endl << std::endl;
        std::cout << "This program is free software; you can redistribute it and/or modify" << std::endl;
        std::cout << "it under the terms of the GNU General Public License as published by" << std::endl;
        std::cout << "the Free Software Foundation; either version 3 of the License, or" << std::endl;
        std::cout << "(at your option) any later version." << std::endl << std::endl;
        std::cout << "You should have received a copy of the GNU General Public License" << std::endl;
        std::cout << "along with this program; if not, see <https://www.gnu.org/licenses/>." << std::endl;
        return 0;
    }

    MainWindow w(files);
    w.show();

    return a.exec();
}
