/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2022 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include "imageoperationsui.h"

#include <QPointer>
#include <QApplication>
#include <QFile>
#include <QRegularExpression>
#include <QDebug>

#include "scaleandqualitydialog.h"
#include "imageoperations.h"
#include "fileoperations.h"
#include "exif.h"

class ImageOperationsUI::Private
{
public:
    ImageOperations *imageOperations;
    Exif *exif;

    Private(ImageOperationsUI *parent) {
        imageOperations = new ImageOperations(parent);
        exif = new Exif(parent);
    }

    ~Private() {
        delete exif;
        delete imageOperations;
    }
};


ImageOperationsUI::ImageOperationsUI(QWidget *parent)
    : QObject(parent), d(new ImageOperationsUI::Private(this))
{
    /// nothing
}

ImageOperationsUI::~ImageOperationsUI()
{
    delete d;
}

ErrorCode ImageOperationsUI::scale(const QString &inputfilename, QString &outputfilename)
{
    outputfilename = inputfilename;
    QWidget *parentWidget = qobject_cast<QWidget *>(parent());

    const ImageOperations::FileType inputFileType = ImageOperations::fileTypeForFileName(inputfilename);
    if (!ImageOperations::canScale(inputFileType)) {
        /// Cannot scale for requested file type
        qWarning() << "Unsupported filetype for scale operation:" << inputfilename;
        return UnsupportedFileType;
    }

    ScaleAndQualityDialog::ScaleQualityParameters sqp;
    sqp.keepAspectRatio = true;
    const ErrorCode identifyImageResult = d->imageOperations->identifyImage(inputfilename, sqp.width, sqp.height, sqp.originalFileType, sqp.originalColors);
    if (identifyImageResult != NoError)
        return identifyImageResult;
    sqp.scale = 10000;
    sqp.percentQuality = 75;
    sqp.indexColors = ScaleAndQualityDialog::ColorsAutomatic;
    sqp.gamma = 0.0;
    sqp.normalize = false;
    sqp.convertTo = sqp.originalFileType;

    QPointer<ScaleAndQualityDialog> dialog = new ScaleAndQualityDialog(sqp, parentWidget);
    const int dialogResult = dialog->exec();
    if (dialogResult != QDialog::Accepted)
        /// User aborted/cancelled 'scale and quality' dialog
        return UserCancelled;

    qApp->setOverrideCursor(Qt::WaitCursor);

    sqp = dialog->parameters();
    delete dialog;

    outputfilename = outputfilename.replace(QRegularExpression(QStringLiteral("[.][a-z]{2,5}$")), ImageOperations::extensionForFileType(sqp.convertTo));

#define numColorsFromSQP(sqp) (sqp.indexColors == ScaleAndQualityDialog::Colors2Dithered || sqp.indexColors == ScaleAndQualityDialog::Colors2 ? 2 : (sqp.indexColors == ScaleAndQualityDialog::Colors4Dithered || sqp.indexColors == ScaleAndQualityDialog::Colors4 ? 4 : (sqp.indexColors == ScaleAndQualityDialog::Colors8Dithered || sqp.indexColors == ScaleAndQualityDialog::Colors8 ? 8 : (sqp.indexColors == ScaleAndQualityDialog::Colors16Dithered || sqp.indexColors == ScaleAndQualityDialog::Colors16 ? 16 : (sqp.indexColors == ScaleAndQualityDialog::Colors32Dithered || sqp.indexColors == ScaleAndQualityDialog::Colors32 ? 32 : (sqp.indexColors == ScaleAndQualityDialog::Colors64Dithered || sqp.indexColors == ScaleAndQualityDialog::Colors64 ? 64 : (sqp.indexColors == ScaleAndQualityDialog::Colors128Dithered || sqp.indexColors == ScaleAndQualityDialog::Colors128 ? 128 : (sqp.indexColors == ScaleAndQualityDialog::Colors256Dithered || sqp.indexColors == ScaleAndQualityDialog::Colors256 ? 256 : 0xffff))))))))
#define ditherFromSQP(sqp) (sqp.indexColors == ScaleAndQualityDialog::Colors2Dithered || sqp.indexColors == ScaleAndQualityDialog::Colors4Dithered || sqp.indexColors == ScaleAndQualityDialog::Colors8Dithered || sqp.indexColors == ScaleAndQualityDialog::Colors16Dithered ||sqp.indexColors == ScaleAndQualityDialog::Colors32Dithered || sqp.indexColors == ScaleAndQualityDialog::Colors64Dithered  || sqp.indexColors == ScaleAndQualityDialog::Colors128Dithered|| sqp.indexColors == ScaleAndQualityDialog::Colors256Dithered)

    ErrorCode result = UnspecifiedError;
    if (sqp.convertTo == ImageOperations::IsJPEG) {
        const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("scale-jpeg"), ImageOperations::extensionForFileType(sqp.convertTo));
        result = d->imageOperations->scaleToJPEG(inputfilename, tempfile, sqp.width, sqp.height, sqp.percentQuality, sqp.gamma, sqp.normalize);
        if (result == NoError) {
            result = sqp.convertTo == ImageOperations::IsJPEG ? d->imageOperations->touchJPEG(tempfile) : NoError;
            if (result == NoError) {
                result = sqp.convertTo == ImageOperations::IsJPEG && ImageOperations::isJPEG(inputfilename) ? d->exif->copyExif(inputfilename, tempfile) : NoError;
                if (result == NoError)
                    result = FileOperations::moveFile(tempfile, outputfilename, true);
            }
        }
        QFile::remove(tempfile);
    } else if (sqp.convertTo == ImageOperations::IsJpegXLLossless) {
        const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("scale-jpegxllossless"), ImageOperations::extensionForFileType(sqp.convertTo));
        result = d->imageOperations->scaleToJpegXLLossless(inputfilename, tempfile, sqp.width, sqp.height, numColorsFromSQP(sqp), ditherFromSQP(sqp), sqp.gamma, sqp.normalize);
        if (result == NoError)
            result = FileOperations::moveFile(tempfile, outputfilename, true);
        QFile::remove(tempfile);
    } else if (sqp.convertTo == ImageOperations::IsJpegXLLossy) {
        const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("scale-jpegxllossy"), ImageOperations::extensionForFileType(sqp.convertTo));
        result = d->imageOperations->scaleToJpegXLLossy(inputfilename, tempfile, sqp.width, sqp.height, sqp.percentQuality, sqp.gamma, sqp.normalize);
        if (result == NoError)
            result = FileOperations::moveFile(tempfile, outputfilename, true);
        QFile::remove(tempfile);
    } else if (sqp.convertTo == ImageOperations::IsPNG) {
        const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("scale-png"), ImageOperations::extensionForFileType(sqp.convertTo));
        result = d->imageOperations->scaleToPNG(inputfilename, tempfile, sqp.width, sqp.height, numColorsFromSQP(sqp), ditherFromSQP(sqp), sqp.gamma, sqp.normalize, !ImageOperations::externalPNGcompressionAvailable);
        if (result == NoError) {
            result = d->imageOperations->touchPNG(tempfile);
            if (result == NoError)
                result = FileOperations::moveFile(tempfile, outputfilename, true);
        }
        QFile::remove(tempfile);
    } else if (sqp.convertTo == ImageOperations::IsTIFF) {
        const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("scale-tiff"), ImageOperations::extensionForFileType(sqp.convertTo));
        result = d->imageOperations->scaleToTIFF(inputfilename, tempfile, sqp.width, sqp.height, numColorsFromSQP(sqp), ditherFromSQP(sqp), sqp.gamma, sqp.normalize, true);
        if (result == NoError)
            result = FileOperations::moveFile(tempfile, outputfilename, true);
        QFile::remove(tempfile);
    } else if (sqp.convertTo == ImageOperations::IsPGM) {
        const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("scale-pgm"), ImageOperations::extensionForFileType(sqp.convertTo));
        result = d->imageOperations->scaleToPGM(inputfilename, tempfile, sqp.width, sqp.height, sqp.gamma, sqp.normalize);
        if (result == NoError)
            result = FileOperations::moveFile(tempfile, outputfilename, true);
        QFile::remove(tempfile);
    } else if (sqp.convertTo == ImageOperations::IsPBM) {
        const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("scale-pbm"), ImageOperations::extensionForFileType(sqp.convertTo));
        result = d->imageOperations->scaleToPBM(inputfilename, tempfile, sqp.width, sqp.height);
        if (result == NoError)
            result = FileOperations::moveFile(tempfile, outputfilename, true);
        QFile::remove(tempfile);
    } else if (sqp.convertTo == ImageOperations::IsWebPLossless) {
        const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("scale-webp"), ImageOperations::extensionForFileType(sqp.convertTo));
        result = d->imageOperations->scaleToWebPLossless(inputfilename, tempfile, sqp.width, sqp.height, numColorsFromSQP(sqp), ditherFromSQP(sqp), sqp.gamma, sqp.normalize, true);
        if (result == NoError)
            result = FileOperations::moveFile(tempfile, outputfilename, true);
        QFile::remove(tempfile);
    } else if (sqp.convertTo == ImageOperations::IsWebPLossy) {
        const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("scale-webp"), ImageOperations::extensionForFileType(sqp.convertTo));
        result = d->imageOperations->scaleToWebPLossy(inputfilename, tempfile, sqp.width, sqp.height, sqp.percentQuality, sqp.gamma, sqp.normalize);
        if (result == NoError)
            result = FileOperations::moveFile(tempfile, outputfilename, true);
        QFile::remove(tempfile);
    } else if (sqp.convertTo == ImageOperations::IsAVIF) {
        const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("scale-avif"), ImageOperations::extensionForFileType(sqp.convertTo));
        result = d->imageOperations->scaleToAVIF(inputfilename, tempfile, sqp.width, sqp.height, sqp.percentQuality, sqp.gamma, sqp.normalize);
        if (result == NoError)
            result = FileOperations::moveFile(tempfile, outputfilename, true);
        QFile::remove(tempfile);
    }
    else
        result = UnspecifiedError;

    qApp->restoreOverrideCursor();

    return result;
}
