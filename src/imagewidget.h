/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2019 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#ifndef IMAGEWIDGET_H
#define IMAGEWIDGET_H

#include <QWidget>
#include <QSharedPointer>
#include <QImageReader>

class QPixmap;
class QSize;
class QMovie;

class ImageWidget : public QWidget
{
    Q_OBJECT
public:
    enum Command {commandQuit = 1001, commandUndo = 1002, commandDelete = 1003,
                  commandReload = 1004, commandTouch = 1005, commandRandomizeFileOrder = 1006,
                  commandFullscreen = 1007,
                  commandGreyScale = 1010, commandGrayScale = commandGreyScale,
                  commandScaleAndQuality = 1011, commandCrop = 1012, commandARAWCRS = 1013, commandBlackBar = 1014, commandBlur = 1015,
                  commandRotateRight90 = 1020, commandRotateLeft90 = 1021,
                  commandNextImage = 1030, commandPrevImage = 1031,
                  commandNext10Image = 1032, commandPrev10Image = 1033,
                  commandNext100Image = 1034, commandPrev100Image = 1035,
                  commandGotoFirst = 1038, commandGotoLast = 1039,
                  commandFileCopyTo = 1050, commandFileMoveTo = 1051,
                  commandConvertImageType = 1060
                 };
    enum FitMode {fitMaximumScale = 0, fitPreferOriginalSize = 1, fitInvalid = 2};

    explicit ImageWidget(QWidget *parent = nullptr);
    ~ImageWidget() override;

    void setImage(const QPixmap &pixmap);
    void setMovie(QSharedPointer<QMovie> &movie);
    void unsetImage(bool nonImageFile);
    bool toggleFullscreenMode();

    void showMessage(const QString &messageText, int duration, QColor fontColor = Qt::white);

    QSize imageSize() const;
    QSize visualImageSize() const;
    QRect selection() const;
    void setARAWCRSselection();

signals:
    void commandTriggered(int cmd);
    void zoomPercentChanged(int newPercent);

public slots:
    void availableUndoStepsChanged(int);

protected:
    virtual void paintEvent(QPaintEvent *event) override;
    virtual void resizeEvent(QResizeEvent *event) override;
    virtual void keyPressEvent(QKeyEvent *event) override;
    virtual void wheelEvent(QWheelEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;
    virtual void mousePressEvent(QMouseEvent *event) override;

private slots:
    void visualSelectionChanged(QRect);
    void mouseCursorChanged(Qt::CursorShape);
    void movieFrameChanged(int);
    void movieErrors(QImageReader::ImageReaderError);
    void actionTriggered();
    void toggleFitMode();

private:
    class Private;
    Private *const d;
};

#endif // IMAGEWIDGET_H
