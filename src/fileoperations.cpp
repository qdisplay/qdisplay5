/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2022 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include "fileoperations.h"

#include <utime.h>
#include <errno.h>
#include <sys/statvfs.h>
#include <sys/stat.h>

#include <QRegularExpression>
#include <QStandardPaths>
#include <QProcess>
#include <QFile>
#include <QDir>
#include <QFileInfo>
#include <QDateTime>
#include <QMessageBox>
#include <QApplication>
#include <QRandomGenerator>
#include <QDebug>

FileOperations::FileOperations(QObject *parent) :
    QObject(parent)
{
    /// nothing
}

QString FileOperations::escapeCommandLine(const QString &input)
{
    static const QRegularExpression whiteSpace(QStringLiteral("[ \t\n\r]"));
    const bool containsSpaces = input.contains(whiteSpace);
    const bool containsSingleQuotationMark = input.contains(QChar('\''));
    const bool containsDoubleQuotationMark = input.contains(QChar('"'));
    const bool containsBackslash = input.contains(QChar('\\'));

    if (!containsSpaces && !containsDoubleQuotationMark && !containsSingleQuotationMark && !containsBackslash)
        return input;
    else if ((containsSpaces || containsBackslash || containsDoubleQuotationMark) && !containsSingleQuotationMark)
        return QString(QStringLiteral("'%1'")).arg(input);
    else if ((containsSpaces || containsSingleQuotationMark) && !containsDoubleQuotationMark && !containsBackslash)
        return QString(QStringLiteral("\"%1\"")).arg(input);
    else
        return QString(QStringLiteral("'%1'")).arg(QString(input).replace(QChar('\''), QStringLiteral("\\'")));
}

ErrorCode FileOperations::touchFile(const QString &filename) {
    if (utime(filename.toUtf8().constData(), nullptr) == 0)
        return NoError;
    else {
        qWarning() << "FileOperations::touchFile: utime on file" << filename << "failed with error" << errno;
        return UnspecifiedError;
    }
}

ErrorCode FileOperations::copyFile(const QString &inputFilename, const QString &outputFilename, bool force) {
    QFileInfo inputFileInfo(inputFilename), outputFileInfo(outputFilename);

    if (inputFilename == outputFilename) {
        qWarning() << "FileOperations::copyFile: both files are the same:" << inputFilename << outputFilename;
        return InvalidArgument;
    } else {
        if (inputFileInfo.absoluteFilePath() == outputFileInfo.absoluteFilePath()) {
            qWarning() << "FileOperations::copyFile: both files are the same:" << inputFilename << outputFilename;
            return InvalidArgument;
        }
    }

    if (!force && outputFileInfo.exists()) {
        if (QMessageBox::question(QApplication::activeWindow(), QStringLiteral("Overwrite file?"), QString(QStringLiteral("<qt>Overwrite file '<b>%1</b>' in directory '%2'?</qt>")).arg(outputFileInfo.fileName()).arg(outputFileInfo.path()), QMessageBox::Yes, QMessageBox::Cancel) != QMessageBox::Yes)
            return UserCancelled;
    }

    struct utimbuf inputFileTime;
    inputFileTime.actime = inputFileInfo.lastRead().toSecsSinceEpoch();
    inputFileTime.modtime = inputFileInfo.lastModified().toSecsSinceEpoch();

    const qint64 freeTargetSpace = freeDiskSpace(outputFileInfo.path());
    if (inputFileInfo.size() + 32768 > freeTargetSpace) {
        qWarning() << "FileOperations::copyFile: not enough disk space on target filesystem " << outputFileInfo.path() << inputFileInfo.size() << freeTargetSpace;
        return OutOfDiskSpaceError;
    }

    const QString prelimOutputFilename = outputFilename + QString::number(QRandomGenerator::global()->generate(), 36);
    ErrorCode result = QFile::copy(inputFilename, prelimOutputFilename) ? NoError : UnspecifiedError;
    if (result == NoError) {
        if (QFile::exists(outputFilename)) {
            if (!QFile::remove(outputFilename)) {
                qWarning() << "FileOperations::copyFile: removal of already existing output file failed:" << outputFilename;
                QFile::remove(prelimOutputFilename);
                return UnspecifiedError;
            }
        }
        result = QFile::rename(prelimOutputFilename, outputFilename) ? NoError : UnspecifiedError;
        if (result != NoError) {
            qWarning() << "FileOperations::copyFile: failed to rename" << prelimOutputFilename << "to" << outputFilename;
            QFile::remove(prelimOutputFilename);
            return result;
        }
        cloneFilePermissions(inputFilename, outputFilename);
        if (utime(outputFilename.toUtf8().constData(), &inputFileTime) != 0) {
            qWarning() << "FileOperations::copyFile:  utime on file failed:" << outputFilename;
            return ExternalCommandFailed;
        }
    } else {
        qWarning() << "FileOperations::copyFile: failed to copy" << inputFilename << "to" << prelimOutputFilename;
        if (!QFile::remove(prelimOutputFilename))
            qDebug() << "Could not remove file" << prelimOutputFilename << "in FileOperations::copyFile";
        return UnspecifiedError;
    }

    return result;
}

ErrorCode FileOperations::moveFile(const QString &inputFilename, const QString &outputFilename, bool force) {
    if (inputFilename == outputFilename) {
        qWarning() << "FileOperations::moveFile: both files are the same:" << inputFilename << outputFilename;
        return InvalidArgument;
    } else {
        QFileInfo inputFI(inputFilename), outputFI(outputFilename);
        if (inputFI.absoluteFilePath() == outputFI.absoluteFilePath()) {
            qWarning() << "FileOperations::moveFile: both files are the same:" << inputFilename << outputFilename;
            return InvalidArgument;
        }
    }

    const ErrorCode result = copyFile(inputFilename, outputFilename, force);
    if (result == NoError) {
        if (!QFile::remove(inputFilename))
            qDebug() << "Could not remove file" << inputFilename << "in FileOperations::moveFile";
    } else {
        qWarning() << "FileOperations::moveFile: copying file" << inputFilename << "to" << outputFilename << "failed";
        return result;
    }

    return NoError;
}

qint64 FileOperations::freeDiskSpace(const QString &fileSystem) {
    struct statvfs buffer;
    if (statvfs(fileSystem.toUtf8().constData(), &buffer) == 0) {
        const qint64 free = static_cast<qint64>(buffer.f_bsize) * static_cast<qint64>(buffer.f_bfree);
        return free;
    } else {
        qWarning() << "FileOperations::freeDiskSpace: failed to probe free disk space on" << fileSystem << "(errno=" << errno << ")";
        return -1;
    }
}

QString FileOperations::temporaryFilename(const QString &stem, const QString &suffix) {
    return QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory) + QStringLiteral(".qdisplay5-") + stem + (stem.isEmpty() ? QString() : QStringLiteral("-")) + QString::number(QRandomGenerator::global()->generate(), 36) + QStringLiteral("-") + QString::number(QRandomGenerator::global()->generate(), 36) + suffix;
}

QString FileOperations::createTemporaryDirectory(const QString &stem) {
    const QString base = QStringLiteral(".qdisplay5-") + stem + (stem.isEmpty() ? QString() : QStringLiteral("-")) + QString::number(QRandomGenerator::global()->generate(), 36) + QStringLiteral(".d");
    const QString directoryName = QDir::temp().absolutePath() + QDir::separator() + base;
    if (QDir::temp().mkpath(base))
        return directoryName;
    else {
        qWarning() << "FileOperations::createTemporaryDirectory: could not create temporary directory" << directoryName;
        return QString();
    }
}

ErrorCode FileOperations::removeDirectory(const QString &directoryName) {
    QDir directory(directoryName + QDir::separator());
    if (!directory.exists()) {
        qWarning() << "Directory does not exist:" << directory.absolutePath();
        return UnspecifiedError;
    }

    const QStringList dirListing = directory.entryList(QDir::Files | QDir::NoDotAndDotDot);
    for (const QString &filename : dirListing) {
        if (!QFile::remove(directory.absoluteFilePath(filename))) {
            qWarning() << "Could not remove" << filename << "in" << directory.absolutePath();
            return UnsupportedOperation;
        }
    }

    const QString basename = directory.dirName();
    if (!directory.cdUp()) {
        qWarning() << "Could not change to parent directory of " << directory.absolutePath();
        return UnsupportedOperation;
    }

    if (!directory.rmpath(basename)) {
        qWarning() << "Could not remove directory" << basename << "from parent directory" << directory.absolutePath();
        return UnsupportedOperation;
    }

    return NoError;
}

QString FileOperations::dialogFilterForFilename(const QString &filename) {
    if (filename.endsWith(QStringLiteral(".jpg"), Qt::CaseInsensitive) || filename.endsWith(QStringLiteral(".jpe"), Qt::CaseInsensitive) || filename.endsWith(QStringLiteral(".jpeg"), Qt::CaseInsensitive))
        return QStringLiteral("JPEG image (*.jpg *.jpeg *.jpe)");
    else if (filename.endsWith(QStringLiteral(".jxl"), Qt::CaseInsensitive))
        return QStringLiteral("JPEG XL image (*.jxl)");
    else if (filename.endsWith(QStringLiteral(".png"), Qt::CaseInsensitive))
        return QStringLiteral("PNG image (*.png)");
    else if (filename.endsWith(QStringLiteral(".tif"), Qt::CaseInsensitive) || filename.endsWith(QStringLiteral(".tiff"), Qt::CaseInsensitive))
        return QStringLiteral("TIFF image (*.tif *.tiff)");
    else if (filename.endsWith(QStringLiteral(".gif"), Qt::CaseInsensitive))
        return QStringLiteral("GIF image (*.gif)");
    else if (filename.endsWith(QStringLiteral(".webp"), Qt::CaseInsensitive))
        return QStringLiteral("WebP image (*.webp)");
    else if (filename.endsWith(QStringLiteral(".avif"), Qt::CaseInsensitive))
        return QStringLiteral("AVIF image (*.avif)");
    else
        return QStringLiteral("All files (*)");
}

bool FileOperations::cloneFilePermissions(const QString &inputFilename, const QString &outputFilename, bool removeExecutable) {
    struct stat64 stat64Buffer;
    if (::stat64(inputFilename.toUtf8().constData(), &stat64Buffer) == 0) {
        const mode_t mode = removeExecutable ? stat64Buffer.st_mode & static_cast<uint>(~(S_IXUSR | S_IXGRP | S_IXOTH)) : stat64Buffer.st_mode;
        if (::chmod(outputFilename.toUtf8().constData(), mode) == 0)
            return true;
        else
            return false;
    } else
        return false;
}

QString FileOperations::commandLine(QProcess *process)
{
    QString result = escapeCommandLine(process->program());
    for (const QString &argument : process->arguments())
        result.append(QString(QStringLiteral(" %1")).arg(escapeCommandLine(argument)));
    return result;
}
