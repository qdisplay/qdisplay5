/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2019 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#ifndef UNDOSTACK_H
#define UNDOSTACK_H

#include <QObject>

#include "errorcodes.h"

class UndoStack : public QObject
{
    Q_OBJECT
public:
    explicit UndoStack(QObject *parent = nullptr);
    ~UndoStack() override;

    ErrorCode addBackupToStack(const QString &filename, int position = -1);

    QString undo(int *position);

signals:
    void availableUndoStepsChanged(int);

private:
    class Private;
    Private *const d;

};

#endif // UNDOSTACK_H
