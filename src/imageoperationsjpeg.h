/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2019 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#ifndef IMAGEOPERATIONSJPEG_H
#define IMAGEOPERATIONSJPEG_H

#include <QString>

#include "errorcodes.h"

class QObject;

class ImageOperationsJPEG
{
public:
    static bool isJPEG(const QString &filename);
    ErrorCode touchJPEG(const QString &filename);

protected:
    ErrorCode grayscaleJPEG(const QString &inputFilename, const QString &outputFilename);
    ErrorCode cropJPEG(const QString &inputFilename, const QString &outputFilename, int left, int top, int width, int height);

    ImageOperationsJPEG(QObject *processParent);

private:
    QObject *processParent;
};

#endif // IMAGEOPERATIONSJPEG_H
