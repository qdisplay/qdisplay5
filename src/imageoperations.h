/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2022 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#ifndef IMAGEOPERATIONS_H
#define IMAGEOPERATIONS_H

#include <QObject>
#include <QString>

#include "errorcodes.h"
#include "imageoperationsjpeg.h"

class ImageOperations : public QObject, public ImageOperationsJPEG
{
    Q_OBJECT

public:
    explicit ImageOperations(QObject *parent);
    ~ImageOperations() override;

    enum Rotation {Right90, Left90};
    enum FileType {IsOther = 0, IsJPEG, IsJpegXLLossless, IsJpegXLLossy, IsAVIF, IsPNG, IsTIFF, IsGIF, IsPBM, IsPGM, IsPPM, IsWebPLossless, IsWebPLossy, IsOTF, IsTTF, IsPFB, IsPDF, IsEPS, IsSVG};
    enum Colors { ColorsUnknown = 0, ColorsIndexed, ColorsTrueColor, ColorsGrayscale};

    static FileType fileTypeForFileName(const QString &filename);
    static QString fileTypeToShortDescription(const FileType &fileType);
    static QString fileTypeToLongDescription(const FileType &fileType);
    static QString extensionForFileType(const FileType fileType);
    static QString extensionFromFilename(const QString &filename);
    static const bool externalPNGcompressionAvailable;
    static bool isJpegXLLossless(const QString &filename);
    static bool isJpegXLLossy(const QString &filename);
    static bool isPNG(const QString &filename);
    static bool isTIFF(const QString &filename);
    static bool isGIF(const QString &filename);
    static bool isPBM(const QString &filename);
    static bool isPGM(const QString &filename);
    static bool isPPM(const QString &filename);
    static bool isWebPLossless(const QString &filename);
    static bool isWebPLossy(const QString &filename);
    static bool isOTF(const QString &filename);
    static bool isTTF(const QString &filename);
    static bool isPFB(const QString &filename);
    static bool isPDF(const QString &filename);
    static bool isEPS(const QString &filename);
    static bool isSVG(const QString &filename);
    static bool isAVIF(const QString &filename);
    static ErrorCode identifyImage(const QString &filename, int &width, int &height, FileType &fileType, Colors &colors);
    ErrorCode touch(const QString &filename);
    ErrorCode touchPNG(const QString &filename);
    static bool canJpegXL();
    static bool canScale(const QString &filename);
    static bool canScale(const FileType ft);
    ErrorCode scaleToJPEG(const QString &inputFilename, const QString &outputFilename, int width, int height, int quality, double gamma, bool normalize);
    ErrorCode scaleToJpegXLLossless(const QString &inputFilename, const QString &outputFilename, int width, int height, int numColors, bool dither, double gamma, bool normalize);
    ErrorCode scaleToJpegXLLossy(const QString &inputFilename, const QString &outputFilename, int width, int height, int quality, double gamma, bool normalize);
    ErrorCode scaleToAVIF(const QString &inputFilename, const QString &outputFilename, int width, int height, int quality, double gamma, bool normalize);
    ErrorCode scaleToPNG(const QString &inputFilename, const QString &outputFilename, int width, int height, int numColors, bool dither, double gamma, bool normalize, bool compress);
    ErrorCode scaleToTIFF(const QString &inputFilename, const QString &outputFilename, int width, int height, int numColors, bool dither, double gamma, bool normalize, bool compress);
    ErrorCode scaleToPGM(const QString &inputFilename, const QString &outputFilename, int width, int height, double gamma, bool normalize);
    ErrorCode scaleToPBM(const QString &inputFilename, const QString &outputFilename, int width, int height);
    ErrorCode scaleToWebPLossless(const QString &inputFilename, const QString &outputFilename, int width, int height, int numColors, bool dither, double gamma, bool normalize, bool compress);
    ErrorCode scaleToWebPLossy(const QString &inputFilename, const QString &outputFilename, int width, int height, int quality, double gamma, bool normalize);
    static bool canCrop(const QString &filename);
    static bool canCrop(const FileType ft);
    ErrorCode crop(const QString &inputFilename, const QString &outputFilename, int left, int top, int width, int height);
    static bool canBlackBar(const QString &filename);
    static bool canBlackBar(const FileType ft);
    ErrorCode blackBar(const QString &inputFilename, const QString &outputFilename, int left, int top, int width, int height);
    static bool canBlur(const QString &filename);
    static bool canBlur(const FileType ft);
    ErrorCode blur(const QString &inputFilename, const QString &outputFilename, int left, int top, int width, int height);
    static bool canRotate(const QString &filename);
    static bool canRotate(const FileType ft);
    ErrorCode rotate(const QString &inputFilename, const QString &outputFilename, Rotation rotation);
    static bool canGrayscale(const QString &filename);
    static bool canGrayscale(const FileType ft);
    ErrorCode grayscaleFile(const QString &inputFilename, const QString &outputFilename);
    static bool canConvertImageFormat(const QString &filename);
    static bool canConvertImageFormat(const FileType ft);
    ErrorCode convertImageFormat(const QString &inputFilename, const QString &outputFilename, const FileType outputFileType);

protected:
    ErrorCode scale(const QString &inputFilename, const QString &outputFilename, int width, int height, const QStringList &optionalArguments = QStringList());
    static ErrorCode isGrayscalePalette(const QString &filename, bool &isGrayscale);

private:
    class Private;
    Private *const d;
};

#endif // IMAGEOPERATIONS_H
