/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2020 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include "imageoperationsjpeg.h"

#include <QFile>
#include <QDebug>
#include <QProcess>
#include <QStandardPaths>

#include "fileoperations.h"

ImageOperationsJPEG::ImageOperationsJPEG(QObject *_processParent)
    : processParent(_processParent)
{
    /// nothing
}

bool ImageOperationsJPEG::isJPEG(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperationsJPEG::isJPEG: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        const int numBytesToRead = 3;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead && header[0] == 0xff && header[1] == 0xd8 && header[2] == 0xff;
        file.close();
        return result;
    } else {
        qWarning() << "ImageOperationsJPEG::isJPEG: could not open file:" << filename;
        return false;
    }
}

ErrorCode ImageOperationsJPEG::touchJPEG(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperationsJPEG::touchJPEG: file does not exist:" << filename;
        return FileDoesNotExist;
    }

    if (isJPEG(filename)) {
        if (!QStandardPaths::findExecutable(QStringLiteral("jpegtran")).isEmpty()) {
            QProcess jpegTran(processParent);
            jpegTran.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
            const QString tempFilename = FileOperations::temporaryFilename(QStringLiteral("jpegtran"), QStringLiteral(".jpg"));
            const QStringList args {QStringLiteral("-copy"), QStringLiteral("all"), QStringLiteral("-optimize"), QStringLiteral("-outfile"), tempFilename, filename};
            jpegTran.start(QStringLiteral("jpegtran"), args);

            if (!jpegTran.waitForStarted()) {
                qWarning() << "ImageOperationsJPEG::touchJPEG: failed to start command" << FileOperations::commandLine(&jpegTran);
                return ExternalCommandFailed;
            }
            if (!jpegTran.waitForFinished(120000)) {
                qWarning() << "ImageOperationsJPEG::touchJPEG: failed to finish command" << FileOperations::commandLine(&jpegTran);
                return ExternalCommandFailed;
            }
            ErrorCode result = jpegTran.exitStatus() == QProcess::NormalExit && jpegTran.exitCode() == 0 ? NoError : ExternalCommandFailed;
            if (result != NoError)
                qWarning() << "ImageOperationsJPEG::touchJPEG: process existed with error" << FileOperations::commandLine(&jpegTran) << "(exit code" << jpegTran.exitCode() << ")";
            else
                result = FileOperations::moveFile(tempFilename, filename, true) == NoError ? NoError : UnspecifiedError;
            return result;
        } else if (!QStandardPaths::findExecutable(QStringLiteral("jpegoptim")).isEmpty()) {
            QProcess jpegOptim(processParent);
            jpegOptim.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
            const QStringList args {filename};
            jpegOptim.start(QStringLiteral("jpegoptim"), args);

            if (!jpegOptim.waitForStarted()) {
                qWarning() << "ImageOperationsJPEG::touchJPEG: failed to start command" << FileOperations::commandLine(&jpegOptim);
                return ExternalCommandFailed;
            }
            if (!jpegOptim.waitForFinished(120000)) {
                qWarning() << "ImageOperationsJPEG::touchJPEG: failed to finish command" << FileOperations::commandLine(&jpegOptim);
                return ExternalCommandFailed;
            }
            const ErrorCode result = jpegOptim.exitStatus() == QProcess::NormalExit && jpegOptim.exitCode() == 0 ? NoError : ExternalCommandFailed;
            if (result != NoError)
                qWarning() << "ImageOperationsJPEG::touchJPEG: process existed with error" << FileOperations::commandLine(&jpegOptim) << "(exit code" << jpegOptim.exitCode() << ")";
            return result;
        } else {
            qWarning() << "ImageOperationsJPEG::touchJPEG: no known command installed to touch JPEG files";
            return ExternalCommandFailed;
        }
    } else {
        qWarning() << "ImageOperationsJPEG::touchJPEG: only JPEG files supported:" << filename;
        return UnsupportedOperation;
    }
}

ErrorCode ImageOperationsJPEG::grayscaleJPEG(const QString &inputFilename, const QString &outputFilename) {
    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperationsJPEG::grayscaleJPEG: output file already exists, overwriting:" << outputFilename;
    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperationsJPEG::grayscaleJPEG: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }
    if (!isJPEG(inputFilename)) {
        qWarning() << "ImageOperationsJPEG::grayscaleJPEG: only JPEG files can be converted to greyscale";
        return UnsupportedFileType;
    }

    QProcess jpegtran(processParent);
    jpegtran.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
    const QStringList args {QStringLiteral("-copy"), QStringLiteral("all"), QStringLiteral("-optimize"), QStringLiteral("-grayscale"), QStringLiteral("-outfile"), outputFilename, inputFilename};
    jpegtran.start(QStringLiteral("jpegtran"), args);

    if (!jpegtran.waitForStarted()) {
        qWarning() << "ImageOperationsJPEG::grayscaleJPEG: failed to start command" << FileOperations::commandLine(&jpegtran);
        return ExternalCommandFailed;
    }
    if (!jpegtran.waitForFinished(120000)) {
        qWarning() << "ImageOperationsJPEG::grayscaleJPEG: failed to finish command" << FileOperations::commandLine(&jpegtran);
        return ExternalCommandFailed;
    }

    const ErrorCode result = jpegtran.exitStatus() == QProcess::NormalExit && jpegtran.exitCode() == 0 ? NoError : ExternalCommandFailed;
    if (result != NoError) {
        qWarning() << "ImageOperationsJPEG::grayscaleJPEG: process existed with error" << FileOperations::commandLine(&jpegtran) << "(exit code" << jpegtran.exitCode() << ")";
        const QString errorOutput = QString::fromUtf8(jpegtran.readAllStandardError());
        qDebug() << errorOutput;
        return result;
    } else if (!QFile::exists(outputFilename)) {
        qWarning() << "ImageOperationsJPEG::grayscaleJPEG: no output file generated:" << outputFilename;
        return ExternalCommandFailed;
    } else /// result == NoError
        return NoError;
}

ErrorCode ImageOperationsJPEG::cropJPEG(const QString &inputFilename, const QString &outputFilename, int left, int top, int width, int height) {
    if (isJPEG(inputFilename)) {
        QProcess crop(processParent);
        crop.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
        const QStringList args {QStringLiteral("-copy"), QStringLiteral("all"), QStringLiteral("-crop"), QString(QStringLiteral("%1x%2+%3+%4")).arg(width).arg(height).arg(left).arg(top), QStringLiteral("-trim"), QStringLiteral("-outfile"), outputFilename, inputFilename};
        crop.start(QStringLiteral("jpegtran"), args);

        if (!crop.waitForStarted()) {
            qWarning() << "ImageOperationsJPEG::cropJPEG: failed to start command" << FileOperations::commandLine(&crop);
            return ExternalCommandFailed;
        }
        if (!crop.waitForFinished(120000)) {
            qWarning() << "ImageOperationsJPEG::cropJPEG: failed to finish command" << FileOperations::commandLine(&crop);
            return ExternalCommandFailed;
        }

        const ErrorCode result = crop.exitStatus() == QProcess::NormalExit && crop.exitCode() == 0 ? NoError : ExternalCommandFailed;
        if (result != NoError) {
            qWarning() << "ImageOperationsJPEG::cropJPEG: process existed with error" << FileOperations::commandLine(&crop) << "(exit code" << crop.exitCode() << ")";
            const QString errorOutput = QString::fromUtf8(crop.readAllStandardError());
            qDebug() << errorOutput;
            return result;
        } else if (!QFile::exists(outputFilename)) {
            qWarning() << "ImageOperationsJPEG::cropJPEG: no output file generated:" << outputFilename;
            return ExternalCommandFailed;
        } else /// result==NoError
            return NoError;
    } else {
        qWarning() << "ImageOperationsJPEG::cropJPEG: only JPEG files supported:" << inputFilename;
        return UnsupportedOperation;
    }
}
