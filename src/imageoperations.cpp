/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2022 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include "imageoperations.h"

#include <jpeglib.h>

#include <QFile>
#include <QProcess>
#include <QDebug>
#include <QStandardPaths>
#include <QTemporaryFile>
#include <QRegularExpression>
#include <QRandomGenerator>
#include <QImageReader>

#include "fileoperations.h"
#include "exif.h"

class ImageOperations::Private {
public:
    Exif *exif;

    Private(ImageOperations *parent)
    {
        exif = new Exif(parent);
    }

    ~Private()
    {
        delete exif;
    }

    static int log2_roundup_powerof2(int value)
    {
        if (value <= 2)
            return 1;
        else if (value <= 4)
            return 2;
        else if (value <= 16)
            return 4;
        else if (value <= 256)
            return 8;
        else if (value <= 65536)
            return 16;
        else
            return 32;
    }
};

const bool ImageOperations::externalPNGcompressionAvailable {!QStandardPaths::findExecutable(QStringLiteral("optipng")).isEmpty() || !QStandardPaths::findExecutable(QStringLiteral("pngcrush")).isEmpty()};

ImageOperations::ImageOperations(QObject *parent) :
    QObject(parent), ImageOperationsJPEG(this), d(new Private(this))
{
    /// nothing
}

ImageOperations::~ImageOperations() {
    delete d;
}

ImageOperations::FileType ImageOperations::fileTypeForFileName(const QString &filename) {
    if (isJPEG(filename))
        return IsJPEG;
    else if (isJpegXLLossy(filename))
        return IsJpegXLLossy;
    else if (isJpegXLLossless(filename))
        return IsJpegXLLossless;
    else if (isPNG(filename))
        return IsPNG;
    else if (isTIFF(filename))
        return IsTIFF;
    else if (isAVIF(filename))
        return IsAVIF;
    else if (isGIF(filename))
        return IsGIF;
    else if (isPBM(filename))
        return IsPBM;
    else if (isPGM(filename))
        return IsPGM;
    else if (isPPM(filename))
        return IsPPM;
    else if (isWebPLossless(filename))
        return IsWebPLossless;
    else if (isWebPLossy(filename))
        return IsWebPLossy;
    else if (isOTF(filename))
        return IsOTF;
    else if (isTTF(filename))
        return IsTTF;
    else if (isPFB(filename))
        return IsPFB;
    else if (isPDF(filename))
        return IsPDF;
    else if (isEPS(filename))
        return IsEPS;
    else if (isSVG(filename))
        return IsSVG;
    else
        return IsOther;
}

QString ImageOperations::fileTypeToShortDescription(const ImageOperations::FileType &fileType)
{
    switch (fileType) {
    case IsJPEG: return QStringLiteral("JPEG");
    case IsJpegXLLossless: return QStringLiteral("JPEG XL (lossless)");
    case IsJpegXLLossy: return QStringLiteral("JPEG XL (lossy)");
    case IsAVIF: return QStringLiteral("AVIF");
    case IsPNG: return QStringLiteral("PNG");
    case IsTIFF: return QStringLiteral("TIFF");
    case IsGIF: return QStringLiteral("GIF");
    case IsPBM: return QStringLiteral("PBM");
    case IsPGM: return QStringLiteral("PGM");
    case IsPPM: return QStringLiteral("PPM");
    case IsWebPLossless: return QStringLiteral("WebP (lossless)");
    case IsWebPLossy: return QStringLiteral("WebP (lossy)");
    case IsOTF: return QStringLiteral("OTF");
    case IsTTF: return QStringLiteral("TTF");
    case IsPFB: return QStringLiteral("PFB");
    case IsPDF: return QStringLiteral("PDF");
    case IsEPS: return QStringLiteral("EPS");
    case IsSVG: return QStringLiteral("SVG");
    default: return QString();
    }
}

QString ImageOperations::fileTypeToLongDescription(const ImageOperations::FileType &fileType)
{
    switch (fileType) {
    case IsJPEG: return QStringLiteral("Joint Photographic Experts Group (.jpg)");
    case IsJpegXLLossless: return QStringLiteral("JPEG XL (lossless, .jxl)");
    case IsJpegXLLossy: return QStringLiteral("JPEG XL (lossy, .jxl)");
    case IsAVIF: return QStringLiteral("AV1 Image File Format (.avif)");
    case IsPNG: return QStringLiteral("Portable Network Graphics (.png)");
    case IsTIFF: return QStringLiteral("TIFF (.tiff, .tif)");
    case IsGIF: return QStringLiteral("Graphics Interchange Format (.gif)");
    case IsPBM: return QStringLiteral("Portable BitMap (.pbm)");
    case IsPGM: return QStringLiteral("Portable GrayMap (.pgm)");
    case IsPPM: return QStringLiteral("Portable Pixmap (.ppm)");
    case IsWebPLossless: return QStringLiteral("WebP (lossless, .webp)");
    case IsWebPLossy: return QStringLiteral("WebP (lossy, .webp)");
    case IsOTF: return QStringLiteral("OpenType (.otf)");
    case IsTTF: return QStringLiteral("TrueType (.ttf)");
    case IsPFB: return QStringLiteral("Printer Font Binary (.pfb)");
    case IsPDF: return QStringLiteral("Portable Document Format (.pdf)");
    case IsEPS: return QStringLiteral("Encapsulated PostScript (.eps)");
    case IsSVG: return QStringLiteral("Scalable Vector Graphics (.svg)");
    default: return fileTypeToShortDescription(fileType);
    }
}

QString ImageOperations::extensionForFileType(const ImageOperations::FileType fileType) {
    switch (fileType) {
    case IsJPEG: return QStringLiteral(".jpg");
    case IsJpegXLLossless: return QStringLiteral(".jxl");
    case IsJpegXLLossy: return QStringLiteral(".jxl");
    case IsAVIF: return QStringLiteral(".avif");
    case IsPNG: return QStringLiteral(".png");
    case IsTIFF: return QStringLiteral(".tiff");
    case IsGIF: return QStringLiteral(".gif");
    case IsPBM: return QStringLiteral(".pbm");
    case IsPGM: return QStringLiteral(".pgm");
    case IsPPM: return QStringLiteral(".ppm");
    case IsWebPLossless: return QStringLiteral(".webp");
    case IsWebPLossy: return QStringLiteral(".webp");
    case IsOTF: return QStringLiteral(".otf");
    case IsTTF: return QStringLiteral(".ttf");
    case IsPFB: return QStringLiteral(".pfb");
    case IsPDF: return QStringLiteral(".pdf");
    case IsEPS: return QStringLiteral(".eps");
    case IsSVG: return QStringLiteral(".svg");
    default: return QString();
    }
}

QString ImageOperations::extensionFromFilename(const QString &filename) {
    if (isJPEG(filename))
        return QStringLiteral(".jpg");
    else if (isJpegXLLossless(filename))
        return QStringLiteral(".jxl");
    else if (isJpegXLLossy(filename))
        return QStringLiteral(".jxl");
    else if (isPNG(filename))
        return QStringLiteral(".png");
    else if (isTIFF(filename))
        return QStringLiteral(".tiff");
    else if (isGIF(filename))
        return QStringLiteral(".gif");
    else if (isPBM(filename))
        return QStringLiteral(".pbm");
    else if (isPGM(filename))
        return QStringLiteral(".pgm");
    else if (isPPM(filename))
        return QStringLiteral(".ppm");
    else if (isWebPLossless(filename))
        return QStringLiteral(".webp");
    else if (isWebPLossy(filename))
        return QStringLiteral(".webp");
    else if (isOTF(filename))
        return QStringLiteral(".otf");
    else if (isTTF(filename))
        return QStringLiteral(".ttf");
    else if (isPFB(filename))
        return QStringLiteral(".pfb");
    else if (isPDF(filename))
        return QStringLiteral(".pdf");
    else if (isEPS(filename))
        return QStringLiteral(".eps");
    else if (isSVG(filename))
        return QStringLiteral(".svg");
    return QString();
}

bool ImageOperations::isJpegXLLossless(const QString &filename)
{
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isJpegXLLossless: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 12;
        unsigned char header[numBytesToRead];
        bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead &&
                      ((header[0] == 0xff && header[1] == 0x0a)
                       || (header[0] == 0x00 && header[1] == 0x00 && header[2] == 0x00 && header[3] == 0x0c && header[4] == 0x4a && header[5] == 0x58 && header[6] == 0x4c && header[7] == 0x20 && header[8] == 0x0d && header[9] == 0x0a && header[10] == 0x87 && header[11] == 0x0a));
        file.close();

        if (result) {
            int width = -1, height = -1;
            FileType fileType = IsOther;
            Colors colors = ColorsUnknown;
            result = identifyImage(filename, width, height, fileType, colors) == NoError;
            result &= fileType == IsJpegXLLossless && colors == ColorsIndexed;
            if ((fileType != IsJpegXLLossy && fileType != IsJpegXLLossless) || (colors != ColorsIndexed && colors != ColorsTrueColor))
                qWarning() << "Failed to identify JPEG XL file with those parameters:" << filename << width << height << fileType << colors;
        }
        return result;
    } else {
        qWarning() << "ImageOperations::isJpegXLLossless: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isJpegXLLossy(const QString &filename)
{
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isJpegXLLossy: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 12;
        unsigned char header[numBytesToRead];
        bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead &&
                      ((header[0] == 0xff && header[1] == 0x0a)
                       || (header[0] == 0x00 && header[1] == 0x00 && header[2] == 0x00 && header[3] == 0x0c && header[4] == 0x4a && header[5] == 0x58 && header[6] == 0x4c && header[7] == 0x20 && header[8] == 0x0d && header[9] == 0x0a && header[10] == 0x87 && header[11] == 0x0a));
        file.close();

        if (result) {
            int width = -1, height = -1;
            FileType fileType = IsOther;
            Colors colors = ColorsUnknown;
            result = identifyImage(filename, width, height, fileType, colors) == NoError;
            result &= fileType == IsJpegXLLossy && colors == ColorsTrueColor;
            if ((fileType != IsJpegXLLossy && fileType != IsJpegXLLossless) || (colors != ColorsIndexed && colors != ColorsTrueColor))
                qWarning() << "Failed to identify JPEG XL file with those parameters:" << filename << width << height << fileType << colors;
        }
        return result;
    } else {
        qWarning() << "ImageOperations::isJpegXLLossy: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isPNG(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isPNG: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 8;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead && header[0] == 0x89 && header[1] == 0x50 && header[2] == 0x4e && header[3] == 0x47 && header[4] == 0x0d && header[5] == 0x0a && header[6] == 0x1a && header[7] == 0x0a;
        file.close();
        return result;
    } else {
        qWarning() << "ImageOperations::isPNG: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isTIFF(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isTIFF: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 4;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead && ((header[0] == 0x49 && header[1] == 0x49 && header[2] == 0x2a && header[3] == 0x00) || (header[0] == 0x4d && header[1] == 0x4d && header[2] == 0x00 && header[3] == 0x2a));
        file.close();
        return result;
    } else {
        qWarning() << "ImageOperations::isTIFF: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isGIF(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isGIF: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 6;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead && header[0] == 'G' && header[1] == 'I' && header[2] == 'F' && header[3] == '8' && (header[4] == '7' || header[4] == '9') && header[5] == 'a';
        file.close();
        return result;
    } else {
        qWarning() << "ImageOperations::isGIF: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isPBM(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isPBM: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 2;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead && header[0] == 'P' && (header[1] == '1' || header[1] == '4');
        file.close();
        return result;
    } else {
        qWarning() << "ImageOperations::isPBM: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isPGM(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isPGM: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 2;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead && header[0] == 'P' && (header[1] == '2' || header[1] == '5');
        file.close();
        return result;
    } else {
        qWarning() << "ImageOperations::isPGM: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isPPM(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isPPM: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 2;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead && header[0] == 'P' && (header[1] == '3' || header[1] == '6');
        file.close();
        return result;
    } else {
        qWarning() << "ImageOperations::isPPM: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isWebPLossless(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isWebPLossless: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 16;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead && header[0] == 'R' && header[1] == 'I' && header[2] == 'F' && header[3] == 'F' && header[8] == 'W' && header[9] == 'E' && header[10] == 'B' && header[11] == 'P' && header[12] == 'V' && header[13] == 'P' && header[14] == '8' && header[15] == 'L';
        file.close();
        return result;
    } else {
        qWarning() << "ImageOperations::isWebPLossless: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isWebPLossy(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isWebPLossy: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 16;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead && header[0] == 'R' && header[1] == 'I' && header[2] == 'F' && header[3] == 'F' && header[8] == 'W' && header[9] == 'E' && header[10] == 'B' && header[11] == 'P' && header[12] == 'V' && header[13] == 'P' && header[14] == '8' && header[15] != 'L';
        file.close();
        return result;
    } else {
        qWarning() << "ImageOperations::isWebPLossy: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isOTF(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isOTF: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 4;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead && header[0] == 'O' && header[1] == 'T' && header[2] == 'T' && header[3] == 'O';
        file.close();
        return result;
    } else {
        qWarning() << "ImageOperations::isOTF: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isTTF(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isTTF: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 4;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead && header[0] == 0x00 && header[1] == 0x01 && header[2] == 0x00 && header[3] == 0x00;
        file.close();
        return result;
    } else {
        qWarning() << "ImageOperations::isTTF: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isPFB(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isPFB: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 2;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead && header[0] == 0x80 && header[1] == 0x01;
        file.close();
        return result;
    } else {
        qWarning() << "ImageOperations::isPFB: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isPDF(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isPDF: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 4;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead && header[0] == '%' && header[1] == 'P' && header[2] == 'D' && header[3] == 'F';
        file.close();
        return result;
    } else {
        qWarning() << "ImageOperations::isPDF: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isEPS(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isEPS: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 24;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead && (
                                (header[0] == 0xc5 && header[1] == 0xd0 && header[2] == 0xd3 && header[3] == 0xc6)
                                ||
                                (header[0] == '%' && header[1] == '!' && header[2] == 'P' && header[3] == 'S' && header[4] == '-' && header[5] == 'A' && header[8] == 'b' && header[10] == '-' && header[15] == 'E' && header[17] == 'S')
                            );
        file.close();
        return result;
    } else {
        qWarning() << "ImageOperations::isEPS: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isSVG(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isSVG: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 256;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) > numBytesToRead / 2 && QString::fromUtf8(reinterpret_cast<char *>(header)).contains(QStringLiteral("<svg"));
        return result;
    } else {
        qWarning() << "ImageOperations::isSVG: could not open file:" << filename;
        return false;
    }
}

bool ImageOperations::isAVIF(const QString &filename)
{
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::isAVIF: file does not exist:" << filename;
        return false;
    }

    QFile file(filename);
    if (file.open(QFile::ReadOnly)) {
        static const int numBytesToRead = 12;
        unsigned char header[numBytesToRead];
        const bool result = file.read(reinterpret_cast<char *>(header), numBytesToRead) == numBytesToRead && header[8] == 'a' && header[9] == 'v' && header[10] == 'i' && header[11] == 'f';
        return result;
    } else {
        qWarning() << "ImageOperations::isAVIF: could not open file:" << filename;
        return false;
    }
}

ErrorCode ImageOperations::isGrayscalePalette(const QString &filename, bool &isGrayscale)
{
    isGrayscale = false;
    QByteArray output;
    QProcess imageMagicConvert(QCoreApplication::instance());
    imageMagicConvert.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
    connect(&imageMagicConvert, &QProcess::readyReadStandardOutput, [&imageMagicConvert, &output]() {
        output.append(imageMagicConvert.readAllStandardOutput());
    });
    connect(&imageMagicConvert, &QProcess::readyReadStandardError, [&imageMagicConvert]() {
        QTextStream ts(imageMagicConvert.readAllStandardError());
        while (!ts.atEnd()) {
            const QString line = ts.readLine(256);
            qWarning() << "Convert:" << line;
        }
    });
    const QStringList args {filename, QStringLiteral("-format"), QStringLiteral("%c"), QStringLiteral("-depth"), QStringLiteral("8"), QStringLiteral("histogram:info:-")};

    imageMagicConvert.start(QStringLiteral("convert"), args);
    if (!imageMagicConvert.waitForStarted()) {
        qWarning() << "ImageOperations::isGrayscalePalette: failed to start command" << FileOperations::commandLine(&imageMagicConvert);
        return ExternalCommandFailed;
    }
    if (!imageMagicConvert.waitForFinished(120000)) {
        qWarning() << "ImageOperations::isGrayscalePalette: failed to finish command" << FileOperations::commandLine(&imageMagicConvert);
        return ExternalCommandFailed;
    }

    ErrorCode result = imageMagicConvert.exitStatus() == QProcess::NormalExit && imageMagicConvert.exitCode() == 0 ? NoError : ExternalCommandFailed;
    if (result != NoError) {
        qWarning() << "ImageOperations::isGrayscalePalette: process existed with error" << FileOperations::commandLine(&imageMagicConvert) << "(exit code" << imageMagicConvert.exitCode() << ")";
        return result;
    }

    QTextStream ts(&output, QIODevice::ReadOnly);
    static const QRegularExpression histogramRegExp(QStringLiteral("^\\s*(\\d+): \\(\\s*(\\d+),\\s*(\\d+),\\s*(\\d+)\\) #"));
    while (!ts.atEnd()) {
        const QString line = ts.readLine(256);
        const QRegularExpressionMatch histogramRegExpMatch = histogramRegExp.match(line);
        if (!histogramRegExpMatch.hasMatch()) {
            isGrayscale = false;
            return NoError;
        } else {
            bool ok = false;
            const int r = histogramRegExpMatch.captured(2).toInt(&ok);
            if (!ok || r < 0 || r > 255)
                return ExternalCommandFailed;
            ok = false;
            const int g = histogramRegExpMatch.captured(3).toInt(&ok);
            if (!ok || g < 0 || g > 255)
                return ExternalCommandFailed;
            ok = false;
            const int b = histogramRegExpMatch.captured(4).toInt(&ok);
            if (!ok || b < 0 || b > 255)
                return ExternalCommandFailed;
#define diff(a,b) (qAbs((a)-(b)))
            if (diff(r, g) >= 2 || diff(r, b) >= 2 || diff(g, b) >= 2) {
                isGrayscale = false;
                return NoError;
            }
        }
    }

    isGrayscale = true;
    return NoError;
}

ErrorCode ImageOperations::identifyImage(const QString &filename, int &width, int &height, FileType &fileType, Colors &colors) {
    width = height = -1;
    fileType = IsOther;
    colors = ColorsUnknown;

    QByteArray output;
    QProcess imageMagicIdentify(QCoreApplication::instance());
    imageMagicIdentify.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
    connect(&imageMagicIdentify, &QProcess::readyReadStandardOutput, [&imageMagicIdentify, &output]() {
        output.append(imageMagicIdentify.readAllStandardOutput());
    });
    connect(&imageMagicIdentify, &QProcess::readyReadStandardError, [&imageMagicIdentify]() {
        QTextStream ts(imageMagicIdentify.readAllStandardError());
        while (!ts.atEnd()) {
            const QString line = ts.readLine(256);
            qWarning() << "Identify:" << line;
        }
    });
    const QStringList args {QStringLiteral("-format"), QStringLiteral("%W\\n%H\\n%e\\n%[type]\\n"), filename};

    imageMagicIdentify.start(QStringLiteral("identify"), args);
    if (!imageMagicIdentify.waitForStarted()) {
        qWarning() << "ImageOperations::identifyImage: failed to start command" << FileOperations::commandLine(&imageMagicIdentify);
        return ExternalCommandFailed;
    }
    if (!imageMagicIdentify.waitForFinished(120000)) {
        qWarning() << "ImageOperations::identifyImage: failed to finish command" << FileOperations::commandLine(&imageMagicIdentify);
        return ExternalCommandFailed;
    }

    ErrorCode result = imageMagicIdentify.exitStatus() == QProcess::NormalExit && imageMagicIdentify.exitCode() == 0 ? NoError : ExternalCommandFailed;
    if (result != NoError) {
        qWarning() << "ImageOperations::identifyImage: process existed with error" << FileOperations::commandLine(&imageMagicIdentify) << "(exit code" << imageMagicIdentify.exitCode() << ")";
        const QString errorOutput = QString::fromUtf8(imageMagicIdentify.readAllStandardError());
        qDebug() << errorOutput;
        width = height = -1;
        return result;
    }

    QTextStream ts(&output, QIODevice::ReadOnly);
    bool ok = false;
    width = ts.readLine(6).toInt(&ok);
    if (!ok || width < 1 || width > 0xffff) {
        qWarning() << "ImageOperations::identifyImage: could not identify image's width";
        width = height = -1;
        return ExternalCommandFailed;
    }
    ok = false;
    height = ts.readLine(6).toInt(&ok);
    if (!ok || height < 1 || height > 0xffff) {
        qWarning() << "ImageOperations::identifyImage: could not identify image's height";
        width = height = -1;
        return ExternalCommandFailed;
    }

    const QString formatString = ts.readLine(8).toLower();
    if (formatString == QStringLiteral("png"))
        fileType = IsPNG;
    else if (formatString.startsWith(QStringLiteral("tif")))
        fileType = IsTIFF;
    else if (formatString == QStringLiteral("webp") && isWebPLossless(filename))
        fileType = IsWebPLossless;
    else if (formatString == QStringLiteral("webp") && isWebPLossy(filename))
        fileType = IsWebPLossy;
    else if (formatString == QStringLiteral("jpg"))
        fileType = IsJPEG;
    else if (formatString == QStringLiteral("jxl")) {
        /// Check for lossless later when know if a index palette or not
        fileType = IsJpegXLLossy;
    } else if (formatString == QStringLiteral("ppm"))
        fileType = IsPPM;
    else if (formatString == QStringLiteral("pgm"))
        fileType = IsPGM;
    else if (formatString == QStringLiteral("pbm"))
        fileType = IsPBM;
    else if (formatString == QStringLiteral("avif"))
        fileType = IsAVIF;
    else
        fileType = ImageOperations::fileTypeForFileName(filename);

    const QString colorString = ts.readLine(32).toLower();
    if (colorString.startsWith(QStringLiteral("palette")) || colorString.startsWith(QStringLiteral("bilevel"))) {
        colors = ColorsIndexed;
        if (fileType == IsJpegXLLossy)
            fileType = IsJpegXLLossless;
    } else if (colorString.startsWith(QStringLiteral("grayscale")))
        colors = ColorsGrayscale;
    else if (colorString.startsWith(QStringLiteral("truecolor")))
        colors = ColorsTrueColor;

    if ((fileType == IsWebPLossless || fileType == IsWebPLossy || fileType == IsJpegXLLossless || fileType == IsJpegXLLossy) && colors == ColorsIndexed) {
        /// For .webp and (possibly .jxl) , detection of grayscale does not work always,
        /// so make a manual check of the palette in case the color mode is 'indexed'
        bool isGrayscale = false;
        if (isGrayscalePalette(filename, isGrayscale) == NoError) {
            if (isGrayscale) colors = ColorsGrayscale;
        }
    }

    return fileType != IsOther && colors != ColorsUnknown ? NoError : UnspecifiedError;
}

ErrorCode ImageOperations::touch(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::touch: file does not exist:" << filename;
        return FileDoesNotExist;
    }

    if (isJPEG(filename))
        return touchJPEG(filename);
    else if (isPNG(filename))
        return touchPNG(filename);
    else
        return FileOperations::touchFile(filename);
}

ErrorCode ImageOperations::touchPNG(const QString &filename) {
    if (!QFile::exists(filename)) {
        qWarning() << "ImageOperations::touchPNG: file does not exist:" << filename;
        return FileDoesNotExist;
    }

    if (isPNG(filename)) {
        if (!QStandardPaths::findExecutable(QStringLiteral("optipng")).isEmpty()) {
            QProcess optiPng(this);
            optiPng.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
            const QStringList args {QStringLiteral("-fix"), QStringLiteral("-preserve"), filename};
            optiPng.start(QStringLiteral("optipng"), args);

            if (!optiPng.waitForStarted()) {
                qWarning() << "ImageOperations::touchPNG: failed to start command" << FileOperations::commandLine(&optiPng);
                return ExternalCommandFailed;
            }
            if (!optiPng.waitForFinished(120000)) {
                qWarning() << "ImageOperations::touchPNG: failed to finish command" << FileOperations::commandLine(&optiPng);
                return ExternalCommandFailed;
            }
            const ErrorCode result = optiPng.exitStatus() == QProcess::NormalExit && optiPng.exitCode() == 0 ? NoError : ExternalCommandFailed;
            if (result != NoError)
                qWarning() << "ImageOperations::touchPNG: process existed with error" << FileOperations::commandLine(&optiPng) << "(exit code" << optiPng.exitCode() << ")";
            return result;
        } else if (!QStandardPaths::findExecutable(QStringLiteral("pngcrush")).isEmpty()) {
            QProcess pngCrush(this);
            pngCrush.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
            const QString tempFilename = FileOperations::temporaryFilename(QStringLiteral("pngcrush"), QStringLiteral(".png"));
            const QStringList args {filename, tempFilename};
            pngCrush.start(QStringLiteral("pngcrush"), args);

            if (!pngCrush.waitForStarted()) {
                qWarning() << "ImageOperations::touchPNG: failed to start command" << FileOperations::commandLine(&pngCrush);
                return ExternalCommandFailed;
            }
            if (!pngCrush.waitForFinished(120000)) {
                qWarning() << "ImageOperations::touchPNG: failed to finish command" << FileOperations::commandLine(&pngCrush);
                return ExternalCommandFailed;
            }
            ErrorCode result = pngCrush.exitStatus() == QProcess::NormalExit && pngCrush.exitCode() == 0 ? NoError : ExternalCommandFailed;
            if (result != NoError)
                qWarning() << "ImageOperations::touchPNG: process existed with error" << FileOperations::commandLine(&pngCrush) << "(exit code" << pngCrush.exitCode() << ")";
            else
                result = FileOperations::moveFile(tempFilename, filename, true) == NoError ? NoError : UnspecifiedError;
            return result;
        } else {
            qWarning() << "ImageOperations::touchPNG: no known command installed to touch PNG files";
            return ExternalCommandFailed;
        }
    } else {
        qWarning() << "ImageOperations::touchPNG: non-PNG files not supported in \"touch png\" operation:" << filename;
        return UnsupportedOperation;
    }
}

bool ImageOperations::canJpegXL()
{
    return QImageReader::supportedImageFormats().contains("jxl");
}

bool ImageOperations::canScale(const QString &filename) {
    const FileType ft = fileTypeForFileName(filename);
    return canScale(ft);
}

bool ImageOperations::canScale(const ImageOperations::FileType ft)
{
    return ft == IsJPEG || ft == IsJpegXLLossless || ft == IsJpegXLLossy || ft == IsAVIF || ft == IsPGM || ft == IsPNG || ft == IsTIFF || ft == IsPPM || ft == IsWebPLossy || ft == IsWebPLossless;
}

ErrorCode ImageOperations::scale(const QString &inputFilename, const QString &outputFilename, int outputWidth, int outputHeight, const QStringList &optionalArguments) {
    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::scale: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::scale: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    QString actualInputFilename = inputFilename;
#ifdef LIBAVIF_CMDLINE
    QTemporaryFile pngInputFile(QStringLiteral("qdisplay5-convert-image-XXXXXX.png"), this);
    pngInputFile.setAutoRemove(true);
    if (isAVIF(inputFilename) && pngInputFile.open()) {
        pngInputFile.close();
        const QString pngInputFilename = pngInputFile.fileName();
        QFile::remove(pngInputFilename);
        QProcess avifdec(this);
        avifdec.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
        const QStringList args {QStringLiteral("--png-compress"), QStringLiteral("0"), inputFilename, pngInputFilename};
        avifdec.start(QStringLiteral("avifdec"), args);

        if (!avifdec.waitForStarted()) {
            qWarning() << "ImageOperations::scale: failed to start command" << FileOperations::commandLine(&avifdec);
            return ExternalCommandFailed;
        }
        if (!avifdec.waitForFinished(120000)) {
            qWarning() << "ImageOperations::scale: failed to finish command" << FileOperations::commandLine(&avifdec);
            return ExternalCommandFailed;
        }

        ErrorCode result = avifdec.exitStatus() == QProcess::NormalExit && avifdec.exitCode() == 0 ? NoError : ExternalCommandFailed;
        if (result != NoError) {
            qWarning() << "ImageOperations::scale: process existed with error" << FileOperations::commandLine(&avifdec) << "(exit code" << avifdec.exitCode() << ")";
            const QString errorOutput = QString::fromUtf8(avifdec.readAllStandardError());
            qDebug() << errorOutput;
            return result;
        } else if (!QFile::exists(pngInputFilename)) {
            qWarning() << "ImageOperations::scale: no output file generated:" << outputFilename;
            return ExternalCommandFailed;
        } else
            actualInputFilename = pngInputFilename;
    }
#endif // LIBAVIF_CMDLINE

    int inputWidth = 0, inputHeight = 0;
    FileType inputFileType = IsOther;
    Colors inputColors = ColorsUnknown;
    const ErrorCode identifyImageResult = identifyImage(actualInputFilename, inputWidth, inputHeight, inputFileType, inputColors);
    if (identifyImageResult != NoError)
        return identifyImageResult;

#ifdef LIBAVIF_CMDLINE
    const bool convertToAVIF = outputFilename.right(5).toLower() == QStringLiteral(".avif");
    if (convertToAVIF) {
        qWarning() << "ImageOperations::scale: AVIF output not supported, use scaleToAVIF directly";
        return UnsupportedOperation;
    }
#endif // LIBAVIF_CMDLINE

    QProcess convert(this);
    convert.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
    QStringList args {actualInputFilename};
    if (outputWidth != inputWidth || outputHeight != inputHeight)
        args << QStringLiteral("-scale") << QString(QStringLiteral("%1x%2!")).arg(outputWidth).arg(outputHeight);
    if (!optionalArguments.isEmpty())
        args << optionalArguments;
    args << outputFilename;
    convert.start(QStringLiteral("convert"), args);

    if (!convert.waitForStarted()) {
        qWarning() << "ImageOperations::scale: failed to start command" << FileOperations::commandLine(&convert);
        return ExternalCommandFailed;
    }
    if (!convert.waitForFinished(120000)) {
        qWarning() << "ImageOperations::scale: failed to finish command" << FileOperations::commandLine(&convert);
        return ExternalCommandFailed;
    }

    const ErrorCode result = convert.exitStatus() == QProcess::NormalExit && convert.exitCode() == 0 ? NoError : ExternalCommandFailed;
    if (result != NoError) {
        qWarning() << "ImageOperations::scale: process existed with error" << FileOperations::commandLine(&convert) << "(exit code" << convert.exitCode() << ")";
        const QString errorOutput = QString::fromUtf8(convert.readAllStandardError());
        qDebug() << errorOutput;
        return result;
    } else if (!QFile::exists(outputFilename)) {
        qWarning() << "ImageOperations::scale: no output file generated:" << outputFilename;
        return ExternalCommandFailed;
    } else
        return NoError;
}

ErrorCode ImageOperations::scaleToJPEG(const QString &inputFilename, const QString &outputFilename, int width, int height, int quality, double gamma, bool normalize) {
    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::scaleToJPEG: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::scaleToJPEG: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    if (outputFilename.right(5).toLower() != QStringLiteral(".jpeg") && outputFilename.right(4).toLower() != QStringLiteral(".jpg")) {
        qWarning() << "ImageOperations::scaleToJPEG: output filename does not end in '.jpg' or '.jpeg':" << outputFilename;
        return UnsupportedFileType;
    }

    QStringList optionalArguments = {QStringLiteral("-quality"), QString::number(quality)};
    if (normalize)
        optionalArguments << QStringLiteral("-contrast-stretch") << QStringLiteral("0");
    if (gamma < -0.5)
        optionalArguments << QStringLiteral("-auto-gamma");
    else if (gamma > 0.5)
        optionalArguments << QStringLiteral("-gamma") << QString::number(gamma);

    return scale(inputFilename, outputFilename, width, height, optionalArguments);
}

ErrorCode ImageOperations::scaleToJpegXLLossless(const QString &inputFilename, const QString &outputFilename, int width, int height, int numColors, bool dither, double gamma, bool normalize)
{
    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::scaleToJpegXLLossless: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::scaleToJpegXLLossless: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    if (outputFilename.right(4).toLower() != QStringLiteral(".jxl")) {
        qWarning() << "ImageOperations::scaleToJpegXLLossless: output filename does not end in '.jxl':" << outputFilename;
        return UnsupportedFileType;
    }

    QString actualInputFilename = inputFilename;
    QTemporaryFile pngInputFile(QStringLiteral("qdisplay5-convert-image-XXXXXX.png"), this);
    pngInputFile.setAutoRemove(true);
    if (pngInputFile.open()) {
        pngInputFile.close();
        actualInputFilename = pngInputFile.fileName();
        QFile::remove(actualInputFilename);
        ErrorCode result = scaleToPNG(inputFilename, actualInputFilename, width, height, numColors, dither, gamma, normalize, false);
        if (result != NoError) {
            qWarning() << "ImageOperations::scaleToJpegXLLossless: failed to scale input file to PNG";
            return result;
        }
    } else {
        qWarning() << "ImageOperations::scaleToJpegXLLossless: Failed to open temporary PNG file";
        return FileDoesNotExist;
    }

    QProcess cjxl(this);
    cjxl.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
    const QStringList args {actualInputFilename, outputFilename, QStringLiteral("-q"), QStringLiteral("100"), QStringLiteral("-s"), QStringLiteral("9"), QStringLiteral("-g"), QStringLiteral("3")};
    cjxl.start(QStringLiteral("cjxl"), args);

    if (!cjxl.waitForStarted()) {
        qWarning() << "ImageOperations::scaleToJpegXLLossless: failed to start command" << FileOperations::commandLine(&cjxl);
        return ExternalCommandFailed;
    }
    if (!cjxl.waitForFinished(240000)) {
        qWarning() << "ImageOperations::scaleToJpegXLLossless: failed to finish command" << FileOperations::commandLine(&cjxl);
        return ExternalCommandFailed;
    }

    ErrorCode result = cjxl.exitStatus() == QProcess::NormalExit && cjxl.exitCode() == 0 ? NoError : ExternalCommandFailed;
    if (result != NoError) {
        qWarning() << "ImageOperations::scaleToJpegXLLossless: process existed with error" << FileOperations::commandLine(&cjxl) << "(exit code" << cjxl.exitCode() << ")";
        const QString errorOutput = QString::fromUtf8(cjxl.readAllStandardError());
        qDebug() << errorOutput;
        return result;
    } else if (!QFile::exists(outputFilename)) {
        qWarning() << "ImageOperations::scaleToJpegXLLossless: no output file generated:" << outputFilename;
        return ExternalCommandFailed;
    } else // result==NoError
        return NoError;
}

ErrorCode ImageOperations::scaleToJpegXLLossy(const QString &inputFilename, const QString &outputFilename, int width, int height, int quality, double gamma, bool normalize)
{
    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::scaleToJpegXLLossy: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::scaleToJpegXLLossy: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    if (outputFilename.right(4).toLower() != QStringLiteral(".jxl")) {
        qWarning() << "ImageOperations::scaleToJpegXLLossy: output filename does not end in '.jxl':" << outputFilename;
        return UnsupportedFileType;
    }

    int curWidth = -1, curHeight = -1;
    Colors curColors = Colors::ColorsUnknown;
    FileType curFileType = FileType::IsOther;
    ErrorCode result = identifyImage(inputFilename, curWidth, curHeight, curFileType, curColors);
    if (result != NoError) {
        qWarning() << "ImageOperations::scaleToJpegXLLossy: failed identify image:" << inputFilename;
        return result;
    }
    QString actualInputFilename = inputFilename;
    // Temporary PNG file must be declared outside of following if-clause
    // to allow temporary file's existance until end of this function
    QTemporaryFile pngInputFile(QStringLiteral("qdisplay5-convert-image-XXXXXX.png"), this);
    pngInputFile.setAutoRemove(true);
    static const QSet<FileType> cjxlSupportedInputFormats{FileType::IsPNG, FileType::IsGIF, FileType::IsPPM, FileType::IsJPEG};
    if (curWidth != width || curHeight != height || !cjxlSupportedInputFormats.contains(curFileType)) {
        if (pngInputFile.open()) {
            pngInputFile.close();
            actualInputFilename = pngInputFile.fileName();
            QFile::remove(actualInputFilename);
            result = scaleToPNG(inputFilename, actualInputFilename, width, height, 0x7fff, false, gamma, normalize, false);
            if (result != NoError) {
                qWarning() << "ImageOperations::scaleToJpegXLLossy: failed to scale input file to PNG";
                return result;
            }
        } else {
            qWarning() << "ImageOperations::scaleToJpegXLLossy: Failed to open temporary PNG file";
            return FileDoesNotExist;
        }
    }

    QProcess cjxl(this);
    cjxl.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
    const QStringList args {actualInputFilename, outputFilename, QStringLiteral("-q"), QString::number(quality)};
    cjxl.start(QStringLiteral("cjxl"), args);

    if (!cjxl.waitForStarted()) {
        qWarning() << "ImageOperations::scaleToJpegXLLossy: failed to start command" << FileOperations::commandLine(&cjxl);
        return ExternalCommandFailed;
    }
    if (!cjxl.waitForFinished(240000)) {
        qWarning() << "ImageOperations::scaleToJpegXLLossy: failed to finish command" << FileOperations::commandLine(&cjxl);
        return ExternalCommandFailed;
    }

    result = cjxl.exitStatus() == QProcess::NormalExit && cjxl.exitCode() == 0 ? NoError : ExternalCommandFailed;
    if (result != NoError) {
        qWarning() << "ImageOperations::scaleToJpegXLLossy: process existed with error" << FileOperations::commandLine(&cjxl) << "(exit code" << cjxl.exitCode() << ")";
        const QString errorOutput = QString::fromUtf8(cjxl.readAllStandardError());
        qDebug() << errorOutput;
        return result;
    } else if (!QFile::exists(outputFilename)) {
        qWarning() << "ImageOperations::scaleToJpegXLLossy: no output file generated:" << outputFilename;
        return ExternalCommandFailed;
    } else // result==NoError
        return NoError;
}

ErrorCode ImageOperations::scaleToAVIF(const QString &inputFilename, const QString &outputFilename, int width, int height, int quality, double gamma, bool normalize)
{
    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::scaleToAVIF: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::scaleToAVIF: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    if (outputFilename.right(5).toLower() != QStringLiteral(".avif")) {
        qWarning() << "ImageOperations::scaleToAVIF: output filename does not end in '.avif':" << outputFilename;
        return UnsupportedFileType;
    }

    QString actualInputFilename = inputFilename;
#ifdef LIBAVIF_CMDLINE
    int curWidth = -1, curHeight = -1;
    Colors curColors = Colors::ColorsUnknown;
    FileType curFileType = FileType::IsOther;
    ErrorCode result = identifyImage(inputFilename, curWidth, curHeight, curFileType, curColors);
    if (result != NoError) {
        qWarning() << "ImageOperations::scaleToAVIF: failed identify image:" << inputFilename;
        return result;
    }
    // Temporary PNG file must be declared outside of following if-clause
    // to allow temporary file's existance until end of this function
    QTemporaryFile pngInputFile(QStringLiteral("qdisplay5-convert-image-XXXXXX.png"), this);
    pngInputFile.setAutoRemove(true);
    static const QSet<FileType> avifencSupportedInputFormats{FileType::IsPNG, FileType::IsJPEG};
    if (curWidth != width || curHeight != height || !avifencSupportedInputFormats.contains(curFileType)) {
        if (pngInputFile.open()) {
            pngInputFile.close();
            actualInputFilename = pngInputFile.fileName();
            QFile::remove(actualInputFilename);
            result = scaleToPNG(inputFilename, actualInputFilename, width, height, 0x7fff, false, gamma, normalize);
            if (result != NoError) {
                qWarning() << "ImageOperations::scaleToAVIF: failed to scale input file to PNG";
                return result;
            }
        } else {
            qWarning() << "ImageOperations::scaleToAVIF: Failed to open temporary PNG file";
            return FileDoesNotExist;
        }
    }

    QProcess avifenc(this);
    avifenc.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
    const QString q = QString::number(((100 - quality) * 63 + 50) / 100);
    const QStringList args {actualInputFilename, QStringLiteral("--min"), q, QStringLiteral("--max"), q, outputFilename};
    avifenc.start(QStringLiteral("avifenc"), args);

    if (!avifenc.waitForStarted()) {
        qWarning() << "ImageOperations::scaleToAVIF: failed to start command" << FileOperations::commandLine(&avifenc);
        return ExternalCommandFailed;
    }
    if (!avifenc.waitForFinished(240000)) {
        qWarning() << "ImageOperations::scaleToAVIF: failed to finish command" << FileOperations::commandLine(&avifenc);
        return ExternalCommandFailed;
    }

    result = avifenc.exitStatus() == QProcess::NormalExit && avifenc.exitCode() == 0 ? NoError : ExternalCommandFailed;
    if (result != NoError) {
        qWarning() << "ImageOperations::scaleToAVIF: process existed with error" << FileOperations::commandLine(&avifenc) << "(exit code" << avifenc.exitCode() << ")";
        const QString errorOutput = QString::fromUtf8(avifenc.readAllStandardError());
        qDebug() << errorOutput;
        return result;
    } else if (!QFile::exists(outputFilename)) {
        qWarning() << "ImageOperations::scaleToAVIF: no output file generated:" << outputFilename;
        return ExternalCommandFailed;
    } else // result==NoError
        return NoError;
#else // LIBAVIF_CMDLINE
    QStringList optionalArguments = {QStringLiteral("-quality"), QString::number((quality * 63 + 50) / 100)};
    if (normalize)
        optionalArguments << QStringLiteral("-contrast-stretch") << QStringLiteral("0");
    if (gamma < -0.5)
        optionalArguments << QStringLiteral("-auto-gamma");
    else if (gamma > 0.5)
        optionalArguments << QStringLiteral("-gamma") << QString::number(gamma);

    return scale(inputFilename, outputFilename, width, height, optionalArguments);
#endif // LIBAVIF_CMDLINE
}

ErrorCode ImageOperations::scaleToPNG(const QString &inputFilename, const QString &outputFilename, int width, int height, int numColors, bool dither, double gamma, bool normalize, bool compress) {
    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::scaleToPNG: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::scaleToPNG: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    if (outputFilename.right(4).toLower() != QStringLiteral(".png")) {
        qWarning() << "ImageOperations::scaleToPNG: output filename does not end in '.png':" << outputFilename;
        return UnsupportedFileType;
    }

    QStringList optionalArguments;
    if (gamma < -0.5)
        optionalArguments << QStringLiteral("-auto-gamma");
    else if (gamma > 0.5)
        optionalArguments << QStringLiteral("-gamma") << QString::number(gamma);
    if (normalize)
        optionalArguments << QStringLiteral("-contrast-stretch") << QStringLiteral("0");
    if (dither)
        optionalArguments << QStringLiteral("-dither") << QStringLiteral("FloydSteinberg");
    else
        optionalArguments << QStringLiteral("+dither");
    if (numColors <= 256)
        optionalArguments << QStringLiteral("-colors") << QString::number(numColors) << QStringLiteral("-depth") << QString::number(Private::log2_roundup_powerof2(numColors));
    optionalArguments << QStringLiteral("-quality") << (compress ? QStringLiteral("9") : QStringLiteral("1"));

    return scale(inputFilename, outputFilename, width, height, optionalArguments);
}

ErrorCode ImageOperations::scaleToTIFF(const QString &inputFilename, const QString &outputFilename, int width, int height, int numColors, bool dither, double gamma, bool normalize, bool compress) {
    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::scaleToTIFF: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::scaleToTIFF: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    if (outputFilename.right(5).toLower() != QStringLiteral(".tiff") && outputFilename.right(4).toLower() != QStringLiteral(".tif")) {
        qWarning() << "ImageOperations::scaleToTIFF: output filename does not end in '.tiff' or '.tif':" << outputFilename;
        return UnsupportedFileType;
    }

    QStringList optionalArguments;
    if (gamma < -0.5)
        optionalArguments << QStringLiteral("-auto-gamma");
    else if (gamma > 0.5)
        optionalArguments << QStringLiteral("-gamma") << QString::number(gamma);
    if (normalize)
        optionalArguments << QStringLiteral("-contrast-stretch") << QStringLiteral("0");
    if (dither)
        optionalArguments << QStringLiteral("-dither") << QStringLiteral("FloydSteinberg");
    else
        optionalArguments << QStringLiteral("+dither");
    if (numColors <= 256)
        optionalArguments << QStringLiteral("-colors") << QString::number(numColors) << QStringLiteral("-depth") << QString::number(Private::log2_roundup_powerof2(numColors));
    optionalArguments << QStringLiteral("-compress") << (compress ? QStringLiteral("LZW") : QStringLiteral("None"));

    return scale(inputFilename, outputFilename, width, height, optionalArguments);
}

ErrorCode ImageOperations::scaleToPGM(const QString &inputFilename, const QString &outputFilename, int width, int height, double gamma, bool normalize) {
    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::scaleToPGM: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::scaleToPGM: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    if (outputFilename.right(4).toLower() != QStringLiteral(".pgm")) {
        qWarning() << "ImageOperations::scaleToPGM: output filename does not end in '.pgm':" << outputFilename;
        return UnsupportedFileType;
    }

    QStringList optionalArguments;
    if (gamma < -0.5)
        optionalArguments << QStringLiteral("-auto-gamma");
    else if (gamma > 0.5)
        optionalArguments << QStringLiteral("-gamma") << QString::number(gamma);
    if (normalize)
        optionalArguments << QStringLiteral("-contrast-stretch") << QStringLiteral("0");

    return scale(inputFilename, outputFilename, width, height, optionalArguments);
}

ErrorCode ImageOperations::scaleToPBM(const QString &inputFilename, const QString &outputFilename, int width, int height) {
    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::scaleToPBM: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::scaleToPBM: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    if (outputFilename.right(4).toLower() != QStringLiteral(".pbm")) {
        qWarning() << "ImageOperations::scaleToPBM: output filename does not end in '.pbm':" << outputFilename;
        return UnsupportedFileType;
    }

    return scale(inputFilename, outputFilename, width, height);
}

ErrorCode ImageOperations::scaleToWebPLossless(const QString &inputFilename, const QString &outputFilename, int width, int height, int numColors, bool dither, double gamma, bool normalize, bool compress) {
    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::scaleToWebPLossless: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::scaleToWebPLossless: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    if (outputFilename.right(5).toLower() != QStringLiteral(".webp")) {
        qWarning() << "ImageOperations::scaleToWebPLossless: output filename does not end in '.webp':" << outputFilename;
        return UnsupportedFileType;
    }

    QStringList optionalArguments = {QStringLiteral("-quality"), QStringLiteral("100"), QStringLiteral("-define"), QStringLiteral("webp:lossless=true"), QStringLiteral("-define")};
    optionalArguments << QString(QStringLiteral("webp:method=%1")).arg(compress ? QStringLiteral("4") : QStringLiteral("0"));
    if (gamma < -0.5)
        optionalArguments << QStringLiteral("-auto-gamma");
    else if (gamma > 0.5)
        optionalArguments << QStringLiteral("-gamma") << QString::number(gamma);
    if (normalize)
        optionalArguments << QStringLiteral("-contrast-stretch") << QStringLiteral("0");
    if (dither)
        optionalArguments << QStringLiteral("-dither") << QStringLiteral("FloydSteinberg");
    else
        optionalArguments << QStringLiteral("+dither");
    if (numColors <= 256)
        optionalArguments << QStringLiteral("-colors") << QString::number(numColors) << QStringLiteral("-depth") << QString::number(Private::log2_roundup_powerof2(numColors));
    return scale(inputFilename, outputFilename, width, height, optionalArguments);
}

ErrorCode ImageOperations::scaleToWebPLossy(const QString &inputFilename, const QString &outputFilename, int width, int height, int quality, double gamma, bool normalize) {
    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::scaleToWebPLossy: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::scaleToWebPLossy: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    if (outputFilename.right(5).toLower() != QStringLiteral(".webp")) {
        qWarning() << "ImageOperations::scaleToWebPLossy: output filename does not end in '.webp':" << outputFilename;
        return UnsupportedFileType;
    }

    QStringList optionalArguments = {QStringLiteral("-quality"), QString::number(quality), QStringLiteral("-define"), QStringLiteral("webp:lossless=false")};
    if (gamma < -0.5)
        optionalArguments << QStringLiteral("-auto-gamma");
    else if (gamma > 0.5)
        optionalArguments << QStringLiteral("-gamma") << QString::number(gamma);
    if (normalize)
        optionalArguments << QStringLiteral("-contrast-stretch") << QStringLiteral("0");
    return scale(inputFilename, outputFilename, width, height, optionalArguments);
}

bool ImageOperations::canCrop(const QString &filename)
{
    const FileType ft = fileTypeForFileName(filename);
    return canCrop(ft);
}

bool ImageOperations::canCrop(const ImageOperations::FileType ft)
{
    return ft == IsJPEG
#ifndef LIBAVIF_CMDLINE
           || ft == IsAVIF
#endif // LIBAVIF_CMDLINE
           // FIXME || ft == IsJpegXLLossless || ft == IsJpegXLLossy
           || ft == IsTIFF || ft == IsPBM || ft == IsPGM || ft == IsPNG || ft == IsPPM || ft == IsWebPLossy || ft == IsWebPLossless;

}

ErrorCode ImageOperations::crop(const QString &inputFilename, const QString &outputFilename, int left, int top, int width, int height) {
    if (left < 0 || left >= 16384 || top < 0 || top >= 16384 || width <= 0 || width >= 16384 || height <= 0 || height >= 16384) {
        qWarning() << "ImageOperations::crop: arguments for left, top, width, and/or height may be illegal:" << left << top << width << height;
        return InvalidArgument;
    }

    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::crop: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::crop: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    const bool isIndeedJpeg = isJPEG(inputFilename);
    if (isIndeedJpeg) {
        const ErrorCode result = cropJPEG(inputFilename, outputFilename, left, top, width, height);
        if (result == NoError)
            return NoError;
    }

    if (canCrop(inputFilename)) {
        QProcess crop(this);
        crop.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
        QStringList args {inputFilename, QStringLiteral("+repage"), QStringLiteral("-crop"), QString(QStringLiteral("%1x%2+%3+%4")).arg(width).arg(height).arg(left).arg(top), QStringLiteral("+repage")};
        if (isWebPLossless(inputFilename)) {
            args << QStringLiteral("-define") << QStringLiteral("webp:lossless=true");
            args << QStringLiteral("-quality") << QStringLiteral("100"); //< for lossless WebP, high quality means 'try hard to achieve good compression'
        } else if (isWebPLossy(inputFilename)) {
            args << QStringLiteral("-define") << QStringLiteral("webp:lossless=false");
            args << QStringLiteral("-quality") << QStringLiteral("85"); //< accept some loss of quality
        }
        args << outputFilename;
        crop.start(QStringLiteral("convert"), args);

        if (!crop.waitForStarted()) {
            qWarning() << "ImageOperations::crop: failed to start command" << FileOperations::commandLine(&crop);
            return ExternalCommandFailed;
        }
        if (!crop.waitForFinished(120000)) {
            qWarning() << "ImageOperations::crop: failed to finish command" << FileOperations::commandLine(&crop);
            return ExternalCommandFailed;
        }

        const ErrorCode result = crop.exitStatus() == QProcess::NormalExit && crop.exitCode() == 0 ? NoError : ExternalCommandFailed;
        if (result != NoError) {
            qWarning() << "ImageOperations::crop: process existed with error" << FileOperations::commandLine(&crop) << "(exit code" << crop.exitCode() << ")";
            const QString errorOutput = QString::fromUtf8(crop.readAllStandardError());
            qDebug() << errorOutput;
            return result;
        } else if (!QFile::exists(outputFilename)) {
            qWarning() << "ImageOperations::crop: no output file generated:" << outputFilename;
            return ExternalCommandFailed;
        } else /// result==NoError
            return NoError;
    } else {
        qWarning() << "ImageOperations::crop: Unsupported file type for this operation:" << inputFilename;
        return UnsupportedOperation;
    }
}

bool ImageOperations::canBlackBar(const QString &filename) {
    const FileType ft = fileTypeForFileName(filename);
    return canBlackBar(ft);
}

bool ImageOperations::canBlackBar(const ImageOperations::FileType ft)
{
    return ft == IsJPEG
#ifndef LIBAVIF_CMDLINE
           || ft == IsAVIF
#endif // LIBAVIF_CMDLINE
           // FIXME || ft == IsJpegXLLossless || ft == IsJpegXLLossy
           || ft == IsTIFF || ft == IsPBM || ft == IsPGM || ft == IsPNG || ft == IsPPM || ft == IsWebPLossless || ft == IsWebPLossy;
}

ErrorCode ImageOperations::blackBar(const QString &inputFilename, const QString &outputFilename, int left, int top, int width, int height) {
    if (left < 0 || left >= 16384 || top < 0 || top >= 16384 || width <= 0 || width >= 16384 || height <= 0 || height >= 16384) {
        qWarning() << "ImageOperations::blackBar: arguments for left, top, width, and/or height may be illegal:" << left << top << width << height;
        return InvalidArgument;
    }

    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::blackBar: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::blackBar: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    if (isJPEG(inputFilename)) {
        width = (((left + width) >> 4) + 1) << 4;
        height = (((top + height) >> 4) + 1) << 4;
        left = (left >> 4) << 4;
        top = (top >> 4) << 4;
        width -= left;
        height -= top;

        FILE *infile = fopen(inputFilename.toLatin1().constData(), "rb");
        if (infile == nullptr) {
            qWarning() << "ImageOperations::blackBar: cannot open input file for reading:" << inputFilename;
            return FileDoesNotExist;
        }
        FILE *outfile = fopen(outputFilename.toLatin1().constData(), "wb");
        if (outfile == nullptr) {
            fclose(infile);
            qWarning() << "ImageOperations::blackBar: cannot open output file for writing:" << outputFilename;
            return FileDoesNotExist;
        }

        struct jpeg_decompress_struct decompr_info;
        struct jpeg_error_mgr jerr_decompr;
        decompr_info.err = jpeg_std_error(&jerr_decompr);

        jpeg_create_decompress(&decompr_info);
        jpeg_stdio_src(&decompr_info, infile);
        jpeg_read_header(&decompr_info, TRUE);
        jvirt_barray_ptr *coef_arrays = jpeg_read_coefficients(&decompr_info);
        const JDIMENSION MCU_cols = decompr_info.image_width / static_cast<JDIMENSION>(decompr_info.max_h_samp_factor * DCTSIZE);

        jpeg_component_info *compptr = decompr_info.comp_info;
        for (int ci = 0; ci < decompr_info.num_components; ++ci, ++compptr) {
            const JDIMENSION blk_width = static_cast<JDIMENSION>(DCTSIZE * decompr_info.max_h_samp_factor / compptr->h_samp_factor);
            const JDIMENSION blk_height = static_cast<JDIMENSION>(DCTSIZE * decompr_info.max_v_samp_factor / compptr->v_samp_factor);

            /// For grayscale images, both compptr->h_samp_factor and compptr->h_samp_factor are always 1
            /// For color images, both compptr->h_samp_factor and compptr->h_samp_factor are 2 for luminance components and 1 for chrominance components
            const JDIMENSION comp_width = MCU_cols * static_cast<JDIMENSION>(compptr->h_samp_factor);
            for (JDIMENSION blk_y = 0; blk_y < compptr->height_in_blocks; blk_y += static_cast<JDIMENSION>(compptr->v_samp_factor)) {
                JBLOCKARRAY buffer = (*decompr_info.mem->access_virt_barray)((j_common_ptr) &decompr_info, coef_arrays[ci], blk_y, static_cast<JDIMENSION>(compptr->v_samp_factor), TRUE);

                for (int offset_y = 0; offset_y < compptr->v_samp_factor; ++offset_y) {
                    for (JDIMENSION blk_x = 0; blk_x < comp_width; ++blk_x) {
                        JCOEFPTR ptr = buffer[offset_y][blk_x];
                        const JDIMENSION actual_x = blk_x * blk_width;
                        const JDIMENSION actual_y = blk_y * blk_height;
                        if (actual_x >= static_cast<JDIMENSION>(left) && actual_x < static_cast<JDIMENSION>(left + width) && actual_y >= static_cast<JDIMENSION>(top) && actual_y < static_cast<JDIMENSION>(top + height)) {
                            ptr[0] = ci == 0 ? -1024 : 0;
                            for (int k = 1; k < DCTSIZE2; ++k)
                                ptr[k] = 0;
                        }
                    }
                }
            }
        }

        struct jpeg_compress_struct compr_info;
        struct jpeg_error_mgr jerr_compr;
        compr_info.err = jpeg_std_error(&jerr_compr);

        jpeg_create_compress(&compr_info);
        jpeg_stdio_dest(&compr_info, outfile);
        jpeg_copy_critical_parameters(&decompr_info, &compr_info);
        compr_info.in_color_space = decompr_info.out_color_space; ///< Not copied by previous call
        jpeg_write_coefficients(&compr_info, coef_arrays);

        jpeg_finish_compress(&compr_info);
        jpeg_finish_decompress(&decompr_info);
        jpeg_destroy_decompress(&decompr_info);
        jpeg_destroy_compress(&compr_info);

        fclose(infile);
        fclose(outfile);

        return NoError;
    } else if (canBlackBar(inputFilename)) {
        QProcess blackBar(this);
        blackBar.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
        QStringList args {inputFilename, QStringLiteral("-fill"), QStringLiteral("black"), QStringLiteral("-stroke"), QStringLiteral("black"), QStringLiteral("-strokewidth"), QStringLiteral("1"), QStringLiteral("-draw"), QString(QStringLiteral("rectangle %1,%2 %3,%4")).arg(left).arg(top).arg(left + width).arg(top + height)};
        if (isWebPLossless(inputFilename)) {
            args << QStringLiteral("-define") << QStringLiteral("webp:lossless=true");
            args << QStringLiteral("-quality") << QStringLiteral("100"); //< for lossless WebP, high quality means 'try hard to achieve good compression'
        } else if (isWebPLossy(inputFilename)) {
            args << QStringLiteral("-define") << QStringLiteral("webp:lossless=false");
            args << QStringLiteral("-quality") << QStringLiteral("85"); //< accept some loss of quality
        }
        args << outputFilename;
        blackBar.start(QStringLiteral("convert"), args);

        if (!blackBar.waitForStarted()) {
            qWarning() << "ImageOperations::blackBar: failed to start command" << FileOperations::commandLine(&blackBar);
            return ExternalCommandFailed;
        }
        if (!blackBar.waitForFinished(120000)) {
            qWarning() << "ImageOperations::blackBar: failed to finish command" << FileOperations::commandLine(&blackBar);
            return ExternalCommandFailed;
        }

        const ErrorCode result = blackBar.exitStatus() == QProcess::NormalExit && blackBar.exitCode() == 0 ? NoError : ExternalCommandFailed;
        if (result != NoError) {
            qWarning() << "ImageOperations::blackBar: process existed with error" << FileOperations::commandLine(&blackBar) << "(exit code" << blackBar.exitCode() << ")";
            const QString errorOutput = QString::fromUtf8(blackBar.readAllStandardError());
            qDebug() << errorOutput;
            return result;
        } else if (!QFile::exists(outputFilename)) {
            qWarning() << "ImageOperations::blackBar: no output file generated:" << outputFilename;
            return ExternalCommandFailed;
        } else /// result==NoError
            return NoError;
    } else {
        qWarning() << "ImageOperations::blackBar: Unsupported file type for this operation:" << inputFilename;
        return UnsupportedOperation;
    }
}

bool ImageOperations::canBlur(const QString &filename) {
    const FileType ft = fileTypeForFileName(filename);
    return canBlur(ft);
}

bool ImageOperations::canBlur(const ImageOperations::FileType ft)
{
    return ft == IsJPEG
#ifndef LIBAVIF_CMDLINE
           || ft == IsAVIF
#endif // LIBAVIF_CMDLINE
           // FIXME || ft == IsJpegXLLossless || ft == IsJpegXLLossy
           || ft == IsTIFF || ft == IsPBM || ft == IsPGM || ft == IsPNG || ft == IsPPM || ft == IsWebPLossless || ft == IsWebPLossy;
}

ErrorCode ImageOperations::blur(const QString &inputFilename, const QString &outputFilename, int left, int top, int width, int height) {
    if (left < 0 || left >= 16384 || top < 0 || top >= 16384 || width <= 0 || width >= 16384 || height <= 0 || height >= 16384) {
        qWarning() << "ImageOperations::blur: arguments for left, top, width, and/or height may be illegal:" << left << top << width << height;
        return InvalidArgument;
    }

    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::blur: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::blur: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    if (canBlur(inputFilename)) {
        QProcess blur(this);
        blur.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
        const QStringList args {inputFilename, QStringLiteral("("), QStringLiteral("-clone"), QStringLiteral("0"), QStringLiteral("-fill"), QStringLiteral("white"), QStringLiteral("-colorize"), QStringLiteral("100"), QStringLiteral("-fill"), QStringLiteral("black"), QStringLiteral("-draw"), QString(QStringLiteral("polygon %1,%2 %3,%4 %5,%6 %7,%8")).arg(QString::number(left), QString::number(top), QString::number(left + width), QString::number(top), QString::number(left + width), QString::number(top + height), QString::number(left), QString::number(top + height)), QStringLiteral("-alpha"), QStringLiteral("off"), QStringLiteral("-write"), QStringLiteral("mpr:mask"), QStringLiteral("+delete"), QStringLiteral(")"), QStringLiteral("-mask"), QStringLiteral("mpr:mask"), QStringLiteral("-blur"), QStringLiteral("0x5"), QStringLiteral("+mask"), outputFilename};
        blur.start(QStringLiteral("convert"), args);

        if (!blur.waitForStarted()) {
            qWarning() << "ImageOperations::blur: failed to start command" << FileOperations::commandLine(&blur);
            return ExternalCommandFailed;
        }
        if (!blur.waitForFinished(120000)) {
            qWarning() << "ImageOperations::blur: failed to finish command" << FileOperations::commandLine(&blur);
            return ExternalCommandFailed;
        }

        const ErrorCode result = blur.exitStatus() == QProcess::NormalExit && blur.exitCode() == 0 ? NoError : ExternalCommandFailed;
        if (result != NoError) {
            qWarning() << "ImageOperations::blur: process existed with error" << FileOperations::commandLine(&blur) << "(exit code" << blur.exitCode() << ")";
            const QString errorOutput = QString::fromUtf8(blur.readAllStandardError());
            qDebug() << errorOutput;
            return result;
        } else if (!QFile::exists(outputFilename)) {
            qWarning() << "ImageOperations::blur: no output file generated:" << outputFilename;
            return ExternalCommandFailed;
        } else /// result==NoError
            return NoError;
    } else {
        qWarning() << "ImageOperations::blur: Unsupported file type for this operation:" << inputFilename;
        return UnsupportedOperation;
    }
}

bool ImageOperations::canRotate(const QString &filename)
{
    const FileType ft = fileTypeForFileName(filename);
    return canRotate(ft);
}

bool ImageOperations::canRotate(const ImageOperations::FileType ft)
{
    return ft == IsJPEG
#ifndef LIBAVIF_CMDLINE
           || ft == IsAVIF
#endif // LIBAVIF_CMDLINE
           // FIXME || ft == IsJpegXLLossless || ft == IsJpegXLLossy
           || ft == IsTIFF || ft == IsPBM || ft == IsPGM || ft == IsPNG || ft == IsPPM || ft == IsWebPLossless || ft == IsWebPLossy;
}

ErrorCode ImageOperations::rotate(const QString &inputFilename, const QString &outputFilename, Rotation rotation) {
    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::rotate: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::rotate: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    int rotateDegree = 0;
    switch (rotation) {
    case Right90:
        rotateDegree = 90;
        break;
    case Left90:
        rotateDegree = 270;
        break;
    }

    if (isJPEG(inputFilename)) {
        QProcess jpegtran(this);
        jpegtran.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
        const QStringList args {QStringLiteral("-copy"), QStringLiteral("all"), QStringLiteral("-optimize"), QStringLiteral("-rotate"), QString::number(rotateDegree), QStringLiteral("-outfile"), outputFilename, inputFilename};
        jpegtran.start(QStringLiteral("jpegtran"), args);

        if (!jpegtran.waitForStarted()) {
            qWarning() << "ImageOperations::rotate: failed to start command" << FileOperations::commandLine(&jpegtran);
            return ExternalCommandFailed;
        }
        if (!jpegtran.waitForFinished(120000)) {
            qWarning() << "ImageOperations::rotate: failed to finish command" << FileOperations::commandLine(&jpegtran);
            return ExternalCommandFailed;
        }

        const ErrorCode result = jpegtran.exitStatus() == QProcess::NormalExit && jpegtran.exitCode() == 0 ? NoError : ExternalCommandFailed;
        if (result != NoError) {
            qWarning() << "ImageOperations::rotate: process existed with error" << FileOperations::commandLine(&jpegtran) << "(exit code" << jpegtran.exitCode() << ")";
            const QString errorOutput = QString::fromUtf8(jpegtran.readAllStandardError());
            qDebug() << errorOutput;
        } else if (!QFile::exists(outputFilename)) {
            qWarning() << "ImageOperations::rotate: no output file generated:" << outputFilename;
            return ExternalCommandFailed;
        } else if (d->exif->setOrientation(outputFilename, Exif::Normal) != NoError) {
            qWarning() << "ImageOperations::rotate: Failed to set orientation tags in Exif data:" << outputFilename;
            return ExternalCommandFailed;
        } else
            return NoError;
    }

    if (canRotate(inputFilename)) {
        QProcess convert(this);
        convert.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
        QStringList args {inputFilename, QStringLiteral("+repage"), QStringLiteral("-rotate"), QString::number(rotateDegree)};
        if (isWebPLossless(inputFilename)) {
            args << QStringLiteral("-define") << QStringLiteral("webp:lossless=true");
            args << QStringLiteral("-quality") << QStringLiteral("100"); //< for lossless WebP, high quality means 'try hard to achieve good compression'
        } else if (isWebPLossy(inputFilename)) {
            args << QStringLiteral("-define") << QStringLiteral("webp:lossless=false");
            args << QStringLiteral("-quality") << QStringLiteral("85"); //< accept some loss of quality
        }
        args << outputFilename;
        convert.start(QStringLiteral("convert"), args);

        if (!convert.waitForStarted()) {
            qWarning() << "ImageOperations::rotate: failed to start command" << FileOperations::commandLine(&convert);
            return ExternalCommandFailed;
        }
        if (!convert.waitForFinished(120000)) {
            qWarning() << "ImageOperations::rotate: failed to finish command" << FileOperations::commandLine(&convert);
            return ExternalCommandFailed;
        }

        const ErrorCode result = convert.exitStatus() == QProcess::NormalExit && convert.exitCode() == 0 ? NoError : ExternalCommandFailed;
        if (result != NoError) {
            qWarning() << "ImageOperations::rotate: process existed with error" << FileOperations::commandLine(&convert) << "(exit code" << convert.exitCode() << ")";
            const QString errorOutput = QString::fromUtf8(convert.readAllStandardError());
            qDebug() << errorOutput;
            return result;
        } else if (!QFile::exists(outputFilename)) {
            qWarning() << "ImageOperations::rotate: no output file generated:" << outputFilename;
            return ExternalCommandFailed;
        } else /// result==NoError
            return NoError;
    } else {
        qWarning() << "ImageOperations::rotate: Unsupported file type for this operation:" << inputFilename;
        return UnsupportedOperation;
    }
}

bool ImageOperations::canGrayscale(const QString &filename)
{
    const FileType ft = fileTypeForFileName(filename);
    return canGrayscale(ft);
}

bool ImageOperations::canGrayscale(const ImageOperations::FileType ft)
{
    return ft == IsJPEG
#ifndef LIBAVIF_CMDLINE
           || ft == IsAVIF
#endif // LIBAVIF_CMDLINE
           // FIXME || ft == IsJpegXLLossless || ft == IsJpegXLLossy
           || ft == IsTIFF || ft == IsPNG || ft == IsPPM || ft == IsWebPLossless || ft == IsWebPLossy;
}

ErrorCode ImageOperations::grayscaleFile(const QString &inputFilename, const QString &outputFilename) {
    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::grayscaleFile: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::grayscaleFile: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    const bool isIndeedJpeg = isJPEG(inputFilename);
    if (isIndeedJpeg) {
        const ErrorCode result = grayscaleJPEG(inputFilename, outputFilename);
        if (result == NoError)
            return NoError;
    }

    if (canGrayscale(inputFilename)) {
        QProcess convert(this);
        convert.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
        QStringList args {inputFilename, QStringLiteral("+repage"), QStringLiteral("-colorspace"), QStringLiteral("gray")};
        if (isWebPLossless(inputFilename)) {
            args << QStringLiteral("-define") << QStringLiteral("webp:lossless=true");
            args << QStringLiteral("-quality") << QStringLiteral("100"); //< for lossless WebP, high quality means 'try hard to achieve good compression'
        } else if (isWebPLossy(inputFilename)) {
            args << QStringLiteral("-define") << QStringLiteral("webp:lossless=false");
            args << QStringLiteral("-quality") << QStringLiteral("85"); //< accept some loss of quality
        }
        args << outputFilename;
        convert.start(QStringLiteral("convert"), args);

        if (!convert.waitForStarted()) {
            qWarning() << "ImageOperations::grayscaleFile: failed to start command" << FileOperations::commandLine(&convert);
            return ExternalCommandFailed;
        }
        if (!convert.waitForFinished(120000)) {
            qWarning() << "ImageOperations::grayscaleFile: failed to finish command" << FileOperations::commandLine(&convert);
            return ExternalCommandFailed;
        }

        const ErrorCode result = convert.exitStatus() == QProcess::NormalExit && convert.exitCode() == 0 ? NoError : ExternalCommandFailed;
        if (result != NoError) {
            qWarning() << "ImageOperations::grayscaleFile: process existed with error" << FileOperations::commandLine(&convert) << "(exit code" << convert.exitCode() << ")";
            const QString errorOutput = QString::fromUtf8(convert.readAllStandardError());
            qDebug() << errorOutput;
            return result;
        } else if (!QFile::exists(outputFilename)) {
            qWarning() << "ImageOperations::grayscaleFile: no output file generated:" << outputFilename;
            return ExternalCommandFailed;
        } else /// result==NoError
            return NoError;
    } else {
        qWarning() << "ImageOperations::grayscaleFile: Unsupported file type for this operation:" << inputFilename;
        return UnsupportedOperation;
    }
}

bool ImageOperations::canConvertImageFormat(const QString &filename)
{
    const FileType ft = fileTypeForFileName(filename);
    return canConvertImageFormat(ft);
}

bool ImageOperations::canConvertImageFormat(const ImageOperations::FileType ft)
{
    return ft == IsTIFF || ft == IsPNG || ft == IsJPEG || ft == IsJpegXLLossless || ft == IsJpegXLLossy || ft == IsWebPLossless || ft == IsWebPLossy || ft == IsAVIF || ft == IsPBM || ft == IsPGM || ft == IsPPM;
}

ErrorCode ImageOperations::convertImageFormat(const QString &inputFilename, const QString &outputFilename, const ImageOperations::FileType outputFileType) {
    if (QFile::exists(outputFilename))
        qWarning() << "ImageOperations::convertImageFormat: output file already exists, overwriting:" << outputFilename;

    if (!QFile::exists(inputFilename)) {
        qWarning() << "ImageOperations::convertImageFormat: input file does not exist:" << inputFilename;
        return FileDoesNotExist;
    }

    int width = 0, height = 0;
    FileType inputFileType = IsOther;
    Colors colors = ColorsUnknown;
    const ErrorCode identifyImageResult = identifyImage(inputFilename, width, height, inputFileType, colors);
    if (identifyImageResult != NoError)
        return identifyImageResult;

#ifdef LIBAVIF_CMDLINE
    const bool convertFromAVIF = inputFileType == IsAVIF && outputFileType != IsAVIF;
    const bool convertToAVIF = inputFileType != IsAVIF && outputFileType == IsAVIF;
    const QString internalInputFilename = convertFromAVIF || (convertToAVIF && inputFileType != IsPNG) ? QString(QStringLiteral("%1/qdisplay5-convert-image-%2.png")).arg(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory), QString::number(QRandomGenerator::global()->generate(), 16)) : inputFilename;
    if (convertFromAVIF) {
        QProcess avifdec(this);
        avifdec.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
        const QStringList args {QStringLiteral("--png-compress"), QStringLiteral("0"), inputFilename, internalInputFilename};
        avifdec.start(QStringLiteral("avifdec"), args);

        if (!avifdec.waitForStarted()) {
            qWarning() << "ImageOperations::convertImageFormat: failed to start command" << FileOperations::commandLine(&avifdec);
            return ExternalCommandFailed;
        }
        if (!avifdec.waitForFinished(120000)) {
            qWarning() << "ImageOperations::convertImageFormat: failed to finish command" << FileOperations::commandLine(&avifdec);
            return ExternalCommandFailed;
        }

        ErrorCode result = avifdec.exitStatus() == QProcess::NormalExit && avifdec.exitCode() == 0 ? NoError : ExternalCommandFailed;
        if (result != NoError) {
            qWarning() << "ImageOperations::convertImageFormat: process existed with error" << FileOperations::commandLine(&avifdec) << "(exit code" << avifdec.exitCode() << ")";
            const QString errorOutput = QString::fromUtf8(avifdec.readAllStandardError());
            qDebug() << errorOutput;
            return result;
        } else if (!QFile::exists(internalInputFilename)) {
            qWarning() << "ImageOperations::convertImageFormat: no output file generated:" << outputFilename;
            return ExternalCommandFailed;
        }
    } else if (convertToAVIF && inputFileType != IsPNG) {
        QProcess convert(this);
        convert.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
        const QStringList args {inputFilename, internalInputFilename};
        convert.start(QStringLiteral("convert"), args);

        if (!convert.waitForStarted()) {
            qWarning() << "ImageOperations::convertImageFormat: failed to start command" << FileOperations::commandLine(&convert);
            return ExternalCommandFailed;
        }
        if (!convert.waitForFinished(120000)) {
            qWarning() << "ImageOperations::convertImageFormat: failed to finish command" << FileOperations::commandLine(&convert);
            return ExternalCommandFailed;
        }

        ErrorCode result = convert.exitStatus() == QProcess::NormalExit && convert.exitCode() == 0 ? NoError : ExternalCommandFailed;
        if (result != NoError) {
            qWarning() << "ImageOperations::convertImageFormat: process existed with error" << FileOperations::commandLine(&convert) << "(exit code" << convert.exitCode() << ")";
            const QString errorOutput = QString::fromUtf8(convert.readAllStandardError());
            qDebug() << errorOutput;
            return result;
        } else if (!QFile::exists(internalInputFilename)) {
            qWarning() << "ImageOperations::convertImageFormat: no output file generated:" << outputFilename;
            return ExternalCommandFailed;
        }
    }
#endif // LIBAVIF_CMDLINE

#ifdef LIBAVIF_CMDLINE
    if (convertToAVIF) {
        QProcess avifenc(this);
        avifenc.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
        const QString q = QString::number(((100 - 60) * 63 + 50) / 100); //< 60% is standard quality
        const QStringList args {internalInputFilename, QStringLiteral("--min"), q, QStringLiteral("--max"), q, outputFilename};
        avifenc.start(QStringLiteral("avifenc"), args);

        if (!avifenc.waitForStarted()) {
            qWarning() << "ImageOperations::convertImageFormat: failed to start command" << FileOperations::commandLine(&avifenc);
            return ExternalCommandFailed;
        }
        if (!avifenc.waitForFinished(240000)) {
            qWarning() << "ImageOperations::convertImageFormat: failed to finish command" << FileOperations::commandLine(&avifenc);
            return ExternalCommandFailed;
        }

        if (internalInputFilename != inputFilename)
            QFile::remove(internalInputFilename);

        ErrorCode result = avifenc.exitStatus() == QProcess::NormalExit && avifenc.exitCode() == 0 ? NoError : ExternalCommandFailed;
        if (result != NoError) {
            qWarning() << "ImageOperations::convertImageFormat: process existed with error" << FileOperations::commandLine(&avifenc) << "(exit code" << avifenc.exitCode() << ")";
            const QString errorOutput = QString::fromUtf8(avifenc.readAllStandardError());
            qDebug() << errorOutput;
            return result;
        } else if (!QFile::exists(outputFilename)) {
            qWarning() << "ImageOperations::convertImageFormat: no output file generated:" << outputFilename;
            return ExternalCommandFailed;
        } else // result==NoError
            return NoError;
    } else {
#endif // LIBAVIF_CMDLINE
        QProcess convert(this);
        convert.setWorkingDirectory(QStandardPaths::locate(QStandardPaths::TempLocation, QString(), QStandardPaths::LocateDirectory));
#ifdef LIBAVIF_CMDLINE
        QStringList args {internalInputFilename};
#else // LIBAVIF_CMDLINE
        QStringList args {inputFilename};
        const bool convertToAVIF = inputFileType != IsAVIF && outputFileType == IsAVIF;
#endif // LIBAVIF_CMDLINE
        const bool convertToJPEG = inputFileType != IsJPEG && outputFileType == IsJPEG;
        const bool convertToWebP = inputFileType != IsWebPLossless && inputFileType != IsWebPLossy && (outputFileType == IsWebPLossless || outputFileType == IsWebPLossy);
        if (convertToWebP) {
            const bool losslessWebP = outputFileType == IsWebPLossless;
            args << QStringLiteral("-define") << QStringLiteral("webp:lossless=") + (losslessWebP ? QStringLiteral("true") : QStringLiteral("false"));
            if (losslessWebP)
                args << QStringLiteral("-quality") << QStringLiteral("100"); //< for lossless WebP, high quality means 'try hard to achieve good compression'
            else
                args << QStringLiteral("-quality") << QStringLiteral("85"); //< accept some loss of quality
        }
#ifndef LIBAVIF_CMDLINE
        else if (convertToAVIF)
            args << QStringLiteral("-quality") << QStringLiteral("40");
#endif // LIBAVIF_CMDLINE
        else if (convertToJPEG)
            args << QStringLiteral("-quality") << QStringLiteral("75");
        args << outputFilename;
        convert.start(QStringLiteral("convert"), args);

        if (!convert.waitForStarted()) {
            qWarning() << "ImageOperations::convertImageFormat: failed to start command" << FileOperations::commandLine(&convert);
            return ExternalCommandFailed;
        }
        if (!convert.waitForFinished(120000)) {
            qWarning() << "ImageOperations::convertImageFormat: failed to finish command" << FileOperations::commandLine(&convert);
            return ExternalCommandFailed;
        }

#ifdef LIBAVIF_CMDLINE
        if (internalInputFilename != inputFilename)
            QFile::remove(internalInputFilename);
#endif // LIBAVIF_CMDLINE

        ErrorCode result = convert.exitStatus() == QProcess::NormalExit && convert.exitCode() == 0 ? NoError : ExternalCommandFailed;
        if (result != NoError) {
            qWarning() << "ImageOperations::convertImageFormat: process existed with error" << FileOperations::commandLine(&convert) << "(exit code" << convert.exitCode() << ")";
            const QString errorOutput = QString::fromUtf8(convert.readAllStandardError());
            qDebug() << errorOutput;
            return result;
        } else if (!QFile::exists(outputFilename)) {
            qWarning() << "ImageOperations::convertImageFormat: no output file generated:" << outputFilename;
            return ExternalCommandFailed;
        } else // result==NoError
            return NoError;
#ifdef LIBAVIF_CMDLINE
    }
#endif // LIBAVIF_CMDLINE
}
