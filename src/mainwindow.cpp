/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2022 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include "mainwindow.h"

#include <QApplication>
#include <QLabel>
#include <QLayout>
#include <QDateTime>
#include <QFile>
#include <QFileInfo>
#include <QDebug>
#include <QFontMetrics>
#include <QPointer>
#include <QMovie>
#include <QHash>
#include <QSharedPointer>
#include <QScreen>
#include <QFileDialog>
#include <QInputDialog>
#include <QImage>
#include <QImageReader>
#include <QCryptographicHash>
#include <QProcess>
#include <QRegularExpression>
#include <QRandomGenerator>

#include "imageoperations.h"
#include "imageoperationsui.h"
#include "fileoperations.h"
#include "undostack.h"
#include "exif.h"

class MainWindow::Private {
private:
    MainWindow *p;
    QWidget *statusBar;
    QString currentFilename;

    QStringList fileList;
    int fileListPos;

    QString formatSize(qint64 size)
    {
        if (size < 2048)
            return QString("%1%2B").arg(size).arg(QChar(0x2009));
        else if (size < (48 * 2048))
            return QString("%1%2K").arg(static_cast<double>(size) / 1024.0, 0, 'f', 1).arg(QChar(0x2009));
        else if (size < (2048 * 2048))
            return QString("%1%2K").arg((size + 512) / 1024).arg(QChar(0x2009));
        else if (size < (48 * 2048 * 1024))
            return QString("%1%2M").arg(static_cast<double>(size) / 1024.0 / 1024.0, 0, 'f', 1).arg(QChar(0x2009));
        else
            return QString("%1%2M").arg((size + 524288) / 1048576).arg(QChar(0x2009));
    }

    void invalidateStatusBar(const QString &filename = QString()) {
        QFileInfo fi(filename);
        statusBarFilename->setText(fi.exists() ? fi.fileName() : filename);
        statusBarFilename->setToolTip(fi.exists() ? fi.absoluteFilePath() : filename);
        statusBarFileSize->setText(QString());
        statusBarFileDate->setText(QString());
        statusBarImageSize->setText(QString());
    }

    void setImage(const QString &filename) {
        currentFilename = filename;
        if (currentFilename.isEmpty()) {
            invalidateStatusBar();
            imageWidget->unsetImage(false);
            return;
        }

        QFileInfo fi(currentFilename);
        if (!fi.exists()) {
            invalidateStatusBar();
            imageWidget->unsetImage(false);
            return;
        }

        if (ImageOperations::isGIF(currentFilename)) {
            QSharedPointer<QMovie> movie = QSharedPointer<QMovie>(new QMovie(currentFilename));
            if (!movie->isValid()) {
                invalidateStatusBar(currentFilename);
                imageWidget->unsetImage(true);
                return;
            }

            imageWidget->setMovie(movie);
            statusBarFilename->setText(fi.fileName());
            statusBarFilename->setToolTip(fi.absoluteFilePath());
            statusBarFileSize->setText(formatSize(fi.size()));
            statusBarFileDate->setText(fi.lastModified().toString(QStringLiteral("yyyy-MM-dd HH:mm")));
            const QSize firstMovieImageSize = movie->currentImage().size();
            statusBarImageSize->setText(QString(QStringLiteral("%1%2%3%4%5")).arg(firstMovieImageSize.width()).arg(QChar(0x2009)).arg(timesCharacter).arg(QChar(0x2009)).arg(firstMovieImageSize.height()));
        } else if (ImageOperations::isPNG(currentFilename) || ImageOperations::isTIFF(currentFilename) || ImageOperations::isJPEG(currentFilename) || ImageOperations::isJpegXLLossy(currentFilename) || ImageOperations::isJpegXLLossless(currentFilename) || ImageOperations::isAVIF(currentFilename) || ImageOperations::isPBM(currentFilename) || ImageOperations::isPGM(currentFilename) || ImageOperations::isPPM(currentFilename) || ImageOperations::isWebPLossless(currentFilename) || ImageOperations::isWebPLossy(currentFilename)) {
            QPixmap pixmap(currentFilename);
            if (pixmap.isNull()) {
                invalidateStatusBar(currentFilename);
                imageWidget->unsetImage(true);
                return;
            }

            statusBarFilename->setText(fi.fileName());
            statusBarFilename->setToolTip(fi.absoluteFilePath());
            statusBarFileSize->setText(formatSize(fi.size()));
            statusBarFileDate->setText(fi.lastModified().toString(QStringLiteral("yyyy-MM-dd HH:mm")));
            statusBarImageSize->setText(QString(QStringLiteral("%1%2%3%4%5")).arg(pixmap.width()).arg(QChar(0x2009)).arg(timesCharacter).arg(QChar(0x2009)).arg(pixmap.height()));
            imageWidget->setImage(pixmap);
        } else if (ImageOperations::isOTF(currentFilename) || ImageOperations::isTTF(currentFilename) || ImageOperations::isPFB(currentFilename)) {
            const QByteArray hexHash = QCryptographicHash::hash(currentFilename.toUtf8().constData(), QCryptographicHash::Md4).toHex();
            const QString cacheFile = QString(QStringLiteral("/tmp/.qdisplay-fontimage-%1.png")).arg(QString::fromLatin1(hexHash));
            if (!QFile::exists(cacheFile) || QFileInfo(currentFilename).lastModified() > QFileInfo(cacheFile).lastModified()) {
                static const QString text1 = QStringLiteral("ABCQJYKP");
                static const QString text2 = QStringLiteral("abcqjykp");
                static const QString text3 = QStringLiteral("1234567 ");
                static const QString text4 = QString(QChar(0x00e4)).append(QChar(0x00e5)).append(QChar(0x00f6)).append(QChar(0x00fc)).append(QChar(0x00c5)).append(QChar(0x00c6)).append(QChar(0x00d6));
                static const QString text5 = QString(QChar(0x20ac)).append(QChar(0x0161)).append(QChar(0x0192)).append(QChar(0x01fa)).append(QChar(0x0245)).append(QChar(0x0237)).append(QChar(0x02da)).append(QChar(0x03c0));
                QProcess imageMagickConvert(p);
                const QStringList args {QStringLiteral("-size"), QStringLiteral("3072x896"), QStringLiteral("-font"), currentFilename, QStringLiteral("xc:white"), QStringLiteral("-pointsize"), QStringLiteral("128"), QStringLiteral("-tile"), QStringLiteral("xc:black"), QStringLiteral("-annotate"), QStringLiteral("+16+160"), text1, QStringLiteral("-annotate"), QStringLiteral("+16+320"), text2, QStringLiteral("-annotate"), QStringLiteral("+16+480"), text3, QStringLiteral("-annotate"), QStringLiteral("+16+640"), text4, QStringLiteral("-annotate"), QStringLiteral("+16+800"), text5, QStringLiteral("-trim"), QStringLiteral("+repage"), cacheFile};
                imageMagickConvert.start(QStringLiteral("convert"), args);

                if (!imageMagickConvert.waitForStarted()) {
                    qWarning() << "setImage: failed to start command" << FileOperations::commandLine(&imageMagickConvert);
                    QFile(cacheFile).remove();
                } else if (!imageMagickConvert.waitForFinished(120000)) {
                    qWarning() << "setImage: failed to finish command" << FileOperations::commandLine(&imageMagickConvert);
                    QFile(cacheFile).remove();
                } else if (imageMagickConvert.exitStatus() != QProcess::NormalExit || imageMagickConvert.exitCode() != 0) {
                    qWarning() << "setImage: process existed with error" << FileOperations::commandLine(&imageMagickConvert) << "(exit code" << imageMagickConvert.exitCode() << ")";
                    qDebug() << imageMagickConvert.readAllStandardOutput();
                    qDebug() << imageMagickConvert.readAllStandardError();
                    QFile(cacheFile).remove();
                }
            }

            const QPixmap pixmap = QFile::exists(cacheFile) ? QPixmap(cacheFile) : QPixmap();
            if (pixmap.isNull()) {
                invalidateStatusBar(currentFilename);
                imageWidget->unsetImage(true);
                return;
            }

            statusBarFilename->setText(fi.fileName());
            statusBarFilename->setToolTip(fi.absoluteFilePath());
            statusBarFileSize->setText(formatSize(fi.size()));
            statusBarFileDate->setText(fi.lastModified().toString(QStringLiteral("yyyy-MM-dd HH:mm")));
            statusBarImageSize->setText(QString(QStringLiteral("%1%2%3%4%5")).arg(pixmap.width()).arg(QChar(0x2009)).arg(timesCharacter).arg(QChar(0x2009)).arg(pixmap.height()));
            imageWidget->setImage(pixmap);
        } else if (ImageOperations::isSVG(currentFilename)) {
            const QByteArray hexHash = QCryptographicHash::hash(currentFilename.toUtf8().constData(), QCryptographicHash::Md4).toHex();
            const QString cacheFile = QString(QStringLiteral("/tmp/.qdisplay-svgimage-%1.png")).arg(QString::fromLatin1(hexHash));
            if (!QFile::exists(cacheFile) || QFileInfo(currentFilename).lastModified() > QFileInfo(cacheFile).lastModified()) {
                QProcess rsvgConvert(p);
                const QStringList args {QStringLiteral("-f"), QStringLiteral("png"), QStringLiteral("-o"), cacheFile, currentFilename};
                rsvgConvert.start(QStringLiteral("rsvg-convert"), args);

                if (!rsvgConvert.waitForStarted()) {
                    qWarning() << "setImage: failed to start command" << FileOperations::commandLine(&rsvgConvert);
                    QFile(cacheFile).remove();
                } else if (!rsvgConvert.waitForFinished(120000)) {
                    qWarning() << "setImage: failed to finish command" << FileOperations::commandLine(&rsvgConvert);
                    QFile(cacheFile).remove();
                } else if (rsvgConvert.exitStatus() != QProcess::NormalExit || rsvgConvert.exitCode() != 0) {
                    qWarning() << "setImage: process existed with error" << FileOperations::commandLine(&rsvgConvert) << "(exit code" << rsvgConvert.exitCode() << ")";
                    qDebug() << rsvgConvert.readAllStandardOutput();
                    qDebug() << rsvgConvert.readAllStandardError();
                    QFile(cacheFile).remove();
                }
            }

            const QPixmap pixmap = QFile::exists(cacheFile) ? QPixmap(cacheFile) : QPixmap();
            if (pixmap.isNull()) {
                invalidateStatusBar(currentFilename);
                imageWidget->unsetImage(true);
                return;
            }

            statusBarFilename->setText(fi.fileName());
            statusBarFilename->setToolTip(fi.absoluteFilePath());
            statusBarFileSize->setText(formatSize(fi.size()));
            statusBarFileDate->setText(fi.lastModified().toString(QStringLiteral("yyyy-MM-dd HH:mm")));
            statusBarImageSize->setText(QString(QStringLiteral("%1%2%3%4%5")).arg(pixmap.width()).arg(QChar(0x2009)).arg(timesCharacter).arg(QChar(0x2009)).arg(pixmap.height()));
            imageWidget->setImage(pixmap);
        } else if (ImageOperations::isPDF(currentFilename) || ImageOperations::isEPS(currentFilename)) {
            const QByteArray hexHash = QCryptographicHash::hash(currentFilename.toUtf8().constData(), QCryptographicHash::Md4).toHex();
            const QString cacheFile = QString(QStringLiteral("/tmp/.qdisplay-pdfepsimage-%1.png")).arg(QString::fromLatin1(hexHash));
            if (!QFile::exists(cacheFile) || QFileInfo(currentFilename).lastModified() > QFileInfo(cacheFile).lastModified()) {
                QProcess ghostScript(p);

                const QStringList argsBB {QStringLiteral("-dNOPAUSE"), QStringLiteral("-dBATCH"), QStringLiteral("-dSAFER"), QStringLiteral("-q"), QStringLiteral("-sDEVICE=bbox"), currentFilename};
                ghostScript.start(QStringLiteral("gs"), argsBB);

                int resolution = 100;
                if (!ghostScript.waitForStarted()) {
                    qWarning() << "setImage: failed to start command" << FileOperations::commandLine(&ghostScript);
                    QFile(cacheFile).remove();
                } else if (!ghostScript.waitForFinished(120000)) {
                    qWarning() << "setImage: failed to finish command" << FileOperations::commandLine(&ghostScript);
                    QFile(cacheFile).remove();
                } else if (ghostScript.exitStatus() != QProcess::NormalExit || ghostScript.exitCode() != 0) {
                    qWarning() << "setImage: process existed with error" << FileOperations::commandLine(&ghostScript) << "(exit code" << ghostScript.exitCode() << ")";
                    qDebug() << ghostScript.readAllStandardOutput();
                    qDebug() << ghostScript.readAllStandardError();
                    QFile(cacheFile).remove();
                } else {
                    const QString stdErrText = QString::fromLatin1(ghostScript.readAllStandardError());
                    static const QRegularExpression bboxRegExp(QStringLiteral("%%BoundingBox:\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)\\b"));
                    const QRegularExpressionMatch bboxRegExpMatch = bboxRegExp.match(stdErrText);
                    if (bboxRegExpMatch.hasMatch()) {
                        bool ok = false;
                        const int a = bboxRegExpMatch.captured(1).toInt(&ok);
                        const int b = bboxRegExpMatch.captured(2).toInt();
                        const int c = bboxRegExpMatch.captured(3).toInt();
                        const int d = bboxRegExpMatch.captured(4).toInt();
                        const int deltaX = c - a, deltaY = d - b;
                        if (!ok || deltaX <= 0 || deltaY <= 0)
                            resolution = 100;
                        else {
                            resolution = deltaY * 3 / 2 > deltaX ? 72 * 768 / deltaY : 1024 * 72 / deltaX;
                            if (resolution < 16) resolution = 16;
                        }
                    }
                }

                const QStringList args {QStringLiteral("-dNOPAUSE"), QStringLiteral("-dBATCH"), QStringLiteral("-dSAFER"), QStringLiteral("-q"), QStringLiteral("-sDEVICE=png16m"), QStringLiteral("-dTextAlphaBits=4"), QStringLiteral("-dGraphicsAlphaBits=4"), QString(QStringLiteral("-r%1")).arg(resolution), QStringLiteral("-sOutputFile=") + cacheFile, currentFilename};
                ghostScript.start(QStringLiteral("gs"), args);

                if (!ghostScript.waitForStarted()) {
                    qWarning() << "setImage: failed to start command" << FileOperations::commandLine(&ghostScript);
                    QFile(cacheFile).remove();
                } else if (!ghostScript.waitForFinished(120000)) {
                    qWarning() << "setImage: failed to finish command" << FileOperations::commandLine(&ghostScript);
                    QFile(cacheFile).remove();
                } else if (ghostScript.exitStatus() != QProcess::NormalExit || ghostScript.exitCode() != 0) {
                    qWarning() << "setImage: process existed with error" << FileOperations::commandLine(&ghostScript) << "(exit code" << ghostScript.exitCode() << ")";
                    qDebug() << ghostScript.readAllStandardOutput();
                    qDebug() << ghostScript.readAllStandardError();
                    QFile(cacheFile).remove();
                }
            }

            const QPixmap pixmap = QFile::exists(cacheFile) ? QPixmap(cacheFile) : QPixmap();
            if (pixmap.isNull()) {
                invalidateStatusBar(currentFilename);
                imageWidget->unsetImage(true);
                return;
            }

            statusBarFilename->setText(fi.fileName());
            statusBarFilename->setToolTip(fi.absoluteFilePath());
            statusBarFileSize->setText(formatSize(fi.size()));
            statusBarFileDate->setText(fi.lastModified().toString(QStringLiteral("yyyy-MM-dd HH:mm")));
            statusBarImageSize->setText(QString(QStringLiteral("%1%2%3%4%5")).arg(pixmap.width()).arg(QChar(0x2009)).arg(timesCharacter).arg(QChar(0x2009)).arg(pixmap.height()));
            imageWidget->setImage(pixmap);
        } else {
            /// Invalid or unsupported file format
            invalidateStatusBar(currentFilename);
            imageWidget->unsetImage(true);
        }
    }

public:
    enum StatusBarColors {NormalColors, FullscreenColors};

    QLabel *statusBarFilename;
    QLabel *statusBarFileSize;
    QLabel *statusBarFileDate;
    QLabel *statusBarImageSize;
    QLabel *statusBarPosition;
    ImageOperations *imageOperations;
    ImageWidget *imageWidget;
    UndoStack *undoStack;
    Exif *exif;
    ImageOperationsUI *imageOperationsUI;

    QChar timesCharacter;

    Private(const QStringList &files, MainWindow *parent)
        : p(parent), fileList(files), fileListPos(-1) {
        imageOperations = new ImageOperations(parent);
        imageOperationsUI = new ImageOperationsUI(parent);
        undoStack = new UndoStack(parent);
        exif = new Exif(parent);

        const QFontMetrics fm = parent->fontMetrics();
        if (fm.inFont(QChar(0x00d7)))
            timesCharacter = QChar(0x00d7);
        else if (fm.inFont(QChar(0x2715)))
            timesCharacter = QChar(0x2715);
        else if (fm.inFont(QChar(0x2a09)))
            timesCharacter = QChar(0x2a09);
        else
            timesCharacter = QLatin1Char('x');
    }

    ~Private() {
        delete exif;
        delete imageOperations;
        delete imageOperationsUI;
    }

    void setupGUI() {
        QWidget *container = new QWidget(p);
        p->setCentralWidget(container);

        QBoxLayout *layout = new QVBoxLayout(container);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(0);

        imageWidget = new ImageWidget(container);
        layout->addWidget(imageWidget, 1000);

        statusBar = new QWidget(container);
        layout->addWidget(statusBar, 0);

        QBoxLayout *statusBarLayout = new QHBoxLayout(statusBar);
        statusBarLayout->setContentsMargins(1, 1, 1, 1);
        const int defaultSpacing = container->fontMetrics().averageCharWidth() * 2;
        statusBarLayout->setSpacing(defaultSpacing);
        statusBarLayout->addSpacing(1);

        statusBarFilename = new QLabel(statusBar);
        statusBarLayout->addWidget(statusBarFilename, 10);
        statusBarFilename->setText(QStringLiteral("Ready."));
        statusBarFilename->setMinimumWidth(statusBarFilename->fontMetrics().averageCharWidth() * 40);

        statusBarLayout->addSpacing(defaultSpacing);

        statusBarFileSize = new QLabel(statusBar);
        statusBarLayout->addWidget(statusBarFileSize, 0);
        statusBarFileSize->setMinimumWidth(statusBarFileSize->fontMetrics().averageCharWidth() * 10);
        statusBarFileSize->setAlignment(Qt::AlignRight);

        statusBarLayout->addSpacing(defaultSpacing);

        statusBarFileDate = new QLabel(statusBar);
        statusBarLayout->addWidget(statusBarFileDate, 0);
        statusBarFileDate->setMinimumWidth(statusBarFileDate->fontMetrics().averageCharWidth() * 18);
        statusBarFileDate->setAlignment(Qt::AlignRight);

        statusBarLayout->addSpacing(defaultSpacing);

        statusBarImageSize = new QLabel(statusBar);
        statusBarLayout->addWidget(statusBarImageSize, 0);
        statusBarImageSize->setMinimumWidth(statusBarImageSize->fontMetrics().averageCharWidth() * 20);
        statusBarImageSize->setAlignment(Qt::AlignRight);

        statusBarLayout->addSpacing(defaultSpacing);

        statusBarPosition = new QLabel(statusBar);
        statusBarLayout->addWidget(statusBarPosition, 0);
        statusBarPosition->setMinimumWidth(statusBarPosition->fontMetrics().averageCharWidth() * 10);
        statusBarPosition->setAlignment(Qt::AlignRight);

        statusBarLayout->addSpacing(1);

        container->setMinimumSize(200, 200);
    }

    int posititon() const {
        return fileListPos;
    }

    void setPosition(int pos) {
        if (fileList.isEmpty()) {
            fileListPos = -1;
            setImage(QString());
        } else if (pos >= 0 && pos < fileList.count()) {
            fileListPos = pos;
            setImage(fileList[fileListPos]);
        } else if (pos < 0) {
            fileListPos = 0;
            setImage(fileList[fileListPos]);
        } else if (pos >= fileList.count()) {
            fileListPos = fileList.count() - 1;
            setImage(fileList[fileListPos]);
        }

        if (fileListPos >= 0) {
            statusBarPosition->setText(QString(QStringLiteral("%1/%2")).arg(fileListPos + 1).arg(fileList.count()));
            const QFileInfo fi(fileList[fileListPos]);
            p->setWindowTitle(QString(QStringLiteral("%1 [%2] - QDisplay5")).arg(fi.fileName(), fi.path()));
        } else {
            statusBarPosition->setText(QString());
            p->setWindowTitle(QStringLiteral("QDisplay5"));
        }
    }

    QString imageFilename() const {
        return currentFilename;
    }

    bool replaceCurrentImageFilename(const QString &newCurrentFilename) {
        if (fileListPos >= 0 && fileListPos < fileList.length()) {
            currentFilename = newCurrentFilename;
            fileList[fileListPos] = newCurrentFilename;
            setPosition(fileListPos);
            return true;
        }
        return false;
    }

    int removePosition(int pos) {
        if (pos >= 0 && pos < fileList.count()) {
            fileList.removeAt(pos);
            if (pos == posititon())
                setPosition(pos); ///< affected current image
        }
        return posititon();
    }

    void addPosition(int pos, const QString &filename) {
        if (pos >= 0 && pos <= fileList.count()) {
            fileList.insert(pos, filename);
        }
    }

    void randomizeFileOrder() {
        if (fileList.count() < 3) return;

        for (int i = 0; i < fileList.count(); ++i) {
            if (i == fileListPos) continue;

            int newI = QRandomGenerator::global()->bounded(fileList.count() - 1);
            if (newI >= fileListPos) ++newI;

            const QString swap = fileList[i];
            fileList[i] = fileList[newI];
            fileList[newI] = swap;
        }
    }

    void setStatusBarColors(StatusBarColors statusBarColors) {
        switch (statusBarColors) {
        case NormalColors:
            statusBar->setStyleSheet(QString());
            break;
        case FullscreenColors:
            statusBar->setStyleSheet(QStringLiteral("QWidget { background: black; color: gray; }"));
            break;
        }
    }

    enum DialogOperation {DialogMove, DialogCopy};

    /**
     * A customized version of QFileDialog's static getSaveFileName function.
     * Specialized for "move to" and "copy to" operations.
     */
    QString dialogGetFilename(DialogOperation operation, const QString &target, const QString &filter) const {
        QString result;
        const QString caption = operation == DialogMove ? QStringLiteral("Move Image To") : (operation == DialogCopy ? QStringLiteral("Copy Image To") : QStringLiteral("Save Image As"));
        const QString buttonLabel = operation == DialogMove ? QStringLiteral("&Move") : (operation == DialogCopy ? QStringLiteral("&Copy") : QStringLiteral("&Save"));
        QPointer<QFileDialog> dlg = new QFileDialog(p, caption, target, filter);
        dlg->setAcceptMode(QFileDialog::AcceptSave);
        dlg->setFileMode(QFileDialog::AnyFile);
        dlg->setLabelText(QFileDialog::Accept, buttonLabel);
        if (dlg->exec() == QDialog::Accepted && !dlg->selectedFiles().isEmpty())
            result = dlg->selectedFiles().first();
        delete dlg;
        return result;
    }
};

MainWindow::MainWindow(const QStringList &files, QWidget *parent)
    : QMainWindow(parent), d(new Private(files, this))
{
    setWindowTitle(QStringLiteral("QDisplay5"));
    const QRect maxSize = QApplication::primaryScreen()->availableGeometry();
    resize(maxSize.width() * 95 / 100 - 8, maxSize.height() * 95 / 100 - 8);

    d->setupGUI();
    connect(d->imageWidget, &ImageWidget::commandTriggered, this, &MainWindow::executeCommand);
    connect(d->imageWidget, &ImageWidget::zoomPercentChanged, this, &MainWindow::zoomPercentChanged);
    connect(d->undoStack, &UndoStack::availableUndoStepsChanged, d->imageWidget, &ImageWidget::availableUndoStepsChanged);

    d->setPosition(0);
}

MainWindow::~MainWindow()
{
    delete d;
}

void MainWindow::executeCommand(int cmd) {
    static QString previousSaveMoveDirectory;

    switch (cmd) {
    case ImageWidget::commandQuit:
        QCoreApplication::quit();
        break;
    case ImageWidget::commandFullscreen: {
        const bool isFullscreen = d->imageWidget->toggleFullscreenMode();
        if (isFullscreen) {
            d->setStatusBarColors(MainWindow::Private::FullscreenColors);
            showFullScreen();
        } else {
            d->setStatusBarColors(MainWindow::Private::NormalColors);
            showNormal();
        }
    }
    break;
    case ImageWidget::commandFileCopyTo: {
        const QString oldFilename = d->imageFilename();
        const QFileInfo oldFileInfo = QFileInfo(oldFilename);
        const QString target = previousSaveMoveDirectory.isEmpty() ? oldFilename : previousSaveMoveDirectory + QDir::separator() + oldFileInfo.fileName();
        const QString newFilename = d->dialogGetFilename(MainWindow::Private::DialogCopy, target, FileOperations::dialogFilterForFilename(oldFilename));
        const QFileInfo newFileInfo = QFileInfo(newFilename);
        if (newFileInfo == oldFileInfo) {
            qInfo() << "Cannot copy file to itself:" << oldFilename;
            d->imageWidget->showMessage(QString(QStringLiteral("Cannot copy file '%1' to itself")).arg(newFilename), 5, Qt::yellow);
        } else if (!newFilename.isEmpty()) {
            const ErrorCode result = FileOperations::copyFile(oldFilename, newFilename);
            switch (result) {
            case UserCancelled:
                qInfo() << "User cancelled copying" << oldFilename << "to" << newFilename;
                break;
            case NoError:
                previousSaveMoveDirectory = QFileInfo(newFilename).path();
                break;
            default: ///< any other error code
                qWarning() << "Copying file failed:" << oldFilename << "to" << newFilename;
                d->imageWidget->showMessage(QString(QStringLiteral("Copying file '%1' to '%2' failed")).arg(oldFileInfo.fileName(), newFilename), 5, Qt::red);
            }
        }
    }
    break;
    case ImageWidget::commandFileMoveTo: {
        const QString oldFilename = d->imageFilename();
        const QFileInfo oldFileInfo = QFileInfo(oldFilename);
        const QString target = previousSaveMoveDirectory.isEmpty() ? oldFilename : previousSaveMoveDirectory + QDir::separator() + oldFileInfo.fileName();
        const QString newFilename = d->dialogGetFilename(MainWindow::Private::DialogMove, target, FileOperations::dialogFilterForFilename(oldFilename));
        const QFileInfo newFileInfo = QFileInfo(newFilename);
        if (newFileInfo == oldFileInfo) {
            qInfo() << "Cannot move file to itself:" << oldFilename;
            d->imageWidget->showMessage(QString(QStringLiteral("Cannot move file '%1' to itself")).arg(newFilename), 5, Qt::yellow);
        } else if (!newFilename.isEmpty()) {
            if (d->undoStack->addBackupToStack(oldFilename, d->posititon()) == NoError) {
                const ErrorCode result = FileOperations::moveFile(oldFilename, newFilename);
                switch (result) {
                case UserCancelled:
                    qInfo() << "User cancelled moving" << oldFilename << "to" << newFilename;
                    break;
                case NoError:
                    d->removePosition(d->posititon());
                    previousSaveMoveDirectory = QFileInfo(newFilename).path();
                    break;
                default: ///< any other error code
                    qWarning() << "Moving file failed:" << oldFilename << "to" << newFilename;
                    d->imageWidget->showMessage(QString(QStringLiteral("Moving file '%1' to '%2' failed")).arg(oldFileInfo.fileName(), newFilename), 5, Qt::red);
                    qDebug() << "Trying to restore original";
                    executeCommand(ImageWidget::commandUndo);
                }
            }
        }
    }
    break;
    case ImageWidget::commandConvertImageType:
    {
        const QString currentFilename = d->imageFilename();
        if (ImageOperations::canConvertImageFormat(currentFilename)) {
            bool success = false;
            QHash<ImageOperations::FileType, QString> items;
            static const QVector<ImageOperations::FileType> baseTargetFileTypes {ImageOperations::IsPNG, ImageOperations::IsJPEG, ImageOperations::IsWebPLossless, ImageOperations::IsWebPLossy, ImageOperations::IsAVIF, ImageOperations::IsPBM, ImageOperations::IsPPM, ImageOperations::IsPGM};
            QVector<ImageOperations::FileType> targetFileTypes {baseTargetFileTypes};
            if (ImageOperations::canJpegXL())
                targetFileTypes.append({ImageOperations::IsJpegXLLossless, ImageOperations::IsJpegXLLossy});
            const ImageOperations::FileType inputFileType = ImageOperations::fileTypeForFileName(currentFilename);
            for (const ImageOperations::FileType targetFileType : targetFileTypes) {
                if (inputFileType != targetFileType)
                    items[targetFileType] = ImageOperations::fileTypeToLongDescription(targetFileType);
            }
            const QString userChoice = QInputDialog::getItem(this, QStringLiteral("Select image format"), QStringLiteral("Select image format to convert image to:"), items.values(), 0, false, &success);
            const ImageOperations::FileType targetFileType = items.key(userChoice, ImageOperations::IsOther);
            if (targetFileType > ImageOperations::IsOther && success) {
                QString newFilename = currentFilename;
                static const QRegularExpression fileExtensionRegExp(QStringLiteral("[.][a-zA-Z]{3,4}$"));
                if (targetFileType == ImageOperations::IsPNG)
                    newFilename.replace(fileExtensionRegExp, ImageOperations::extensionForFileType(ImageOperations::IsPNG));
                else if (targetFileType == ImageOperations::IsTIFF)
                    newFilename.replace(fileExtensionRegExp, ImageOperations::extensionForFileType(ImageOperations::IsTIFF));
                else if (targetFileType == ImageOperations::IsJPEG)
                    newFilename.replace(fileExtensionRegExp, ImageOperations::extensionForFileType(ImageOperations::IsJPEG));
                else if (targetFileType == ImageOperations::IsJpegXLLossy)
                    newFilename.replace(fileExtensionRegExp, ImageOperations::extensionForFileType(ImageOperations::IsJpegXLLossy));
                else if (targetFileType == ImageOperations::IsJpegXLLossless)
                    newFilename.replace(fileExtensionRegExp, ImageOperations::extensionForFileType(ImageOperations::IsJpegXLLossless));
                else if (targetFileType == ImageOperations::IsPBM)
                    newFilename.replace(fileExtensionRegExp, ImageOperations::extensionForFileType(ImageOperations::IsPBM));
                else if (targetFileType == ImageOperations::IsPGM)
                    newFilename.replace(fileExtensionRegExp, ImageOperations::extensionForFileType(ImageOperations::IsPGM));
                else if (targetFileType == ImageOperations::IsPPM)
                    newFilename.replace(fileExtensionRegExp, ImageOperations::extensionForFileType(ImageOperations::IsPPM));
                else if (targetFileType == ImageOperations::IsWebPLossy)
                    newFilename.replace(fileExtensionRegExp, ImageOperations::extensionForFileType(ImageOperations::IsWebPLossy));
                else if (targetFileType == ImageOperations::IsWebPLossless)
                    newFilename.replace(fileExtensionRegExp, ImageOperations::extensionForFileType(ImageOperations::IsWebPLossless));
                else if (targetFileType == ImageOperations::IsAVIF)
                    newFilename.replace(fileExtensionRegExp, ImageOperations::extensionForFileType(ImageOperations::IsAVIF));
                else
                    newFilename.clear();

                if (!newFilename.isEmpty() && targetFileType != inputFileType && targetFileType > ImageOperations::IsOther) {
                    qApp->setOverrideCursor(Qt::WaitCursor);
                    if (d->undoStack->addBackupToStack(currentFilename) == NoError) {
                        if (d->imageOperations->convertImageFormat(currentFilename, newFilename, targetFileType) == NoError) {
                            if (ImageOperations::isJPEG(currentFilename) && ImageOperations::isJPEG(newFilename))
                                d->exif->copyExif(currentFilename, newFilename);
                            QFile::remove(currentFilename);
                            d->replaceCurrentImageFilename(newFilename);
                        } else {
                            qWarning() << "Converting image failed:" << currentFilename << "to" << newFilename;
                            d->imageWidget->showMessage(QString(QStringLiteral("Converting image from file '%1' to '%2' failed")).arg(QFileInfo(currentFilename).fileName(), QFileInfo(newFilename).fileName()), 5, Qt::red);
                            qDebug() << "Trying to restore original";
                            executeCommand(ImageWidget::commandUndo);
                        }
                    }
                    qApp->restoreOverrideCursor();
                }
            }
        }
    }
    break;
    case ImageWidget::commandUndo: {
        qApp->setOverrideCursor(Qt::WaitCursor);
        int position = -1;
        const QString filename = d->undoStack->undo(&position);
        if (!filename.isEmpty()) {
            if (position < 0) {
                if (filename == d->imageFilename())
                    d->setPosition(d->posititon());
                else {
                    QFile::remove(d->imageFilename());
                    d->replaceCurrentImageFilename(filename);
                }
            } else {
                d->addPosition(position, filename);
                if (position <= d->posititon())
                    d->setPosition(d->posititon() + 1);
                else
                    d->setPosition(d->posititon());
            }
            d->imageWidget->showMessage(QString(QStringLiteral("Undo performed on file '%1'")).arg(QFileInfo(filename).fileName()), 5);
        } else {
            qWarning() << "MainWindow::executeCommand: undo failed";
            d->imageWidget->showMessage(QStringLiteral("Undo failed"), 5, Qt::red);
        }
        qApp->restoreOverrideCursor();
    }
    break;
    case ImageWidget::commandDelete: {
        const QString currentFilename = d->imageFilename();
        qApp->setOverrideCursor(Qt::WaitCursor);
        if (d->undoStack->addBackupToStack(currentFilename, d->posititon()) == NoError) {
            if (QFile::remove(currentFilename)) {
                d->removePosition(d->posititon());
                d->imageWidget->showMessage(QString(QStringLiteral("Deleted file '%1'")).arg(QFileInfo(currentFilename).fileName()), 5);
            } else {
                qWarning() << "Could not remove file" << currentFilename << "in ImageWidget::commandDelete";
                d->imageWidget->showMessage(QString(QStringLiteral("Deleting file '%1' failed")).arg(QFileInfo(currentFilename).fileName()), 5, Qt::red);
            }
        } else {
            qWarning() << "Could not create backup for file" << currentFilename << "in ImageWidget::commandDelete";
            d->imageWidget->showMessage(QString(QStringLiteral("Deleting file '%1' failed (problem with backup)")).arg(QFileInfo(currentFilename).fileName()), 5, Qt::red);
        }
        qApp->restoreOverrideCursor();
    }
    break;
    case ImageWidget::commandReload:
        d->setPosition(d->posititon());
        break;
    case ImageWidget::commandTouch: {
        qApp->setOverrideCursor(Qt::WaitCursor);
        const QString currentFilename = d->imageFilename();
        if (d->imageOperations->touch(currentFilename) == NoError) {
            d->setPosition(d->posititon());
            d->imageWidget->showMessage(QString(QStringLiteral("Touched file '%1'")).arg(QFileInfo(currentFilename).fileName()), 5);
        } else {
            qWarning() << "MainWindow::executeCommand(ImageWidget::commandTouch): touch command failed for file" << currentFilename;
            d->imageWidget->showMessage(QString(QStringLiteral("Touching file '%1' failed")).arg(QFileInfo(currentFilename).fileName()), 5, Qt::red);
        }
        qApp->restoreOverrideCursor();
    }
    break;
    case ImageWidget::commandGrayScale: {
        const QString currentFilename = d->imageFilename();
        if (ImageOperations::canGrayscale(currentFilename)) {
            qApp->setOverrideCursor(Qt::WaitCursor);
            if (d->undoStack->addBackupToStack(currentFilename) == NoError) {
                const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("grayscale"), ImageOperations::extensionFromFilename(currentFilename));
                if (d->imageOperations->grayscaleFile(currentFilename, tempfile) == NoError
                        && d->imageOperations->touch(tempfile) == NoError
                        && FileOperations::moveFile(tempfile, currentFilename, true) == NoError) {
                    d->setPosition(d->posititon());
                    d->imageWidget->showMessage(QString(QStringLiteral("Greyscaled file '%1'")).arg(QFileInfo(currentFilename).fileName()), 5);
                } else {
                    qWarning() << "Greyscale operation failed on file" << currentFilename;
                    d->imageWidget->showMessage(QString(QStringLiteral("Greyscaling file '%1' failed")).arg(QFileInfo(currentFilename).fileName()), 5, Qt::red);
                    qDebug() << "Trying to restore original after failed greyscale operation on " << currentFilename;
                    executeCommand(ImageWidget::commandUndo);
                }
                QFile::remove(tempfile);
            }
            qApp->restoreOverrideCursor();
        }
    }
    break;
    case ImageWidget::commandRotateRight90: {
        const QString currentFilename = d->imageFilename();
        if (ImageOperations::canRotate(currentFilename)) {
            qApp->setOverrideCursor(Qt::WaitCursor);
            if (d->undoStack->addBackupToStack(currentFilename) == NoError) {
                const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("rotate-right90"), ImageOperations::extensionFromFilename(currentFilename));
                if (d->imageOperations->rotate(currentFilename, tempfile, ImageOperations::Right90) == NoError
                        && d->imageOperations->touch(tempfile) == NoError
                        && FileOperations::moveFile(tempfile, currentFilename, true) == NoError) {
                    d->setPosition(d->posititon());
                    d->imageWidget->showMessage(QString(QStringLiteral("Rotated file '%1'")).arg(QFileInfo(currentFilename).fileName()), 5);
                    qApp->restoreOverrideCursor();
                } else {
                    qWarning() << "Rotate right 90 operation failed on file" << currentFilename;
                    d->imageWidget->showMessage(QString(QStringLiteral("Rotating file '%1' failed")).arg(QFileInfo(currentFilename).fileName()), 5, Qt::red);
                    qDebug() << "Trying to restore original after failed rotate operation on " << currentFilename;
                    executeCommand(ImageWidget::commandUndo);
                }
                QFile::remove(tempfile);
            }
            qApp->restoreOverrideCursor();
        }
    }
    break;
    case ImageWidget::commandRotateLeft90: {
        const QString currentFilename = d->imageFilename();
        if (ImageOperations::canRotate(currentFilename)) {
            qApp->setOverrideCursor(Qt::WaitCursor);
            if (d->undoStack->addBackupToStack(currentFilename) == NoError) {
                const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("rotate-left90"), ImageOperations::extensionFromFilename(currentFilename));
                if (d->imageOperations->rotate(currentFilename, tempfile, ImageOperations::Left90) == NoError
                        && d->imageOperations->touch(tempfile) == NoError
                        && FileOperations::moveFile(tempfile, currentFilename, true) == NoError) {
                    d->setPosition(d->posititon());
                    d->imageWidget->showMessage(QString(QStringLiteral("Rotated file '%1'")).arg(QFileInfo(currentFilename).fileName()), 5);
                } else {
                    qWarning() << "Rotate left 90 operation failed on file" << currentFilename;
                    d->imageWidget->showMessage(QString(QStringLiteral("Rotating file '%1' failed")).arg(QFileInfo(currentFilename).fileName()), 5, Qt::red);
                    qDebug() << "Trying to restore original after failed rotate operation on " << currentFilename;
                    executeCommand(ImageWidget::commandUndo);
                }
                QFile::remove(tempfile);
            }
            qApp->restoreOverrideCursor();
        }
    }
    break;
    case ImageWidget::commandScaleAndQuality: {
        const QString currentFilename = d->imageFilename();
        if (ImageOperations::canScale(currentFilename)) {
            qApp->setOverrideCursor(Qt::WaitCursor);
            if (d->undoStack->addBackupToStack(currentFilename) == NoError) {
                QString outputFilename;
                qApp->restoreOverrideCursor();
                const ErrorCode scaleResult = d->imageOperationsUI->scale(currentFilename, outputFilename);
                qApp->setOverrideCursor(Qt::WaitCursor);
                if (scaleResult == NoError) {
                    if (currentFilename == outputFilename)
                        d->setPosition(d->posititon());
                    else {
                        QFile::remove(currentFilename);
                        d->replaceCurrentImageFilename(outputFilename);
                    }
                    d->imageWidget->showMessage(QString(QStringLiteral("Scaled file '%1'")).arg(QFileInfo(outputFilename).fileName()), 5);
                } else {
                    if (scaleResult != UserCancelled) {
                        qWarning() << "Scale operation failed on file" << currentFilename;
                        d->imageWidget->showMessage(QString(QStringLiteral("Scaling file '%1' failed")).arg(QFileInfo(currentFilename).fileName()), 5, Qt::red);
                    } else if (scaleResult == UnsupportedFileType) {
                        qInfo() << "Scaling type of this file not supported:" << currentFilename;
                        d->imageWidget->showMessage(QString(QStringLiteral("Scaling type of file '%1' not supported")).arg(QFileInfo(currentFilename).fileName()), 5, Qt::red);
                    }
                    if (scaleResult != UserCancelled)
                        qDebug() << "Trying to restore original after failed/aborted scale operation on " << currentFilename;
                    executeCommand(ImageWidget::commandUndo);
                }
            }
            qApp->restoreOverrideCursor();
        }
    }
    break;
    case ImageWidget::commandCrop: {
        const QString currentFilename = d->imageFilename();
        const QRect selection = d->imageWidget->selection();
        if (ImageOperations::canCrop(currentFilename) && selection.isValid()) {
            qApp->setOverrideCursor(Qt::WaitCursor);
            if (d->undoStack->addBackupToStack(currentFilename) == NoError) {
                const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("crop"), ImageOperations::extensionFromFilename(currentFilename));
                ErrorCode cropResult = d->imageOperations->crop(currentFilename, tempfile, selection.left(), selection.top(), qMax(1, selection.width() - 1), qMax(1, selection.height() - 1));
                if (cropResult == NoError) {
                    cropResult = ImageOperations::isJPEG(currentFilename) ? d->exif->copyExif(currentFilename, tempfile) : NoError;
                    if (cropResult == NoError) {
                        cropResult = d->imageOperations->touch(tempfile);
                        if (cropResult == NoError) {
                            cropResult = FileOperations::moveFile(tempfile, currentFilename, true);
                            if (cropResult != NoError)
                                cropResult = FileOperations::touchFile(currentFilename);
                        }
                    }
                }
                if (cropResult == NoError) {
                    d->setPosition(d->posititon());
                    d->imageWidget->showMessage(QString(QStringLiteral("Cropped file '%1'")).arg(QFileInfo(currentFilename).fileName()), 5);
                } else {
                    qWarning() << "Crop operation failed on file" << currentFilename;
                    d->imageWidget->showMessage(QString(QStringLiteral("Cropping file '%1' failed")).arg(QFileInfo(currentFilename).fileName()), 5, Qt::red);
                    if (cropResult != UserCancelled)
                        qDebug() << "Trying to restore original for failed/aborted scale operation on " << currentFilename;
                    executeCommand(ImageWidget::commandUndo);
                }
                QFile::remove(tempfile);
            }
            qApp->restoreOverrideCursor();
        }
    }
    break;
    case ImageWidget::commandARAWCRS: {
        d->imageWidget->setARAWCRSselection();
        executeCommand(ImageWidget::commandCrop);
    }
    break;
    case ImageWidget::commandBlackBar: {
        const QString currentFilename = d->imageFilename();
        const QRect selection = d->imageWidget->selection();
        if (ImageOperations::canBlackBar(currentFilename) && selection.isValid()) {
            qApp->setOverrideCursor(Qt::WaitCursor);
            if (d->undoStack->addBackupToStack(currentFilename) == NoError) {
                const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("blackbar"), ImageOperations::extensionFromFilename(currentFilename));
                if (d->imageOperations->blackBar(currentFilename, tempfile, selection.left(), selection.top(), qMax(1, selection.width() - 1), qMax(1, selection.height() - 1)) == NoError
                        && d->imageOperations->touch(tempfile) == NoError
                        && (!ImageOperations::isJPEG(currentFilename) || d->exif->copyExif(currentFilename, tempfile) == NoError)
                        && FileOperations::moveFile(tempfile, currentFilename, true) == NoError) {
                    d->setPosition(d->posititon());
                    d->imageWidget->showMessage(QString(QStringLiteral("Added black bar in file '%1'")).arg(QFileInfo(currentFilename).fileName()), 5);
                } else {
                    qWarning() << "Crop operation failed on file" << currentFilename;
                    d->imageWidget->showMessage(QString(QStringLiteral("Adding black bar in file '%1' failed")).arg(QFileInfo(currentFilename).fileName()), 5, Qt::red);
                    executeCommand(ImageWidget::commandUndo);
                }
                QFile::remove(tempfile);
            }
            qApp->restoreOverrideCursor();
        }
    }
    break;
    case ImageWidget::commandBlur: {
        const QString currentFilename = d->imageFilename();
        const QRect selection = d->imageWidget->selection();
        if (ImageOperations::canBlur(currentFilename) && selection.isValid()) {
            qApp->setOverrideCursor(Qt::WaitCursor);
            if (d->undoStack->addBackupToStack(currentFilename) == NoError) {
                const QString tempfile = FileOperations::temporaryFilename(QStringLiteral("blur"), ImageOperations::extensionFromFilename(currentFilename));
                if (d->imageOperations->blur(currentFilename, tempfile, selection.left(), selection.top(), qMax(1, selection.width() - 1), qMax(1, selection.height() - 1)) == NoError
                        && d->imageOperations->touch(tempfile) == NoError
                        && (!ImageOperations::isJPEG(currentFilename) || d->exif->copyExif(currentFilename, tempfile) == NoError)
                        && FileOperations::moveFile(tempfile, currentFilename, true) == NoError) {
                    d->setPosition(d->posititon());
                    d->imageWidget->showMessage(QString(QStringLiteral("Added blur in file '%1'")).arg(QFileInfo(currentFilename).fileName()), 5);
                } else {
                    qWarning() << "Crop operation failed on file" << currentFilename;
                    d->imageWidget->showMessage(QString(QStringLiteral("Adding blur in file '%1' failed")).arg(QFileInfo(currentFilename).fileName()), 5, Qt::red);
                    executeCommand(ImageWidget::commandUndo);
                }
                QFile::remove(tempfile);
            }
            qApp->restoreOverrideCursor();
        }
    }
    break;
    case ImageWidget::commandNextImage:
        qApp->setOverrideCursor(Qt::WaitCursor);
        d->setPosition(d->posititon() + 1);
        qApp->restoreOverrideCursor();
        break;
    case ImageWidget::commandPrevImage:
        qApp->setOverrideCursor(Qt::WaitCursor);
        d->setPosition(d->posititon() - 1);
        qApp->restoreOverrideCursor();
        break;
    case ImageWidget::commandNext10Image:
        qApp->setOverrideCursor(Qt::WaitCursor);
        d->setPosition(d->posititon() + 10);
        qApp->restoreOverrideCursor();
        break;
    case ImageWidget::commandPrev10Image:
        qApp->setOverrideCursor(Qt::WaitCursor);
        d->setPosition(d->posititon() - 10);
        qApp->restoreOverrideCursor();
        break;
    case ImageWidget::commandNext100Image:
        qApp->setOverrideCursor(Qt::WaitCursor);
        d->setPosition(d->posititon() + 100);
        qApp->restoreOverrideCursor();
        break;
    case ImageWidget::commandPrev100Image:
        qApp->setOverrideCursor(Qt::WaitCursor);
        d->setPosition(d->posititon() - 100);
        qApp->restoreOverrideCursor();
        break;
    case ImageWidget::commandGotoFirst:
        qApp->setOverrideCursor(Qt::WaitCursor);
        d->setPosition(0);
        qApp->restoreOverrideCursor();
        break;
    case ImageWidget::commandGotoLast:
        qApp->setOverrideCursor(Qt::WaitCursor);
        d->setPosition(0xffffff);
        qApp->restoreOverrideCursor();
        break;
    case ImageWidget::commandRandomizeFileOrder:
        d->randomizeFileOrder();
        d->imageWidget->showMessage(QStringLiteral("Randomized order of files"), 5);
        break;
    default:
        qDebug() << "Unsupported command code: " << cmd;
    }
}

void MainWindow::zoomPercentChanged(int newPercent) {
    const QSize size = d->imageWidget->imageSize();
    d->statusBarImageSize->setText(QString(QStringLiteral("%1%2%3%4%5 (%6%2%)")).arg(size.width()).arg(QChar(0x2009)).arg(d->timesCharacter).arg(QChar(0x2009)).arg(size.height()).arg(newPercent));
}
