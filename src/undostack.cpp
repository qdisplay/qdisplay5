/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2019 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include "undostack.h"

#include <QStack>
#include <QFile>
#include <QFileInfo>
#include <QDebug>

#include "fileoperations.h"
#include "imageoperations.h"

class UndoStack::Private {
public:
    static const int limitNumberOfElements;

    struct UndoEntry {
        QString filename, backupFilename;
        int position;

        UndoEntry() :
            position(-1)
        {
            /** nothing */
        }

        UndoEntry(const QString &f, const QString &bf, const int p) :
            filename(f), backupFilename(bf), position(p)
        {
            /// nothing
        }
    };

    QStack<UndoEntry> stack;

    Private(UndoStack *)
    {
        /// nothing
    }

    ~Private() {
        while (!stack.isEmpty()) {
            const UndoEntry ue = stack.pop();
            if (!QFile::remove(ue.backupFilename))
                qDebug() << "Could not remove file" << ue.backupFilename << "in UndoStack::~Private()";
        }
    }

    void limitStack(int numberOfElements = limitNumberOfElements) {
        while (stack.count() > numberOfElements) {
            const UndoEntry ue = stack.first();
            if (!QFile::remove(ue.backupFilename))
                qDebug() << "Could not remove file" << ue.backupFilename << "in UndoStack::limitStack()";
            stack.removeFirst();
        }
    }
};

const int UndoStack::Private::limitNumberOfElements = 32;

UndoStack::UndoStack(QObject *parent) :
    QObject(parent), d(new Private(this))
{
    /// nothing
}

UndoStack::~UndoStack() {
    delete d;
}

ErrorCode UndoStack::addBackupToStack(const QString &filename, int position) {
    const QString backupFilename = FileOperations::temporaryFilename(QStringLiteral("backup"), ImageOperations::extensionFromFilename(filename));

    ErrorCode result = FileOperations::copyFile(filename, backupFilename, true);
    if (result != NoError) {
        if (!QFile::remove(backupFilename))
            qDebug() << "Could not remove file" << backupFilename << "in UndoStack::addBackupToStack()";
        return result;
    }

    d->stack.push(Private::UndoEntry(filename, backupFilename, position));

    d->limitStack();
    emit availableUndoStepsChanged(d->stack.count());

    return NoError;
}

QString UndoStack::undo(int *position) {
    if (d->stack.isEmpty())
        return QString();

    const Private::UndoEntry ue = d->stack.pop();

    if (FileOperations::moveFile(ue.backupFilename, ue.filename, true) != NoError) {
        if (!QFile::remove(ue.backupFilename))
            qDebug() << "Could not remove file" << ue.backupFilename << "in UndoStack::undo()";
        return QString();
    }

    emit availableUndoStepsChanged(d->stack.count());

    if (position != nullptr) *position = ue.position;
    return ue.filename;
}
