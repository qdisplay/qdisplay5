/***************************************************************************
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *   SPDX-FileCopyrightText: 2014-2022 Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see <https://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include "scaleandqualitydialog.h"

#include <QFormLayout>
#include <QCheckBox>
#include <QSpinBox>
#include <QComboBox>
#include <QRadioButton>
#include <QSlider>
#include <QLabel>
#include <QPushButton>
#include <QKeyEvent>
#include <QHash>
#include <QShortcut>
#include <QSignalBlocker>
#include <QDebug>

#define isLossyFormat(fileType) (fileType == ImageOperations::IsJPEG || fileType == ImageOperations::IsAVIF || fileType == ImageOperations::IsJpegXLLossy || fileType == ImageOperations::IsWebPLossy)

class ScaleAndQualityDialog::Private {
private:
    ScaleAndQualityDialog *p;

public:
    QCheckBox *checkBoxKeepAspectRatio;
    QSpinBox *spinBoxWidth, *spinBoxHeight, *spinBoxScale;
    QComboBox *comboBoxPredefinedScale;
    QSpinBox *spinBoxQuality;
    QComboBox *comboBoxColors;
    QCheckBox *checkBoxNormalize;
    QRadioButton *radioGammaNone, *radioGammaAuto, *radioGammaManual;
    QSlider *sliderGammaManual;
    QLabel *labelGammaManual;
    QComboBox *comboBoxConvertTo;

    int originalWidth, originalHeight;
    bool qualityUntouched;
    const bool originalIsLossy;

    Private(ScaleAndQualityDialog *parent, const ScaleQualityParameters &sqp) :
        p(parent), originalIsLossy(isLossyFormat(sqp.originalFileType))
    {
        setupGUI(sqp);
    }

    void setupGUI(const ScaleQualityParameters &sqp) {
        QBoxLayout *layout = new QVBoxLayout(p);
        QFormLayout *formLayout = new QFormLayout();
        formLayout->setContentsMargins(0, 0, 0, 0);
        layout->addLayout(formLayout, 0);

        comboBoxConvertTo = new QComboBox(p);
        comboBoxConvertTo->setEditable(false);
        comboBoxConvertTo->addItem(QString(QStringLiteral("No conversion, stays %1")).arg(ImageOperations::fileTypeToShortDescription(sqp.originalFileType)), static_cast<int>(sqp.originalFileType));
        static const QVector<ImageOperations::FileType> conversionTargets {ImageOperations::IsPNG, ImageOperations::IsTIFF, ImageOperations::IsJPEG, ImageOperations::IsJpegXLLossless, ImageOperations::IsJpegXLLossy, ImageOperations::IsWebPLossless, ImageOperations::IsWebPLossy, ImageOperations::IsAVIF};
        for (const ImageOperations::FileType conversionTargetFileType : conversionTargets)
            if (sqp.originalFileType != conversionTargetFileType)
                comboBoxConvertTo->addItem(QString(QStringLiteral("Convert to %1")).arg(ImageOperations::fileTypeToShortDescription(conversionTargetFileType)), static_cast<int>(conversionTargetFileType));
        formLayout->addRow(QStringLiteral("&Conversion:"), comboBoxConvertTo);

        checkBoxKeepAspectRatio = new QCheckBox(QStringLiteral("Keep it"), p);
        formLayout->addRow(QStringLiteral("&Aspect Ratio:"), checkBoxKeepAspectRatio);

        spinBoxWidth = new QSpinBox(p);
        spinBoxWidth->setMinimum(1);
        spinBoxWidth->setMaximum(32768);
        formLayout->addRow(QStringLiteral("&Width:"), spinBoxWidth);

        spinBoxHeight = new QSpinBox(p);
        spinBoxHeight->setMinimum(1);
        spinBoxHeight->setMaximum(32768);
        formLayout->addRow(QStringLiteral("&Height:"), spinBoxHeight);

        spinBoxScale = new QSpinBox(p);
        spinBoxScale->setMinimum(1);
        spinBoxScale->setMaximum(65536);
        formLayout->addRow(QStringLiteral("&Percent:"), spinBoxScale);

        comboBoxPredefinedScale = new QComboBox(p);
        comboBoxPredefinedScale->setEditable(false);
        static const QVector<int> scalingValues {250, 333, 500, 667, 1000, 1333, 1667, 2000, 2222, 2500, 2667, 3000, 3333, 4000, 4444, 5000, 5556, 6000, 6667, 7000, 7500, 7778, 8000, 8500, 8889, 9000, 9500, 10000};
        for (const int scalingValue : scalingValues) {
            const int newWidth = (sqp.width * scalingValue + 5000) / 10000;
            const int newHeight = (sqp.height * scalingValue + 5000) / 10000;
            const QString percent = scalingValue < 2000 ? QString::number(scalingValue / 100.0, 'g', 3) : QString::number((scalingValue + 50) / 100);
            comboBoxPredefinedScale->addItem(QString(QStringLiteral("%1% (%2%4%3)")).arg(percent, QString::number(newWidth), QString::number(newHeight), QChar(0x2a09)), QVariant::fromValue<int>(scalingValue));
        }
        comboBoxPredefinedScale->addItem(QStringLiteral("Custom"));
        comboBoxPredefinedScale->setCurrentIndex(scalingValues.length() - 1);
        formLayout->addRow(QStringLiteral("Pre&defined:"), comboBoxPredefinedScale);

        spinBoxQuality = new QSpinBox(p);
        spinBoxQuality->setMinimum(1);
        spinBoxQuality->setMaximum(100);
        spinBoxQuality->setSuffix(QChar(0x2009) + QStringLiteral("%"));
        formLayout->addRow(QStringLiteral("&Quality:"), spinBoxQuality);

        comboBoxColors = new QComboBox(p);
        formLayout->addRow(QStringLiteral("&Colors:"), comboBoxColors);
        comboBoxColors->addItem(QStringLiteral("Automatic"), ColorsAutomatic);
        comboBoxColors->addItem(QStringLiteral("2 Colors"), Colors2);
        comboBoxColors->addItem(QStringLiteral("2 Colors (dithered)"), Colors2Dithered);
        comboBoxColors->addItem(QStringLiteral("4 Colors"), Colors4);
        comboBoxColors->addItem(QStringLiteral("4 Colors (dithered)"), Colors4Dithered);
        comboBoxColors->addItem(QStringLiteral("8 Colors"), Colors8);
        comboBoxColors->addItem(QStringLiteral("8 Colors (dithered)"), Colors8Dithered);
        comboBoxColors->addItem(QStringLiteral("16 Colors"), Colors16);
        comboBoxColors->addItem(QStringLiteral("16 Colors (dithered)"), Colors16Dithered);
        comboBoxColors->addItem(QStringLiteral("32 Colors"), Colors32);
        comboBoxColors->addItem(QStringLiteral("32 Colors (dithered)"), Colors32Dithered);
        comboBoxColors->addItem(QStringLiteral("64 Colors"), Colors64);
        comboBoxColors->addItem(QStringLiteral("64 Colors (dithered)"), Colors64Dithered);
        comboBoxColors->addItem(QStringLiteral("128 Colors"), Colors128);
        comboBoxColors->addItem(QStringLiteral("128 Colors (dithered)"), Colors128Dithered);
        comboBoxColors->addItem(QStringLiteral("256 Colors"), Colors256);
        comboBoxColors->addItem(QStringLiteral("256 Colors (dithered)"), Colors256Dithered);

        checkBoxNormalize = new QCheckBox(QStringLiteral("Full brightness range"), p);
        formLayout->addRow(QStringLiteral("&Normalize:"), checkBoxNormalize);

        QWidget *gammaWidget = new QWidget(p);
        QGridLayout *gammaLayout = new QGridLayout(gammaWidget);
        gammaLayout->setContentsMargins(0, 0, 0, 0);
        gammaLayout->setColumnMinimumWidth(0, 24);
        formLayout->addRow(QStringLiteral("Gamma correction:"), gammaWidget);
        radioGammaNone = new QRadioButton(QStringLiteral("No chang&e"), gammaWidget);
        gammaLayout->addWidget(radioGammaNone, 0, 0, 1, 3);
        radioGammaAuto = new QRadioButton(QStringLiteral("Au&tomatic"), gammaWidget);
        gammaLayout->addWidget(radioGammaAuto, 1, 0, 1, 3);
        radioGammaManual = new QRadioButton(QStringLiteral("&Manual"), gammaWidget);
        gammaLayout->addWidget(radioGammaManual, 2, 0, 1, 3);
        sliderGammaManual = new QSlider(Qt::Horizontal, gammaWidget);
        gammaLayout->addWidget(sliderGammaManual, 3, 1, 1, 1);
        sliderGammaManual->setMinimum(60);
        sliderGammaManual->setMaximum(240);
        labelGammaManual = new QLabel(gammaWidget);
        gammaLayout->addWidget(labelGammaManual, 3, 2, 1, 1);

        layout->addSpacing(p->fontMetrics().xHeight());

        QBoxLayout *buttonLayout = new QHBoxLayout();
        buttonLayout->setContentsMargins(0, 0, 0, 0);
        layout->addLayout(buttonLayout);
        buttonLayout->addStretch(10);

        QPushButton *buttonOk = new QPushButton(QStringLiteral("&Scale"), p);
        buttonLayout->addWidget(buttonOk, 1);
        buttonOk->setDefault(true);
        connect(buttonOk, &QPushButton::clicked, p, &QDialog::accept);
        QPushButton *buttonCancel = new QPushButton(QStringLiteral("Cancel"), p);
        buttonLayout->addWidget(buttonCancel, 1);
        connect(buttonCancel, &QPushButton::clicked, p, &QDialog::reject);
    }

    void setupSignals() {
        QObject::connect(checkBoxKeepAspectRatio, &QCheckBox::toggled, p, &ScaleAndQualityDialog::toggleKeepAspectRatio);
        QObject::connect(spinBoxWidth, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), p, &ScaleAndQualityDialog::widthChanged);
        QObject::connect(spinBoxHeight, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), p, &ScaleAndQualityDialog::heightChanged);
        QObject::connect(spinBoxScale, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), p, &ScaleAndQualityDialog::scaleChanged);
        QObject::connect(comboBoxPredefinedScale, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), p, &ScaleAndQualityDialog::combobBoxScaleChanged);
        QObject::connect(sliderGammaManual, &QSlider::valueChanged, p, &ScaleAndQualityDialog::gammaSliderChanged);

        // In this dialog, switch to AVIF format if Meta (or Windows) key and A is pressed
        new QShortcut(QKeySequence(static_cast<int>(Qt::META) + static_cast<int>(Qt::Key_A)), p, [this]() {
            const int index = comboBoxConvertTo->findData(ImageOperations::IsAVIF);
            if (index >= 0)
                comboBoxConvertTo->setCurrentIndex(index);
        }, Qt::WindowShortcut);
        // In this dialog, switch to WebP format if Meta (or Windows) key and W is pressed;
        // choose lossy or lossless WebP depending on source format
        new QShortcut(QKeySequence(static_cast<int>(Qt::META) + static_cast<int>(Qt::Key_W)), p, [this]() {
            const int index = comboBoxConvertTo->findData(originalIsLossy ? ImageOperations::IsWebPLossy : ImageOperations::IsWebPLossless);
            if (index >= 0)
                comboBoxConvertTo->setCurrentIndex(index);
        }, Qt::WindowShortcut);
        // In this dialog, switch to JPEG XL format if Meta (or Windows) key and J is pressed;
        // choose lossy or lossless JPEG XL depending on source format
        new QShortcut(QKeySequence(static_cast<int>(Qt::META) + static_cast<int>(Qt::Key_J)), p, [this]() {
            const int index = comboBoxConvertTo->findData(originalIsLossy ? ImageOperations::IsJpegXLLossy : ImageOperations::IsJpegXLLossless);
            if (index >= 0)
                comboBoxConvertTo->setCurrentIndex(index);
        }, Qt::WindowShortcut);
        // In this dialog, switch to TIFF format if Meta (or Windows) key and T is pressed
        new QShortcut(QKeySequence(static_cast<int>(Qt::META) + static_cast<int>(Qt::Key_T)), p, [this]() {
            const int index = comboBoxConvertTo->findData(ImageOperations::IsTIFF);
            if (index >= 0)
                comboBoxConvertTo->setCurrentIndex(index);
        }, Qt::WindowShortcut);
    }
};

ScaleAndQualityDialog::ScaleAndQualityDialog(const ScaleQualityParameters &sqp, QWidget *parent) :
    QDialog(parent), d(new Private(this, sqp))
{
    setWindowTitle(QStringLiteral("Scale and Quality"));

    d->checkBoxKeepAspectRatio->setChecked(sqp.keepAspectRatio);
    d->spinBoxWidth->setValue(d->originalWidth = qMax(1, sqp.width));
    d->spinBoxWidth->setMaximum(d->spinBoxWidth->value());
    d->spinBoxHeight->setValue(d->originalHeight = qMax(1, sqp.height));
    d->spinBoxHeight->setMaximum(d->spinBoxHeight->value());
    d->spinBoxScale->setValue(sqp.scale);
    const int i = d->comboBoxPredefinedScale->findData(sqp.scale);
    if (i >= 0)
        d->comboBoxPredefinedScale->setCurrentIndex(i);
    else
        d->comboBoxPredefinedScale->setCurrentIndex(d->comboBoxPredefinedScale->count() - 1);
    d->spinBoxQuality->setValue(sqp.percentQuality);
    d->comboBoxColors->setCurrentIndex(qMax(0, d->comboBoxColors->findData(sqp.indexColors)));
    d->checkBoxNormalize->setChecked(sqp.normalize);
    if (sqp.gamma < -0.5) {
        d->sliderGammaManual->setValue(100);
        d->radioGammaAuto->setChecked(true);
    } else if (sqp.gamma > 0.5) {
        d->sliderGammaManual->setValue(static_cast<int>(sqp.gamma * 100.0 + 0.5));
        gammaSliderChanged(d->sliderGammaManual->value());
    } else {
        d->sliderGammaManual->setValue(100);
        d->radioGammaNone->setChecked(true);
    }

    int idx = 0;
    for (idx = d->comboBoxConvertTo->count() - 1; idx >= 0; --idx)
        if (d->comboBoxConvertTo->itemData(idx).toInt() == static_cast<int>(sqp.convertTo))
            break;
    d->comboBoxConvertTo->setCurrentIndex(qMax(0, idx));

    d->qualityUntouched = true;
    connect(d->spinBoxQuality, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, [this]() {
        d->qualityUntouched = false;
    });
    connect(d->comboBoxConvertTo, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &ScaleAndQualityDialog::targetFileTypeChanged);
    d->setupSignals();

    const int spinBoxMinWidth = qMax(qMax(d->spinBoxScale->width(), d->spinBoxQuality->width()), qMax(d->spinBoxWidth->width(), d->spinBoxHeight->width()));
    d->spinBoxWidth->setMinimumWidth(spinBoxMinWidth);
    d->spinBoxHeight->setMinimumWidth(spinBoxMinWidth);
    d->spinBoxScale->setMinimumWidth(spinBoxMinWidth);
    d->spinBoxQuality->setMinimumWidth(spinBoxMinWidth);

    targetFileTypeChanged();

    // Delay configuration of event filter until all instance members are initialized
    d->comboBoxConvertTo->installEventFilter(this);
}

ScaleAndQualityDialog::~ScaleAndQualityDialog() {
    delete d;
}

ScaleAndQualityDialog::ScaleQualityParameters ScaleAndQualityDialog::parameters() const {
    ScaleQualityParameters result;
    result.originalFileType = ImageOperations::IsOther;
    result.originalColors = ImageOperations::ColorsUnknown;
    result.keepAspectRatio = d->checkBoxKeepAspectRatio->isChecked();
    result.width = d->spinBoxWidth->value();
    result.height = d->spinBoxHeight->value();
    result.scale = d->spinBoxScale->value();
    result.percentQuality = d->spinBoxQuality->value();
    result.indexColors = static_cast<IndexColors>(d->comboBoxColors->currentData().toInt());
    result.normalize = d->checkBoxNormalize->isChecked();
    if (d->radioGammaAuto->isChecked())
        result.gamma = -1.0;
    else if (d->radioGammaManual->isChecked())
        result.gamma = d->sliderGammaManual->value() / 100.0;
    else
        result.gamma = 0.0;
    result.convertTo = static_cast<ImageOperations::FileType>(d->comboBoxConvertTo->currentData().toInt());

    return result;
}

bool ScaleAndQualityDialog::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == d->comboBoxConvertTo && event->type() == QEvent::KeyPress) {
        // Allow for a single key press of selected keys to choose a corresponding file type to convert to.
        // Examples: 'A' -> select AVIF, 'J' -> select JPEG
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if (keyEvent->modifiers() == Qt::KeyboardModifiers()) {
            const QHash<int, ImageOperations::FileType> mapping {{Qt::Key_T, ImageOperations::IsTIFF}, {Qt::Key_A, ImageOperations::IsAVIF}, {Qt::Key_P, ImageOperations::IsPNG}, {Qt::Key_W, d->originalIsLossy ? ImageOperations::IsWebPLossy : ImageOperations::IsWebPLossless}, {Qt::Key_J, d->originalIsLossy ? ImageOperations::IsJpegXLLossy : ImageOperations::IsJpegXLLossless}};
            for (auto it = mapping.constBegin(); it != mapping.constEnd(); ++it)
                if (it.key() == keyEvent->key()) {
                    const int index = d->comboBoxConvertTo->findData(it.value());
                    if (index >= 0) {
                        d->comboBoxConvertTo->setCurrentIndex(index);
                        return true;
                    }
                }
        }
    }

    return QObject::eventFilter(obj, event);
}

void ScaleAndQualityDialog::toggleKeepAspectRatio() {
    const bool enabledScale{d->checkBoxKeepAspectRatio->isChecked()};
    d->comboBoxPredefinedScale->setEnabled(enabledScale);
    d->spinBoxScale->setEnabled(enabledScale);
    if (enabledScale)
        widthChanged(d->spinBoxWidth->value());
}

void ScaleAndQualityDialog::gammaSliderChanged(int newGamma) {
    if (d->radioGammaManual == nullptr)
        return;

    d->radioGammaManual->setChecked(true);
    d->labelGammaManual->setText(QString::number(newGamma / 100.0, 'g', 2));
}

void ScaleAndQualityDialog::widthChanged(int newWidth) {
    if (d->checkBoxKeepAspectRatio->isChecked()) {
        if (newWidth < 1)
            qWarning() << "newWidth is 0 in ScaleAndQualityDialog::widthChanged(" << newWidth << ")";
        newWidth = qMin(qMax(1, newWidth), d->originalWidth);

        QSignalBlocker spinBoxHeightSignalBlocker(d->spinBoxHeight);
        d->spinBoxHeight->setValue(d->originalHeight * newWidth / d->originalWidth);
        QSignalBlocker comboBoxScaleSignalBlocker(d->comboBoxPredefinedScale);
        const int i = d->comboBoxPredefinedScale->findData(QVariant::fromValue<int>((10000 * newWidth + 5000) / d->originalWidth));
        if (i >= 0)
            d->comboBoxPredefinedScale->setCurrentIndex(i);
        else
            d->comboBoxPredefinedScale->setCurrentIndex(d->comboBoxPredefinedScale->count() - 1);
        QSignalBlocker spinBoxScaleSignalBlocker(d->spinBoxScale);
        d->spinBoxScale->setValue((10000 * newWidth + 5000) / d->originalWidth);
    }
}

void ScaleAndQualityDialog::heightChanged(int newHeight) {
    if (d->checkBoxKeepAspectRatio->isChecked()) {
        if (newHeight < 1)
            qWarning() << "newHeight is 0 in ScaleAndQualityDialog::heightChanged(" << newHeight << ")";
        newHeight = qMin(qMax(1, newHeight), d->originalHeight);

        QSignalBlocker spinBoxWidthSignalBlocker(d->spinBoxWidth);
        d->spinBoxWidth->setValue(d->originalWidth * newHeight / d->originalHeight);
        QSignalBlocker comboBoxScaleSignalBlocker(d->comboBoxPredefinedScale);
        const int i = d->comboBoxPredefinedScale->findData(QVariant::fromValue<int>((10000 * newHeight + 5000) / d->originalHeight));
        if (i >= 0)
            d->comboBoxPredefinedScale->setCurrentIndex(i);
        else
            d->comboBoxPredefinedScale->setCurrentIndex(d->comboBoxPredefinedScale->count() - 1);
        QSignalBlocker spinBoxScaleSignalBlocker(d->spinBoxScale);
        d->spinBoxScale->setValue((10000 * newHeight + 5000) / d->originalHeight);
    }
}

void ScaleAndQualityDialog::scaleChanged(int newScale) {
    QSignalBlocker spinBoxWidthSignalBlocker(d->spinBoxWidth);
    d->spinBoxWidth->setValue((d->originalWidth * newScale + 5000) / 10000);
    QSignalBlocker spinBoxHeightSignalBlocker(d->spinBoxHeight);
    d->spinBoxHeight->setValue((d->originalHeight * newScale + 5000) / 10000);
    QSignalBlocker comboBoxScaleSignalBlocker(d->comboBoxPredefinedScale);
    const int i = d->comboBoxPredefinedScale->findData(newScale);
    if (i >= 0)
        d->comboBoxPredefinedScale->setCurrentIndex(i);
    else
        d->comboBoxPredefinedScale->setCurrentIndex(d->comboBoxPredefinedScale->count() - 1);
}

void ScaleAndQualityDialog::combobBoxScaleChanged(int newIndex)
{
    const QVariant v{d->comboBoxPredefinedScale->itemData(newIndex)};
    if (v.canConvert<int>()) {
        const int newScale = v.value<int>();
        QSignalBlocker spinBoxWidthSignalBlocker(d->spinBoxWidth);
        d->spinBoxWidth->setValue((d->originalWidth * newScale + 5000) / 10000);
        QSignalBlocker spinBoxHeightSignalBlocker(d->spinBoxHeight);
        d->spinBoxHeight->setValue((d->originalHeight * newScale + 5000) / 10000);
        QSignalBlocker spinBoxScaleSignalBlocker(d->spinBoxScale);
        d->spinBoxScale->setValue(newScale);
    }
}

void ScaleAndQualityDialog::targetFileTypeChanged()
{
    const ImageOperations::FileType newTargetFileType = static_cast<ImageOperations::FileType>(d->comboBoxConvertTo->currentData().toInt());

    const bool lossyFormat = newTargetFileType == ImageOperations::IsJPEG || newTargetFileType == ImageOperations::IsAVIF || newTargetFileType == ImageOperations::IsWebPLossy || newTargetFileType == ImageOperations::IsJpegXLLossy;
    d->spinBoxQuality->setEnabled(lossyFormat);
    if (d->qualityUntouched) {
        if (newTargetFileType == ImageOperations::IsWebPLossy)
            d->spinBoxQuality->setValue(85);
        else if (newTargetFileType == ImageOperations::IsAVIF)
            d->spinBoxQuality->setValue(60);
        else if (newTargetFileType == ImageOperations::IsJPEG)
            d->spinBoxQuality->setValue(75);
        else if (newTargetFileType == ImageOperations::IsJpegXLLossy)
            d->spinBoxQuality->setValue(75);
        d->qualityUntouched = true; //< setValue above set it to false
        d->comboBoxColors->setCurrentIndex(0); //< 'Automatic'
    }
    d->comboBoxColors->setEnabled(!lossyFormat);

    d->checkBoxNormalize->setEnabled(newTargetFileType != ImageOperations::IsPBM);
    d->radioGammaNone->setEnabled(newTargetFileType != ImageOperations::IsPBM);
    d->radioGammaAuto->setEnabled(newTargetFileType != ImageOperations::IsPBM);
    d->radioGammaManual->setEnabled(newTargetFileType != ImageOperations::IsPBM);
    d->sliderGammaManual->setEnabled(newTargetFileType != ImageOperations::IsPBM);
}
